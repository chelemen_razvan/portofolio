//
//  ComputeBoundingBox.m
//  P1_
//
//  Created by Radu Stroia on 02/04/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ComputeBoundingBox.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@implementation ComputeBoundingBox

- (MKMapRect)computeBoundingBoxForArray:(NSArray *)locations
{
	// while we create the route points, we will also be calculating the bounding box of our route
	// so we can easily zoom in on it.
	MKMapPoint northEastPoint;
	MKMapPoint southWestPoint;
		
	for(int idx = 0; idx < [locations count]; idx++)
	{
		// Get our location and create a map point
		CLLocation *location = [locations objectAtIndex:idx];
		MKMapPoint point = MKMapPointForCoordinate(location.coordinate);
		
		
		// if it is the first point, just use it, since we have nothing to compare to yet.
		if (idx == 0)
		{
			northEastPoint = point;
			southWestPoint = point;
		}
		else
		{
			if (point.x > northEastPoint.x)
				northEastPoint.x = point.x;
			if(point.y > northEastPoint.y)
				northEastPoint.y = point.y;
			if (point.x < southWestPoint.x)
				southWestPoint.x = point.x;
			if (point.y < southWestPoint.y)
				southWestPoint.y = point.y;
		}
	}
	
	MKMapRect boundingBox = MKMapRectMake(southWestPoint.x, southWestPoint.y, northEastPoint.x - southWestPoint.x, northEastPoint.y - southWestPoint.y);
	return boundingBox;
}

@end
