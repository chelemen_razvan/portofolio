//
//  RouteSource.h
//  P1_
//
//  Created by Radu Stroia on 02/04/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

typedef void(^RouteSourceCompletionBlock)(NSDictionary *loadedRoute, NSError *error);

@interface RouteSource : NSObject

@property (nonatomic, strong) CLLocation *startLocation;
@property (nonatomic, strong) CLLocation *endLocation;

- (void)loadRouteWithCompletionBlock:(RouteSourceCompletionBlock)completionBlock;

@end
