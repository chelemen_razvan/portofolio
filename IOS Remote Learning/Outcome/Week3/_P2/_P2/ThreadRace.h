//
//  ThreadRace.h
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRaceCompetitor.h"
#import "ThreadRaceContext.h"

@interface ThreadRace : NSObject
- (void) startRace;
@end
