//
//  ThreadRace.m
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ThreadRace.h"

@implementation ThreadRace{
	ThreadRaceContext *context;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		context = [[ThreadRaceContext alloc] init];
	}
	return self;
}
- (void) startRace
{
	
	@autoreleasepool {
	NSMutableArray *competitors = [[NSMutableArray alloc] init];
	for (int i = 0; i<10; i++)
	{
		NSString *name = [[NSString alloc] initWithFormat:@"Player %d",i];
		ThreadRaceCompetitor *competitor = [[ThreadRaceCompetitor alloc] initWithThreadRaceContext:context Name:name];
		[competitors addObject:competitor];
	}
	dispatch_queue_t serialQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
	
	for (ThreadRaceCompetitor *competitor in competitors) {
		dispatch_async(serialQueue, ^{
			[competitor race];
		});
	}
	}
	//NSLog(@"mata");
	//NSMutableDictionary *dictionary = [context getResults];
	//NSLog(@"%@",[[context getResults] description]);
	//[context printResults];
}

@end
