//
//  ThreadRaceCompetitor.h
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRaceContext.h"
@class ThreadRaceContext;

@interface ThreadRaceCompetitor : NSObject
- (void) race;
- (id) initWithThreadRaceContext:(ThreadRaceContext *)raceContext Name:(NSString *) n;
- (void) setPosition:(int) pos;
- (void) printCompetitor;
@end
