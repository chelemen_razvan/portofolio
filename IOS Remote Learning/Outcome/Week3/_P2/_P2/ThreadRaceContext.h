//
//  ThreadRaceContext.h
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRaceCompetitor.h"

@interface ThreadRaceContext : NSObject
- (void)addRacerFinish:(NSString *)competitor;
- (void)printResults;
- (NSMutableDictionary *)getResults;
@end
