//
//  ThreadRaceContext.m
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ThreadRaceContext.h"
#import "ThreadRaceCompetitor.h"
#import "pthread.h"


@implementation ThreadRaceContext{
	int position;
	NSMutableDictionary *dictionary;
	pthread_mutex_t mutex;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		position = 0;
		dictionary = [[NSMutableDictionary alloc] init];
		pthread_mutex_init(&mutex, NULL);
	}
	return self;
}

- (void)addRacerFinish:(NSString *)competitor
{
	pthread_mutex_lock(&mutex);
	position++;
	[dictionary setValue:competitor forKey:[[NSString alloc] initWithFormat:@"%d",position ]];
	pthread_mutex_unlock(&mutex);
	if (position >= 10) {
		[self printResults];
	}
	
}

- (void)printResults
{
	pthread_mutex_destroy(&mutex);
	NSLog(@" %@",[dictionary description]);
}

- (NSMutableDictionary *)getResults
{
	return dictionary;
}

@end
