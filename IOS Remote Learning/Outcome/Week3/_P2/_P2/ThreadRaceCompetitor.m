//
//  ThreadRaceCompetitor.m
//  Week3-2
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ThreadRaceCompetitor.h"
#import "ThreadRaceContext.h"

@implementation ThreadRaceCompetitor {
	NSString *name;
	int position;
	ThreadRaceContext *context;
}
- (void) race
{
	[context addRacerFinish:name];
}

- (id) initWithThreadRaceContext:(ThreadRaceContext *)raceContext Name:(NSString *) n
{
	self = [super self];
	if (self) {
		name = n;
		context = raceContext;
	}
	
	return self;
}

- (void) setPosition:(int) pos
{
	position=pos;
}

- (void) printCompetitor
{
	NSLog(@"Name: %@, position: %d",name,position);
}

@end