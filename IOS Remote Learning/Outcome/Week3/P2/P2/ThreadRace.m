//
//  ThreadRace.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRace.h"

@implementation ThreadRace
{
	NSMutableArray *_racersArray;
	ThreadRaceContext *_raceContext;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_racersArray = [[NSMutableArray alloc] init];
		_raceContext = [[ThreadRaceContext alloc] init];
		[self createRacers];
	}
	return self;
}

- (void) startRace
{
	for (ThreadRaceCompetitor *racer in _racersArray)
	{
		[racer start];
	}
}

- (void) createRacers
{
	for (int i = 0; i < NUMBER_OF_RACERS; i++)
	{
		ThreadRaceCompetitor *competitor = [[ThreadRaceCompetitor alloc]
			initWithContext:_raceContext AndName: [NSString stringWithFormat:@"Racer: %i", i]];
		[_racersArray addObject:competitor];
	}
}

@end
