//
//  ThreadRaceContext.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRaceContext.h"

@implementation ThreadRaceContext
{
	NSMutableDictionary *_rankings;
	int _position;
	NSObject *_lock;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_position = 0;
		_rankings = [[NSMutableDictionary alloc] init];
		_lock = [[NSObject alloc] init];
	}
	return self;
}

- (void) recordRacerRank: (NSString *) racerName
{
	@synchronized(_lock)
	{
		_position++;
		[_rankings setObject:[NSNumber numberWithInt:_position] forKey:racerName];
		if (_position == NUMBER_OF_RACERS)
		{
			[self showRankings];
		}
	}
}

-(void) showRankings
{
	NSLog(@"%@", [_rankings description]);
}

@end
