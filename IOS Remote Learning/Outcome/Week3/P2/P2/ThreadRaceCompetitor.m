//
//  ThreadRaceCompetitor.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRaceCompetitor.h"

@implementation ThreadRaceCompetitor
{
	ThreadRaceContext *_raceContext;
	NSString *_racerName;
}

- (id) initWithContext: (ThreadRaceContext *) context AndName: (NSString *) name
{
	self = [super init];
	if (self)
	{
		_raceContext = context;
		_racerName = name;
	}
	return self;
}

- (void) start
{
	NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(notifyRaceContext) object:Nil];
	[thread start];
}

- (void) notifyRaceContext
{
	int duration = arc4random() % 10;
	sleep(duration);
	[_raceContext recordRacerRank: _racerName];
}

@end
