//
//  ThreadRaceCompetitor.h
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRaceContext.h"
#import "ThreadRaceContext.h"
@class ThreadRaceContext;

@interface ThreadRaceCompetitor : NSObject

- (id) initWithContext: (ThreadRaceContext *) context AndName: (NSString *) name;
- (void) start;

@end
