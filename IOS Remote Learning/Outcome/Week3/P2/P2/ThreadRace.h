//
//  ThreadRace.h
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRaceCompetitor.h"
#import "ThreadRaceContext.h"

static const int NUMBER_OF_RACERS = 10 ;

@interface ThreadRace : NSObject

- (void) startRace;

@end
