//
//  Server.m
//  Week3-1
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "Server.h"

@implementation Server {
	NSMutableArray *storage;
	dispatch_queue_t myQueue;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		storage = [[NSMutableArray alloc] init];
		[self performSelectorInBackground:@selector(processStorage) withObject:nil];
	}
	
	return self;
}

- (void)dealloc
{
	NSLog(@"Server is out");
}

- (void)processStorage
{
	while (true)
	{
		@synchronized(storage)
		{
			for (NSObject *elem in storage)
			{
				NSLog(@"The message is : %@", elem);
			}
			[storage removeAllObjects];
			sleep(1);
		}
	}
}

- (BOOL) addMessage: (NSString *) message
{
	@synchronized(self)
	{
		return [self processMessage:message];
	}
}

- (BOOL)processMessage:(NSString *)message
{
	if ([storage count] < 10)
	{
		[storage addObject:message];
		return YES;
	}
	
	return NO;
}

- (void) printMessages
{
	for (NSObject *elem in storage)
	{
		NSLog(@"The message is : %@", elem);
	}
}

@end
