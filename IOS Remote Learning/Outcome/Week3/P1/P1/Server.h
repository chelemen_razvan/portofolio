//
//  Server.h
//  Week3-1
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Server : NSObject
- (BOOL) addMessage: (NSString *) message;
- (void) printMessages;
@end
