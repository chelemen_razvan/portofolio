//
//  Client.h
//  Week3-1
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Server.h"

@interface Client : NSObject
- (void) sendMessage: (NSString *)message;
- (id) initWithServer:(Server *)s;
@end
