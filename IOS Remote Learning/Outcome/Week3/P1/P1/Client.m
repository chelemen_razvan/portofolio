//
//  Client.m
//  Week3-1
//
//  Created by Dobrean Dragos on 16/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "Client.h"
#import "Server.h"

@implementation Client {
	Server *server;
}

- (id) initWithServer: (Server *) s
{
	self = [super init];
	if (self)
	{
		server = s;
	}
	
	return self;
}

- (void) sendMessage:(NSString *)message;
{
	BOOL sent = NO;
	while (!sent)
	{
		sent = [server addMessage:message];
		if (sent)
		{
			return;
		}
		sleep(2);
	}
}

@end
