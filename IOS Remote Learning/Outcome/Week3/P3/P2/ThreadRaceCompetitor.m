//
//  ThreadRaceCompetitor.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRaceCompetitor.h"

@implementation ThreadRaceCompetitor
{
	ThreadRaceContext *_raceContext;
	NSString *_racerName;
	NSString *_teamName;
}

- (id) initWithContext: (ThreadRaceContext *) context Name: (NSString *) name
			   AndTeam: (NSString *) teamName
{
	self = [super init];
	if (self)
	{
		_raceContext = context;
		_racerName = name;
		_teamName = teamName;
	}
	return self;
}

- (void) start
{
	NSThread *thread = [[NSThread alloc] initWithTarget:self selector:@selector(notifyRaceContext) object:Nil];
	[thread start];
}

- (void) notifyRaceContext
{
	[_raceContext recordRacerRank: _racerName FromTeam: _teamName];
}

@end
