//
//  ThreadRace.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRace.h"

@implementation ThreadRace
{
	NSMutableArray *_racersArray;
	ThreadRaceContext *_raceContext;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_racersArray = [[NSMutableArray alloc] init];
		_raceContext = [[ThreadRaceContext alloc] init];
		[self createRacers];
	}
	return self;
}

- (void) startRace
{
	for (ThreadRelayRaceTeam *racer in _racersArray)
	{
		NSThread *thread = [[NSThread alloc] initWithTarget:racer selector:@selector(startTeam) object:Nil];
		[thread start];
		//[racer startTeam];
	}
}

- (void) createRacers
{
	for (int i = 0; i < NUMBER_OF_TEAMS; i++)
	{
		ThreadRelayRaceTeam *team = [[ThreadRelayRaceTeam alloc]
				initWithName:[NSString stringWithFormat:@"Team %i", i] TeamSize:[NSNumber numberWithInt:TEAM_SIZE]
				AndContext:_raceContext];
		[_racersArray addObject:team];
	}
}

@end
