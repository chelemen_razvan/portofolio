//
//  ThreadRelayRaceTeam.m
//  P3
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRelayRaceTeam.h"

@implementation ThreadRelayRaceTeam
{
	NSMutableArray *_teamRacers;
	NSString *_teamName;
	NSNumber *_teamSize;
	ThreadRaceContext *_raceContext;
}

- (id) initWithName:(NSString *) name TeamSize: (NSNumber *) size AndContext: (ThreadRaceContext *) context
{
	self = [super init];
	if (self)
	{
		_teamRacers = [[NSMutableArray alloc] init];
		_teamName = name;
		_teamSize = size;
		_raceContext = context;
		[self createRacers];
	}
	return self;
}

- (void) createRacers
{
	for (int i = 0; i < [_teamSize integerValue]; i++)
	{
		ThreadRaceCompetitor *competitor = [[ThreadRaceCompetitor alloc]
				initWithContext:_raceContext Name: [NSString stringWithFormat:@"%@ Racer %i", _teamName, i]
				AndTeam: _teamName];
		[_teamRacers addObject: competitor];
	}
}

- (void) startTeam
{
	for (ThreadRaceCompetitor *competitor in _teamRacers)
	{
		[competitor start];
	}
}

@end
