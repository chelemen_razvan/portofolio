//
//  ThreadRelayRaceTeam.h
//  P3
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRace.h"
#import "ThreadRaceCompetitor.h"

@class ThreadRaceContext;

@interface ThreadRelayRaceTeam : NSObject

- (id) initWithName:(NSString *) name TeamSize: (NSNumber *) size AndContext: (ThreadRaceContext *) context;
- (void) startTeam;

@end
