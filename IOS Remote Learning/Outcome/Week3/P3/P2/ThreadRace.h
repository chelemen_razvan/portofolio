//
//  ThreadRace.h
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRelayRaceTeam.h"
#import "ThreadRaceCompetitor.h"
#import "ThreadRaceContext.h"

static const int NUMBER_OF_RACERS = 40;
static const int TEAM_SIZE = 4;
static const int NUMBER_OF_TEAMS = 10;

@interface ThreadRace : NSObject

- (void) startRace;
@end
