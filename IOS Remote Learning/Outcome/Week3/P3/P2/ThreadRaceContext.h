//
//  ThreadRaceContext.h
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThreadRace.h"

@interface ThreadRaceContext : NSObject

- (void) recordRacerRank: (NSString *) racerName FromTeam: (NSString *) team;

@end
