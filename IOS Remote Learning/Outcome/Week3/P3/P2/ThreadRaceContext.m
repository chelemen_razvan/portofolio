//
//  ThreadRaceContext.m
//  P2
//
//  Created by razvan on 3/17/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "ThreadRaceContext.h"

@implementation ThreadRaceContext
{
	NSMutableDictionary *_racersRankings;
	NSMutableDictionary *_teamsRankings;
	int _racersPosition;
	int _teamsPosition;
	NSObject *_lock;
	NSMutableDictionary *_teamCounters;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_racersPosition = 0;
		_teamsPosition = 0;
		_racersRankings = [[NSMutableDictionary alloc] init];
		_lock = [[NSObject alloc] init];
		_teamCounters = [[NSMutableDictionary alloc] init];
		_teamsRankings = [[NSMutableDictionary alloc] init];
	}
	return self;
}

- (void) recordRacerRank: (NSString *) racerName FromTeam: (NSString *) team
{
	@synchronized(_lock)
	{
		//NSLog(@"%@", team);
		_racersPosition++;
		[_racersRankings setObject:[NSNumber numberWithInt:_racersPosition] forKey:racerName];
		
		if ([_teamCounters objectForKey: team])
		{
			NSNumber *finishedCount = [_teamCounters objectForKey: team];
			int value = [finishedCount integerValue];
			value++;
			
			if (value == TEAM_SIZE) {
				_teamsPosition++;
				[_teamsRankings setObject:[NSNumber numberWithInt:_teamsPosition] forKey:team];
			}
			else
			{
				finishedCount = [NSNumber numberWithInt: value];
				[_teamCounters setObject: finishedCount forKey: team];
		
			}
		}
		else
		{
			[_teamCounters setObject:[NSNumber numberWithInt:1] forKey:team];
		}
		
		if (_racersPosition == NUMBER_OF_RACERS)
		{
			[self showRankings];
		}
	}
}

-(void) showRankings
{
	NSLog(@"%@", [_racersRankings description]);
	NSLog(@"%@", [_teamsRankings description]);
}

@end
