//
//  AppDelegate.m
//  SampleApp
//
//  Created by Radu Stroia on 24/02/14.
//  Copyright (c) 2014 Radu Stroia. All rights reserved.
//

#import "AppDelegate.h"
#import "SampleViewController.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor blackColor];
	
	// Custom actions can be defined here.
    [self displayPersonalities];
	
	SampleViewController *sampleVC = [[SampleViewController alloc] init];
	self.window.rootViewController = sampleVC;
    [self.window makeKeyAndVisible];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)displayPersonalities
{
    NSString *pathToPlist = [[NSBundle mainBundle] pathForResource:@"Problem1Data"
                                                            ofType:@"plist"];
    
    NSArray *personalitiesArray = [[NSArray alloc] initWithContentsOfFile:pathToPlist];
    NSSet *filteredPersonalitiesSet = [[NSSet alloc] initWithArray:personalitiesArray];
 
	// TODO: REVIEW RS
	// Use classes to sort the array, not NSDictionary.
    //NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"Name" ascending:YES];
    //NSArray *sortedArray = [filteredPersonalitiesSet sortedArrayUsingDescriptors:[[NSArray alloc]
    //        initWithObjects:sortDescriptor, nil]];
    
    for (NSDictionary *personality in filteredPersonalitiesSet)
    {
        NSString *firstName = [personality objectForKey:@"Name"];
        NSString *lastName = [personality objectForKey:@" Surname"];
        NSString *dateOfBirth = [[personality objectForKey:@" Date of birth"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        NSString *dateOfDeath = [[personality objectForKey:@" Date of death"] stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
        
        if (dateOfDeath)
            NSLog(@"%@ %@ (%@-%@)", firstName, lastName, dateOfBirth, dateOfDeath);
        else
            NSLog(@"%@ %@ %@", firstName, lastName, dateOfBirth);
    }
    
}

@end
