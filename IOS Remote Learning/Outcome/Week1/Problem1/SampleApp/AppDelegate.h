//
//  AppDelegate.h
//  SampleApp
//
//  Created by Radu Stroia on 24/02/14.
//  Copyright (c) 2014 Radu Stroia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
