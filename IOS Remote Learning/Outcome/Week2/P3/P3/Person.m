//
//  Person.m
//  P3
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "Person.h"

@implementation Person


- (id) initWithDictionary: (NSDictionary *) dictionary
{
	self = [super init];
	if (self)
	{
		
		_firstName = [[dictionary objectForKey: @"Name"] retain];
		_lastName = [[dictionary objectForKey: @" Surname"] retain];
		_dateOfBirth = [[[dictionary objectForKey: @" Date of birth"]
						stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]] retain];
		_dateOfDeath = [[[dictionary objectForKey: @" Date of death"]
						stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] retain];
	}
	return self;
}

- (void)dealloc
{
	[_firstName release];
	[_lastName release];
	[_dateOfBirth release];
	[_dateOfDeath release];
	[super dealloc];
}

- (BOOL) isEqual:(id) other
{
	if (other == self)
		return YES;
	if (!other || ![other isKindOfClass: [self class]])
		return NO;
	Person *otherPerson = other;
	return [_lastName isEqual: otherPerson.lastName] && [_firstName isEqual: otherPerson.firstName] &&
			[_dateOfDeath isEqual: otherPerson.dateOfDeath] && [_dateOfBirth isEqual: otherPerson.dateOfBirth];

}

@end
