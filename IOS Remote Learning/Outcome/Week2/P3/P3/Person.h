//
//  Person.h
//  P3
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject
{
	__unsafe_unretained NSObject *obj;
}
@property(retain) NSString *firstName;
@property(retain) NSString *lastName;
@property(retain) NSString *dateOfBirth;
@property(retain) NSString *dateOfDeath;

- (id) initWithDictionary: (NSDictionary *) dictionary;

@end
