//
//  SuperclassDemo.m
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "SuperclassDemo.h"

@implementation SuperclassDemo

- (id) init
{
	NSLog(@"Superclass init called");
	return [super init];
}

- (void) dealloc
{
	NSLog(@"Superclass dealloc called");
	[super dealloc];
}

@end
