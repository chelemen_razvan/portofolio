//
//  SubclassDemo.h
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SuperclassDemo.h"
#import "FieldDemo.h"
#import "PropertyDemo.h"

@interface SubclassDemo : SuperclassDemo
{
	FieldDemo *_fieldDemo;
}

@property PropertyDemo *propertyDemo;

- (void) doStuff;

@end
