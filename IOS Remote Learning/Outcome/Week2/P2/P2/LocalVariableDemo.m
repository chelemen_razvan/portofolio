//
//  LocalVariableDemo.m
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "LocalVariableDemo.h"

@implementation LocalVariableDemo

- (id) init
{
	NSLog(@"Local variable init called");
	return [super init];
}

- (void) dealloc
{
	NSLog(@"Local variable dealloc called");
	[super dealloc];
}

@end
