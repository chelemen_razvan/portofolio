//
//  SubclassDemo.m
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "SubclassDemo.h"
#import "LocalVariableDemo.h"


@implementation SubclassDemo

static int (^staticBlock)();

+ (void) initialize
{
	NSLog(@"Creating static block");
	staticBlock = ^{NSLog(@"Static block called"); return 0;};}

- (id) init
{
	NSLog(@"Subclass init called");
	self = [super init];
	if (self)
	{
		_fieldDemo = [[FieldDemo alloc] init];
		_propertyDemo = [[PropertyDemo alloc] init];
	}
	return self;
}

- (void) dealloc
{
	NSLog(@"Subclass dealloc called");
	[_fieldDemo release];
	[_propertyDemo release];
	[super dealloc];
}

- (void) doStuff
{
	LocalVariableDemo *localVariableDemo = [[LocalVariableDemo alloc] init];
	
	NSLog(@"Doing stuff with local variable");
	
	[localVariableDemo dealloc];
	
	NSLog(@"Calling static block...");
	
	staticBlock();
	
}


@end
