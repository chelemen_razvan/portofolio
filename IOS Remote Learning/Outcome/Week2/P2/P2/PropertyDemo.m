//
//  PropertyDemo.m
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "PropertyDemo.h"

@implementation PropertyDemo

- (id) init
{
	NSLog(@"Property init called");
	return [super init];
}

- (void) dealloc
{
	NSLog(@"Property dealloc called");
	[super dealloc];
}

@end
