//
//  FieldDemo.m
//  P2
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "FieldDemo.h"

@implementation FieldDemo

- (id) init
{
	NSLog(@"Field init called");
	return [super init];
}

- (void) dealloc
{
	NSLog(@"Field dealloc called");
	[super dealloc];
}

@end
