//
//  Person.h
//  P1
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property NSString *firstName;
@property NSString *surname;

- (id) initWithFullName: (NSString *) name;
- (id) initWithFirstName: (NSString *) firstName andSurname: (NSString *) surname;

@end
