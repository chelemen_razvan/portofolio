//
//  Person.m
//  P1
//
//  Created by razvan on 3/6/14.
//  Copyright (c) 2014 razvan. All rights reserved.
//

#import "Person.h"

@implementation Person

- (id) initWithFullName: (NSString *) name
{
    self = [super init];
    if (self)
	{
		NSRange firstWhiteSpace = [name rangeOfString: @" "];
		
		_surname = [name substringToIndex: firstWhiteSpace.location];
		_firstName = [name substringFromIndex: firstWhiteSpace.location + firstWhiteSpace.length];
    }
	return self;
}

- (id) initWithFirstName: (NSString *) firstName andSurname: (NSString *) surname
{
    return [self initWithFullName: [NSString stringWithFormat: @"%@ %@", surname, firstName]];
}

@end
