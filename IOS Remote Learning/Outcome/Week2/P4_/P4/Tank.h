//
//  Tank.h
//  Week24
//
//  Created by Dobrean Dragos on 09/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tank : NSObject
{
	NSMutableArray *stack;
	int counter;
}

- (NSObject *)getObject;
- (void)putObject:(NSObject *) object;

@end
