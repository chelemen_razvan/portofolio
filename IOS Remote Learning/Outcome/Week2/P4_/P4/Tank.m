//
//  Tank.m
//  Week24
//
//  Created by Dobrean Dragos on 09/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "Tank.h"

@implementation Tank

- (id)init
{
	self = [super init];
	if (self) {
		counter = 0;
		stack = [[NSMutableArray alloc] init];
		NSLog(@"Sunt in init counterul este : %d iar count-ul este: %lu",counter,(unsigned long)[stack count]);
	}
	
	return self;
}

- (NSObject *)getObject
{
	NSObject *returnValue = [stack objectAtIndex:[stack count] - 1];
	[stack removeLastObject];
	counter --;
	NSLog(@"Sunt in getObject counterul este : %d iar count-ul este: %lu",counter,(unsigned long)[stack count]);
	return returnValue;
}

- (void)putObject:(NSObject *) object
{
	[stack addObject:object];
	counter++;
	NSLog(@"Sunt in putObject counterul este : %d iar count-ul este: %lu",counter,(unsigned long)[stack count]);
}

- (oneway void)release
{
	NSLog(@"Sunt in release counterul este : %d iar count-ul este: %lu",counter,(unsigned long)[stack count]);
	if (counter == 0) {
		[self dealloc];
	}
}

- (void)dealloc
{
	NSLog(@"Sunt in dealloc counterul este : %d iar count-ul este: %lu",counter,(unsigned long)[stack count]);
	[stack release];
	[super dealloc];
	
}

@end
