//
//  ViewController.m
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController
{
	CLLocationManager *_locationManager;
	LocationManagerDelegate *_locationManagerDelegate;
	MapDelegate *_mapDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	MKPointAnnotation *myLocation = [[MKPointAnnotation alloc] init];
	
	_locationManagerDelegate = [[LocationManagerDelegate alloc]
														initWithAnnotation:myLocation];
	[self startLocationTracking];
	
	_mapDelegate = [[MapDelegate alloc] init];
	_mapView.delegate = _mapDelegate;
	[_mapView addAnnotation:myLocation];
}

- (void) startLocationTracking
{
	if (nil == _locationManager)
	{
		_locationManager = [[CLLocationManager alloc] init];
	}
	
	_locationManager.delegate = _locationManagerDelegate;
	_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	_locationManager.distanceFilter = 5;
	
	[_locationManager startUpdatingLocation];
	
}

@end
