//
//  LocationManagerDelegate.m
//  P1
//
//  Created by razvan on 4/5/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "LocationManagerDelegate.h"

@implementation LocationManagerDelegate
{
	MKPointAnnotation *_annotation;
	CLGeocoder *_geocoder;
}

- (id) initWithAnnotation:(MKPointAnnotation *) annotation
{
	self = [super init];
	if (self)
	{
		_annotation = annotation;
		_geocoder = [[CLGeocoder alloc] init];
	}
	
	return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *location = [locations lastObject];
	NSDate *eventDate = location.timestamp;
	NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
	
	if (abs(howRecent) < 15.0)
	{
		[_annotation setCoordinate:location.coordinate];
		
		[_geocoder reverseGeocodeLocation:location completionHandler:
				^(NSArray *placemarks, NSError *error){
			if ([placemarks count] > 0)
			{
				_annotation.title = [[placemarks objectAtIndex:0] name];
				_annotation.subtitle = [NSString stringWithFormat:@"%@ %@",
										[[placemarks objectAtIndex:0] locality],
										[[placemarks objectAtIndex:0] country]];
			}
		}];
	}
}


@end
