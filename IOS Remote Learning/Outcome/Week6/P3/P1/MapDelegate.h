//
//  MapDelegate.h
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKPinAnnotationView.h>

@interface MapDelegate : NSObject <MKMapViewDelegate>

@end
