//
//  ViewController.m
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController
{
	CLLocationManager *_locationManager;
	LocationManagerDelegate *_locationManagerDelegate;
	CLLocationCoordinate2D *_coordinatesArray;
	NSArray *_locations;
	MapDelegate *_mapDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	MKPointAnnotation *myLocation = [[MKPointAnnotation alloc] init];
	
	_locationManagerDelegate = [[LocationManagerDelegate alloc]
														initWithAnnotation:myLocation];
	[self startLocationTracking];
	
	_mapDelegate = [[MapDelegate alloc] init];
	_mapView.delegate = _mapDelegate;
	[_mapView addAnnotation:myLocation];
	
	//get data from file
	NSString *path = [[NSBundle mainBundle] pathForResource:@"route"
                                                            ofType:@"json"];
	NSData *data = [[NSFileManager defaultManager] contentsAtPath:path];
	
	//parse data
	JSONParser *parser = [[JSONParser alloc] init];
	_locations = [parser locationsFromJSON:data];
	_coordinatesArray = malloc([_locations count] * sizeof(CLLocationCoordinate2D));
	int count = 0;
	
	for (CLLocation *location in _locations)
	{
		_coordinatesArray[count] = location.coordinate;
		count++;
	}
	
	//add the route to map
	MKPolyline *overlay = [MKPolyline polylineWithCoordinates:_coordinatesArray count:count];
	[_mapView addOverlay:overlay];
	free(_coordinatesArray);
	_coordinatesArray = nil;
}

- (void) startLocationTracking
{
	if (nil == _locationManager)
	{
		_locationManager = [[CLLocationManager alloc] init];
	}
	
	_locationManager.delegate = _locationManagerDelegate;
	_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	_locationManager.distanceFilter = 5;
	
	[_locationManager startUpdatingLocation];
	
}

- (void) dealloc
{
	//free(_coordinatesArray);
}

- (IBAction)buttonTouch:(id)sender
{
	//center view
	ComputeBoundingBox *boundingBoxCalculator = [[ComputeBoundingBox alloc] init];
	MKMapRect cameraPosition = [boundingBoxCalculator computeBoundingBoxForArray:_locations];
	_mapView.visibleMapRect = cameraPosition;
}

@end
