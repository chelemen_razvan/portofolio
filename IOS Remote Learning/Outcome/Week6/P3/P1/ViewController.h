//
//  ViewController.h
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapDelegate.h"
#import "LocationManagerDelegate.h"
#import "JSONParser.h"
#import "ComputeBoundingBox.h"

@interface ViewController : UIViewController

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

- (IBAction)buttonTouch:(id)sender;

@end
