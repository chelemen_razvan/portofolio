//
//  JSONParser.m
//  P1
//
//  Created by razvan on 4/5/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "JSONParser.h"

@implementation JSONParser

- (NSArray *) locationsFromJSON:(NSData *) data
{
	NSError *error = nil;
	NSDictionary *parsedData = [NSJSONSerialization JSONObjectWithData:data
															   options:0
																 error:&error];
	if (error != nil)
	{
		return nil;
	}
	
	NSDictionary *route = [[parsedData objectForKey:@"routes"] objectAtIndex:0];
	NSArray *steps = [[[route objectForKey:@"legs"] objectAtIndex:0] objectForKey:@"steps"];
	
	NSMutableArray *points = [[NSMutableArray alloc] init];
	
	for (NSDictionary *step in steps)
	{
		double lat = [[[step objectForKey:@"start_location"] objectForKey:@"lat"] floatValue];
		double lng = [[[step objectForKey:@"start_location"] objectForKey:@"lng"] floatValue];
		
		[points addObject:[[CLLocation alloc] initWithLatitude:lat longitude:lng]];
	}
	
	return points;
}

@end
