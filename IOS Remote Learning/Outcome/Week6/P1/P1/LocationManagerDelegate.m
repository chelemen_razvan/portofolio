//
//  LocationManagerDelegate.m
//  P1
//
//  Created by razvan on 4/5/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "LocationManagerDelegate.h"

@implementation LocationManagerDelegate
{
	id<MKAnnotation> _annotation;
}

- (id) initWithAnnotation:(id<MKAnnotation>) annotation
{
	self = [super init];
	if (self)
	{
		_annotation = annotation;
	}
	
	return self;
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *location = [locations lastObject];
	NSDate *eventDate = location.timestamp;
	NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
	
	if (abs(howRecent) < 15.0)
	{
		[_annotation setCoordinate:location.coordinate];
	}
}

@end
