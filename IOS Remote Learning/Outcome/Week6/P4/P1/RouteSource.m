//
//  RouteSource.m
//  P1_
//
//  Created by Radu Stroia on 02/04/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "RouteSource.h"

#define MAPS_LINK_FORMAT @"http://maps.googleapis.com/maps/api/directions/json?origin=%.6f,%.6f&destination=%.6f,%.6f&sensor=true"

@implementation RouteSource

- (void)loadRouteWithCompletionBlock:(RouteSourceCompletionBlock)completionBlock
{
	NSString *link = [NSString stringWithFormat:MAPS_LINK_FORMAT,
					  self.startLocation.coordinate.latitude,
					  self.startLocation.coordinate.longitude,
					  self.endLocation.coordinate.latitude,
					  self.endLocation.coordinate.longitude];
	
	NSURL *url = [NSURL URLWithString:link];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];
	NSURLResponse *response = nil;
	NSError *error = nil;
	NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
	
	completionBlock(result, error);
}

@end
