//
//  MapDelegate.m
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "MapDelegate.h"

@implementation MapDelegate

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
	MKPinAnnotationView *pinView = (MKPinAnnotationView *) [mapView dequeueReusableAnnotationViewWithIdentifier:@"CustomPinPoint"];
	
	if (!pinView)
	{
		pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
												  reuseIdentifier:@"CustomPinPoint"];
		pinView.pinColor = MKPinAnnotationColorPurple;
		pinView.animatesDrop = YES;
		pinView.canShowCallout = YES;
	}
	else
	{
		pinView.annotation = annotation;
	}
	
	return pinView;
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView viewForOverlay:(id<MKOverlay>)overlay
{
	if ([overlay isKindOfClass:[MKPolyline class]])
	{
		MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
		renderer.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.2];
		renderer.strokeColor = [[UIColor redColor] colorWithAlphaComponent:0.7];
		renderer.lineWidth = 3;

		return renderer;
	}

	return nil;
}

@end
