//
//  JSONParser.h
//  P1
//
//  Created by razvan on 4/5/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface JSONParser : NSObject

- (NSArray *) locationsFromJSON:(NSData *) data;

@end
