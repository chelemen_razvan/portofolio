//
//  ViewController.m
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController
{
	CLLocationManager *_locationManager;
	LocationManagerDelegate *_locationManagerDelegate;
	CLLocationCoordinate2D *_coordinatesArray;
	NSArray *_locations;
	NSOperationQueue *_operationQueue;
	CLLocation *_endLocation;
	MKPolyline *_overlay;
	MapDelegate *_mapDelegate;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	//init the operation queue
	_operationQueue = [[NSOperationQueue alloc] init];
	_operationQueue.maxConcurrentOperationCount = 1;
	
	//init end location
	_endLocation = [[CLLocation alloc] initWithLatitude:46.768879 longitude:23.584081];
	//_endLocation = [[CLLocation alloc] initWithLatitude:37.332331 longitude:-122.031219];
	
	MKPointAnnotation *myLocation = [[MKPointAnnotation alloc] init];
	
	_locationManagerDelegate = [[LocationManagerDelegate alloc]
								initWithAnnotation:myLocation AndNotifier: self];
	[self startLocationTracking];
	
	_mapDelegate = [[MapDelegate alloc] init];
	_mapView.delegate = _mapDelegate;
	[_mapView addAnnotation:myLocation];
	
	//disable center button until first route is displayed
	[_centerCameraButton setEnabled:NO];
}

- (void) startLocationTracking
{
	if (nil == _locationManager)
	{
		_locationManager = [[CLLocationManager alloc] init];
	}
	
	_locationManager.delegate = _locationManagerDelegate;
	_locationManager.desiredAccuracy = kCLLocationAccuracyBest;
	_locationManager.distanceFilter = 20;
	
	[_locationManager startUpdatingLocation];
}

- (void) dealloc
{
	free(_coordinatesArray);
}

- (IBAction)buttonTouch:(id)sender
{
	//center camera
	ComputeBoundingBox *boundingBoxCalculator = [[ComputeBoundingBox alloc] init];
	MKMapRect cameraPosition = [boundingBoxCalculator computeBoundingBoxForArray:_locations];
	_mapView.visibleMapRect = cameraPosition;
}

- (void) locationDidChange:(CLLocation *) location
{
	
	[_operationQueue cancelAllOperations];
	[_operationQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
		RouteSource *source = [[RouteSource alloc] init];
		source.startLocation = location;
		source.endLocation = _endLocation;
		
		//remove previous overlay
		[_mapView performSelectorOnMainThread:@selector(removeOverlay:) withObject:_overlay
								waitUntilDone:NO];
		free(_coordinatesArray);
		
		__block int count = 0;
		[source loadRouteWithCompletionBlock:^(NSData *result, NSError *error) {
			//parse data
			_locations = [[[JSONParser alloc] init] locationsFromJSON:result];
			_coordinatesArray = malloc([_locations count] * sizeof(CLLocationCoordinate2D));
			
			if (_locations != nil)
			for (CLLocation *location in _locations)
			{
				_coordinatesArray[count] = location.coordinate;
				count++;
			}
		}];
		
		if (count > 0)
		{
			//add the route to map
			_overlay = [MKPolyline polylineWithCoordinates:_coordinatesArray count:count];
			[_mapView performSelectorOnMainThread:@selector(addOverlay:) withObject:_overlay waitUntilDone:NO];
		
			//enable center camera button
			[self performSelectorOnMainThread:@selector(enableCenterCameraButton) withObject:nil waitUntilDone:NO];
		}
	}]];

}

		
- (void) enableCenterCameraButton
{
	[_centerCameraButton setEnabled:YES];
}
		
@end
