//
//  ViewController.h
//  P1
//
//  Created by razvan on 4/4/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapDelegate.h"
#import "LocationManagerDelegate.h"
#import "JSONParser.h"
#import "ComputeBoundingBox.h"
#import "LocationChangeNotifier.h"
#import "RouteSource.h"

@interface ViewController : UIViewController <LocationChangeNotifier>

@property (strong, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) IBOutlet UIButton *centerCameraButton;

- (IBAction)buttonTouch:(id)sender;

@end
