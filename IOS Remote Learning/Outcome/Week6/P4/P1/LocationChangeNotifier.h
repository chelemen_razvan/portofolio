//
//  LocationChangeNotifier.h
//  P1
//
//  Created by razvan on 4/5/14.
//  Copyright (c) 2014 Test. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@protocol LocationChangeNotifier <NSObject>

- (void) locationDidChange:(CLLocation *) location;

@end
