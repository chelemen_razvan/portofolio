//
//  EntryFetcher.h
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^requestElements)(NSMutableArray *array);

@protocol EntryFetcher <NSObject>

@required
- (NSArray *) getElements;
- (void) getElementsWithCallback:(requestElements)block;

@end
