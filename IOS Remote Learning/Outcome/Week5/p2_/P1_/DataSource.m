//
//  DataSource.m
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "DataSource.h"
#import "Entity.h"
#import "RSSFetcher.h"

@implementation DataSource
{
	NSArray *_feedsArray;
	RSSFetcher *_fetcher;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		_fetcher = [[RSSFetcher alloc] init];
	}
	return self;
}

- (void)loadWithCallback:(DataSourceCallback)callback
{
	_feedsArray = [_fetcher getElements];
	callback();
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _feedsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	
	cell.textLabel.text = [[_feedsArray objectAtIndex:indexPath.row] title];
    return cell;
}

- (NSString *)getUrlAtRow:(NSUInteger)no
{
	return [[_feedsArray objectAtIndex:no] link];
}

@end
