//
//  RSSFetcher.m
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "RSSFetcher.h"

@implementation RSSFetcher
{
	NSXMLParser *_parser;
    NSMutableArray *_feedsArray;
    NSMutableString *_title;
    NSMutableString *_link;
    NSString *_element;
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    _element = elementName;
    
    if ([_element isEqualToString:@"item"])
	{
        _title   = [[NSMutableString alloc] init];
        _link    = [[NSMutableString alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if ([_element isEqualToString:@"title"])
	{
        [_title appendString:string];
    }
	else if ([_element isEqualToString:@"link"])
	{
        [_link appendString:string];
    }
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([elementName isEqualToString:@"item"])
	{
        
        Entity *elem = [[Entity alloc] initWithTitle:_title Link:_link];
		
        [_feedsArray addObject:elem];
		
    }
}

- (NSArray *) getElements
{
	_feedsArray = [[NSMutableArray alloc] init];
    NSURL *url = [NSURL URLWithString:@"http://images.apple.com/main/rss/hotnews/hotnews.rss"];
    _parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
	
    [_parser setDelegate:self];
    [_parser setShouldResolveExternalEntities:NO];
    [_parser parse];

	return _feedsArray;
}

@end
