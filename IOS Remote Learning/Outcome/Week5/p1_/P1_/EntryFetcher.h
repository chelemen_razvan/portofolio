//
//  EntryFetcher.h
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol EntryFetcher <NSObject>

@required
- (NSArray *) getElements;

@end
