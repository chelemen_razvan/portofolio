//
//  MasterViewController.m
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "MasterViewController.h"


@interface MasterViewController ()
{
	DataSource *_dataSource;
}
@end

@implementation MasterViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	_dataSource = [[DataSource alloc] init];
	_dataSource.delegate = self;
	self.tableView.dataSource = _dataSource;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[_dataSource load];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
	{
		
        [[segue destinationViewController] setUrl:[_dataSource getUrlAtRow:[self.tableView indexPathForSelectedRow].row]];
	}
}

#pragma mark - DataSourceDelegate
- (void)didLoad
{
	[self.tableView reloadData];
}

@end
