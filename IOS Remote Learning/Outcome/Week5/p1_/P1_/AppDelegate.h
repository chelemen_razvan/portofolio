//
//  AppDelegate.h
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
