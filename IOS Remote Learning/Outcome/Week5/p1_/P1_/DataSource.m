//
//  DataSource.m
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "DataSource.h"
#import "Entity.h"
#import "RSSFetcher.h"

@implementation DataSource
{
	NSArray *_feedsArray;
	RSSFetcher *_entryFetcher;
}

- (id)init
{
	self = [super init];
	if (self != nil)
	{
		_entryFetcher = [[RSSFetcher alloc] init];
	}
	return self;
}

- (id)initWithElements:(NSArray*)elements
{
	self = [super init];
	if (self)
	{
		_feedsArray = elements;
	}
	return self;
}

- (void)load
{
	_feedsArray = [_entryFetcher getElements];
	[self.delegate didLoad];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return _feedsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	
	cell.textLabel.text = [[_feedsArray objectAtIndex:indexPath.row] title];
    return cell;
}

- (NSString *)getUrlAtRow:(NSUInteger)no
{
	return [[_feedsArray objectAtIndex:no] link];
}
@end
