//
//  DataSource.h
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataSourceProtocol <NSObject>

- (void)didLoad;

@end

@interface DataSource : NSObject <UITableViewDataSource>

@property (nonatomic, weak) id<DataSourceProtocol> delegate;

- (void)load;



- (id)initWithElements:(NSArray*)elements;
- (NSString *)getUrlAtRow:(NSUInteger)no;
@end
