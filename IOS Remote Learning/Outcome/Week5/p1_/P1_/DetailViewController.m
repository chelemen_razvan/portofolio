//
//  DetailViewController.m
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "DetailViewController.h"

@implementation DetailViewController


- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem)
	{
        _detailItem = newDetailItem;
        [self configureView];
    }
}

- (void)configureView
{
	if (self.detailItem)
	{
	    self.detailDescriptionLabel.text = [self.detailItem description];
	}
}

- (id)initWithUrl:(NSString *)url
{
	self = [super init];
	if (self)
	{
		_url=url;
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	NSURL *myURL = [NSURL URLWithString: [self.url stringByAddingPercentEscapesUsingEncoding:
										  NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:myURL];
    [self.webView  loadRequest:request];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	NSLog(@"Error : %@",error);
}

@end
