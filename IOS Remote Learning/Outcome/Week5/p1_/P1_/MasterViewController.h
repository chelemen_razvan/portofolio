//
//  MasterViewController.h
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "DetailViewController.h"
#import "EntryFetcher.h"
#import "RSSFetcher.h"
#import "DataSource.h"

@interface MasterViewController : UITableViewController <NSXMLParserDelegate, DataSourceProtocol>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
	
@end
