//
//  Entity.m
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "Entity.h"

@implementation Entity

- (id)initWithTitle:(NSMutableString*)title Link:(NSMutableString*)link
{
	self = [super init];
	if (self)
	{
		_title = title;
		_link = link;
	}
	
	return self;
}

@end
