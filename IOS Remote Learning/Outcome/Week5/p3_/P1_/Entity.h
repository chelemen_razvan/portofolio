//
//  Entity.h
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Entity : NSObject
@property (strong, nonatomic) NSMutableString *link;
@property (strong, nonatomic) NSMutableString *title;

- (id)initWithTitle:(NSMutableString*)title Link:(NSMutableString*)link;

@end
