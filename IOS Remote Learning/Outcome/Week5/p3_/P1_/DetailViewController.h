//
//  DetailViewController.h
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface DetailViewController : UIViewController 

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (copy, nonatomic) NSString *url;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

- (id)initWithUrl:(NSString *)url;

@end
