//
//  MasterViewController.m
//  P1_
//
//  Created by Dobrean Dragos on 30/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "MasterViewController.h"


@interface MasterViewController ()
{
    NSArray *_feedsArray;
	DataSource *_dataArray;
}

@end

@implementation MasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	_dataArray = [DataSource new];
	self.tableView.dataSource = _dataArray;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[_dataArray loadWithCallback:^{
		[self.tableView reloadData];
	}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
	{
		
        [[segue destinationViewController] setUrl:[_dataArray getUrlAtRow:[self.tableView indexPathForSelectedRow].row]];
	}
}

@end
