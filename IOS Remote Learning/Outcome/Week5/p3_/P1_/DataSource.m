//
//  DataSource.m
//  P1_
//
//  Created by Dobrean Dragos on 31/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "DataSource.h"
#import "Entity.h"
#import "RSSFetcher.h"

@implementation DataSource
{
	NSArray *feeds;
	RSSFetcher *_fetcher;
}

- (id)init
{
	self = [super init];
	if (self)
	{
		_fetcher = [RSSFetcher new];
	}
	return self;
}

- (void)loadWithCallback:(DataSourceCallback)callback
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
		feeds = [_fetcher getElements];
		sleep(3);
		
		dispatch_async(dispatch_get_main_queue(),callback);
	});
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return feeds.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	
	cell.textLabel.text = [[feeds objectAtIndex:indexPath.row] title];
    return cell;
}

- (NSString *)getUrlAtRow:(NSUInteger)no
{
	return [[feeds objectAtIndex:no] link];
}
@end
