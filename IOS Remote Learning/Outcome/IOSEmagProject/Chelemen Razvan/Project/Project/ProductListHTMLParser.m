//
//  ProductListHTMLParser.m
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ProductListHTMLParser.h"

#define ADDRESS_ROOT @"http://m.emag.ro"

#define ITEM_CONTAINER_XPATH @"//a[@class='product-container']"
#define IMAGE_LINK_XPATH @"//div[@class='img-container square']/img/@src | //div[@class='img-container square']/img/@data-src"
#define TITLE_XPATH @"//span[@class='product-info-lite']/span[@class='product-title'] | //span[@class='product-info-lite']/span[@class='product-title product-title-with-family']"
#define PRICE_XPATH @"//span[@class='price-over']"
#define DETAILS_LINK_PAGE_XPATH @"//a[@class='product-container']/@href"

@implementation ProductListHTMLParser

- (NSArray *) parseResultsPage:(NSData *)data
{
	TFHpple *htmlParser = [TFHpple hppleWithHTMLData:data];
    NSArray *nodes = [htmlParser searchWithXPathQuery:ITEM_CONTAINER_XPATH];
	int elementsNumber = (int) [nodes count];
	
	NSMutableArray *productsArray = [[NSMutableArray alloc] init];
	
	//this method assumes that each product has all fields
	for (int i = 0; i < elementsNumber; i++)
	{
		Product *product = [[Product alloc] init];
		product.imageLink = [self getImageLink:htmlParser AtIndex:i];
		product.title = [self getTitle:htmlParser AtIndex:i];
		product.price = [self getPrice:htmlParser AtIndex:i];
		product.detailsPageLink = [self getDetailsPageLink:htmlParser AtIndex:i];
		
		//if you get a nil value it means that there is no tag for current product
		//and this may gave some inconsistent products
		if ((product.imageLink == nil) || (product.title == nil) ||
				(product.price == nil) || (product.detailsPageLink == nil))
			return nil;
		
		[productsArray addObject:product];
	}
	
	return productsArray;
}

- (NSString *) getImageLink:(TFHpple *) parser AtIndex:(NSUInteger) index
{
    NSArray *nodes = [parser searchWithXPathQuery:IMAGE_LINK_XPATH];
    
    if ([nodes count] <= index)
		return nil;
	
	TFHppleElement *element = [nodes objectAtIndex:index];
	NSString *imageLink = [[[element firstChild] content] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	return imageLink;
}

- (NSString *) getTitle:(TFHpple *) parser AtIndex:(NSUInteger) index
{
    NSArray *nodes = [parser searchWithXPathQuery:TITLE_XPATH];
    
    if ([nodes count] <= index)
		return nil;
	
	TFHppleElement *element = [nodes objectAtIndex:index];
	NSString *tagContent = [self removeTabCharacters:[self removeTags:[element raw]]];
	
	return [tagContent stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *) getPrice:(TFHpple *) parser AtIndex:(NSUInteger) index
{
    NSArray *priceNodes = [parser searchWithXPathQuery:PRICE_XPATH];
	
    if ([priceNodes count] <= index)
		return nil;
	
	TFHppleElement *element = [priceNodes objectAtIndex:index];
	
	NSString *intPrice = [self removeTags:[[element firstChildWithClassName:@"money-int"] raw]];
	NSString *decimalPrice = [self removeTags:[[element firstChildWithClassName:@"money-decimal"] raw]];
	NSString *currency = [self removeTags:[[element firstChildWithClassName:@"money-currency"] raw]];
	
	if ((intPrice == nil) && (decimalPrice == nil) && (currency == nil))
		return nil;
	
	//if there is no decimal part in price use this format
	if (decimalPrice == nil || [decimalPrice isEqualToString:@""])
	{
		NSString *price = [NSString stringWithFormat:@"%@ %@", intPrice, currency];
		//if price and currency are empty return empty string
		if ([price isEqualToString:@" "])
			return @"";
		
		return price;
	}
	
	NSString *price = [NSString stringWithFormat:@"%@,%@ %@", intPrice, decimalPrice, currency];
	return price;
}

- (NSString *) getDetailsPageLink:(TFHpple *) parser AtIndex:(NSUInteger) index
{
    NSArray *nodes = [parser searchWithXPathQuery:DETAILS_LINK_PAGE_XPATH];
    
    if ([nodes count] <= index)
		return nil;
	
	TFHppleElement *element = [nodes objectAtIndex:index];
	NSString *rawPath = [[element firstChild] content];
	
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@".*\?url=" options:NSRegularExpressionCaseInsensitive error:nil];
	NSRange range = NSMakeRange(0, rawPath.length);
    
	NSString *link = [regex stringByReplacingMatchesInString:rawPath options:0 range:range withTemplate:ADDRESS_ROOT];
	return link;
}

- (NSString *) removeTags:(NSString *) text
{
	if (text == nil)
		return nil;
	
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<[0-9 a-z/\"=-]*>" options:NSRegularExpressionCaseInsensitive error:nil];
    NSRange range = NSMakeRange(0, text.length);
    
    NSString *cleanText = [regex stringByReplacingMatchesInString:text options:0 range:range withTemplate:@""];
	
    return cleanText;
}

- (NSString *) removeTabCharacters:(NSString *) text
{
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\t\n ]+" options:NSRegularExpressionCaseInsensitive error:nil];
	NSRange range = NSMakeRange(0, text.length);
	
	NSString *cleanText = [regex stringByReplacingMatchesInString:text options:0 range:range withTemplate:@" "];
	
	return cleanText;
}

@end
