//
//  AppDelegate.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-22.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
