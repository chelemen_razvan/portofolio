//
//  HTMLLoader.m
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "HTMLLoader.h"

@implementation HTMLLoader

-(void) loadHTMLForURL:(NSString *) link WithCompletionBlock:(HTMLLoaderCompletionBlock)completionBlock
{
    NSURL *url = [NSURL URLWithString:link];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    completionBlock(result, error);
}

@end
