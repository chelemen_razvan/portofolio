//
//  ProductDetailsImageSource.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-25.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ImageSourceCompletionBlock)(UIImage *image);

@interface ImageSource : NSObject

- (void) getImageForURL:(NSString *) URL WithCompletionBlock:(ImageSourceCompletionBlock) block;

@end
