//
//  ProductListHTMLParser.h
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Product.h"
#import "TFHpple.h"
#import "TFHppleElement.h"

@interface ProductListHTMLParser : NSObject

- (NSArray *) parseResultsPage:(NSData *)data;

@end
