//
//  ProductTableViewCell.h
//  Project
//
//  Created by razvan on 5/5/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *title;
@property (strong, nonatomic) IBOutlet UILabel *price;

@end
