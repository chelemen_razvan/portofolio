//
//  ProductDetailsSource.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDetailsHandler.h"
#import "HTMLLoader.h"
#import "ProductDetailsParser.h"

@interface ProductDetailsSource : NSObject

- (void) getProductDetailsFor:(NSString *) location WithCallback:(id<ProductDetailsHandler>) callback;

@end
