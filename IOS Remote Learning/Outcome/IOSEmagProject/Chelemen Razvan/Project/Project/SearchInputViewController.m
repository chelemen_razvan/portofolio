//
//  SearchInputViewController.m
//  Project
//
//  Created by razvan on 5/2/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "SearchInputViewController.h"

@interface SearchInputViewController ()

@end

@implementation SearchInputViewController


- (IBAction)searchButtonTuch:(id)sender
{
	if ([ValidateSearchKeyword validateKeyword:_searchTextField.text])
	{
		[self performSegueWithIdentifier:@"searchToListSegue" sender:self];
	}
	else
	{
		//show error pop up
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Search field cannot be empty or contain special characters" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert show];
	}
	
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"searchToListSegue"])
	{
		SearchResultsViewController *destinationController = [segue destinationViewController];
		[destinationController setSearchPhrase:[_searchTextField text]];
	}
}

@end
