//
//  SearchResultsViewController.h
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchService.h"
#import "ImageSource.h"
#import "ProductTableViewCell.h"
#import "ProductDetailsViewController.h"

@interface SearchResultsViewController : UITableViewController

@property (nonatomic, strong) NSString *searchPhrase;

@end
