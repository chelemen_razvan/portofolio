//
//  ViewController.m
//  Project
//
//  Created by Razvan Chelemen on 2014-04-22.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ProductDetailsViewController.h"


@implementation ProductDetailsViewController
{
    ProductDetailsSource *_detailsSource;
    ImageSource *_imageSource;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		_imageSource = [[ImageSource alloc] init];
		_detailsSource = [[ProductDetailsSource alloc] init];
	}
	
	return self;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];

    [_detailsSource getProductDetailsFor:_detailsURL WithCallback:self];
}


- (void) productDetailsReceived:(ProductDetails *)productDetails error:(NSError *)error
{
    if (error != nil)
    {
		[self.errorLabel setText:[NSString stringWithFormat:@"Error: %@", [error localizedDescription]]];
		return ;
    }
	
	[self populateUI:productDetails];
}

- (void) populateUI:(ProductDetails *) productDetails
{

    if (productDetails.title != nil)
        [self.titleLabel setText:productDetails.title];
    
    if (productDetails.price != nil)
        [self.priceLabel setText:[NSString stringWithFormat:@"Pret: %@", productDetails.price]];
        
    NSMutableString *delivery = [[NSMutableString alloc]init];
    if (productDetails.availability != nil)
        [delivery appendFormat:@"%@ | ", productDetails.availability];
    if (productDetails.seller != nil)
        [delivery appendFormat:@"%@ | ", productDetails.seller];
    if (productDetails.shippingType != nil)
        [delivery appendFormat:@"%@", productDetails.shippingType];
    [self.deliveryDetailsLabel setText:delivery];
    
    
    NSMutableString *specs = [[NSMutableString alloc] init];
    
    for (id key in productDetails.specifications)
    {
        [specs appendFormat:@"%@\n", key];
    
        for (NSString *line in [productDetails.specifications objectForKey:key]) {
            [specs appendFormat:@"%@\n", line];
        }
    
    }
        
    NSString *displayString = nil;
    if (productDetails.description != nil)
        displayString = [NSString stringWithFormat:@"Descriere:\n%@\n\nSpecificatii:\n%@", productDetails.description, specs];
    else
        displayString = [NSString stringWithFormat:@"Specificatii:\n%@", specs];
        
    [self.descriptionTextView setText:displayString];
        
        
    [_imageSource getImageForURL:productDetails.imageLink WithCompletionBlock:^(UIImage *image){
        [self.productImageView setImage:image];
        }];
    
    [self.loadingSpinner stopAnimating];
}

#pragma mark - Navigation

- (IBAction)goBack:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

@end
