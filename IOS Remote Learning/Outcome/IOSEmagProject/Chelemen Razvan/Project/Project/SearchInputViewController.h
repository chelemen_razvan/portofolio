//
//  SearchInputViewController.h
//  Project
//
//  Created by razvan on 5/2/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResultsViewController.h"
#import "ValidateSearchKeyword.h"

@interface SearchInputViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *searchTextField;

@end
