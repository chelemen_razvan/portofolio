//
//  ProductDetailsSource.m
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ProductDetailsSource.h"

@implementation ProductDetailsSource
{
    NSOperationQueue *_operationQueue;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        _operationQueue = [[NSOperationQueue alloc] init];
    }
    return self;
}

- (void) getProductDetailsFor:(NSString *) location WithCallback:(id<ProductDetailsHandler>) callback
{
    
    [_operationQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
        //load page
        HTMLLoader *loader = [[HTMLLoader alloc] init];
        [loader loadHTMLForURL:location WithCompletionBlock:^(NSData *data, NSError *error) {
            if (error != nil)
            {
				dispatch_async(dispatch_get_main_queue(),^{
                    [callback productDetailsReceived:nil error:error];
                });
			}
			//parse it
			ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
			ProductDetails *productDetails = [parser parseProductPage:data];
			//send product on the main thread
			dispatch_async(dispatch_get_main_queue(),^{
				[callback productDetailsReceived:productDetails error:error];
			});

        }];
    }]];
}

@end
