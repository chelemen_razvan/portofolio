//
//  ProductDetailsParser.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductDetails.h"
#import "TFHpple.h"

@interface ProductDetailsParser : NSObject

- (ProductDetails *) parseProductPage:(NSData *)data;

@end
