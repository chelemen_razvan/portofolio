//
//  ProductDetailsImageSource.m
//  Project
//
//  Created by Razvan Chelemen on 2014-04-25.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ImageSource.h"

@implementation ImageSource
{
    NSOperationQueue *_operationQueue;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        _operationQueue = [[NSOperationQueue alloc] init];
    }
    return self;
}

- (void) getImageForURL:(NSString *) url WithCompletionBlock:(ImageSourceCompletionBlock) block
{
    [_operationQueue addOperationWithBlock:^{
        NSData *rawData = [NSData dataWithContentsOfURL:[NSURL URLWithString:url]];
        UIImage *image = [UIImage imageWithData:rawData];

        dispatch_async(dispatch_get_main_queue(),^{
            block(image);
        });
    }];

}

@end
