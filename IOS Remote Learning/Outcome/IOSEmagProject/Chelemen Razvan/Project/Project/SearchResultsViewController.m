//
//  SearchResultsViewController.m
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "SearchResultsViewController.h"

@implementation SearchResultsViewController
{
	SearchService *_searchService;
	ImageSource *_imageSource;
	NSArray *_productArray;
}

- (id) initWithCoder:(NSCoder *)aDecoder
{
	self = [super initWithCoder:aDecoder];
	if (self)
	{
		_searchService = [[SearchService alloc] init];
		_imageSource = [[ImageSource alloc] init];
	}
	
	return self;
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[_searchService getProductsForQuery:_searchPhrase WithCompletionBlock:^(NSArray *data, NSError *error) {
		if (error != nil)
		{
			//show error pop up
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert show];
			
			//go to home screen
			[self goBack:nil];
			
			return;
		}

		_productArray = data;
		self.tableView.dataSource = self;
		[self.tableView reloadData];
	}];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_productArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    Product *product = [_productArray objectAtIndex:indexPath.row];
	
	//NSLog(@"%@", product.imageLink);
	
    cell.title.text = product.title;
	cell.price.text = product.price;

	[_imageSource getImageForURL:product.imageLink WithCompletionBlock:^(UIImage *image) {
		cell.imageView.image = image;
		[cell setNeedsLayout];
	}];
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"productListToDetailsSeague"])
	{
		ProductDetailsViewController *destinationController = [segue destinationViewController];
		NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
		Product *selectedProduct = [_productArray objectAtIndex:indexPath.row];
		
		destinationController.detailsURL = selectedProduct.detailsPageLink;
	}
}

- (IBAction)goBack:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

@end
