//
//  ProductTableViewCell.m
//  Project
//
//  Created by razvan on 5/5/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ProductTableViewCell.h"

@implementation ProductTableViewCell

- (void) layoutSubviews
{
	//for some reason setting this in storyboard doesn't work
	[super layoutSubviews];
	self.imageView.bounds = CGRectMake(15, 8, 50, 50);
	self.imageView.frame = CGRectMake(15, 8, 50, 50);
	self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

@end
