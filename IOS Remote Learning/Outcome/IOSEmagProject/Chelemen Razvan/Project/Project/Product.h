//
//  Product.h
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *detailsPageLink;
@end
