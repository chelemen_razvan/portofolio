//
//  ValidateSearchKeyword.h
//  Project
//
//  Created by razvan on 5/6/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ValidateSearchKeyword : NSObject

+ (BOOL) validateKeyword:(NSString *) keyWord;

@end
