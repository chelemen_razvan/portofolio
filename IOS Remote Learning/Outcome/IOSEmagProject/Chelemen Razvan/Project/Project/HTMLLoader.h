//
//  HTMLLoader.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^HTMLLoaderCompletionBlock)(NSData *data, NSError *error);

@interface HTMLLoader : NSObject

-(void) loadHTMLForURL:(NSString *) url WithCompletionBlock:(HTMLLoaderCompletionBlock)completionBlock;

@end
