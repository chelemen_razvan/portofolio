//
//  ProductDetailsParser.m
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ProductDetailsParser.h"

#define IMAGE_LINK_XPATH @"//div[@class='carousel-item photo item item1']/span/@href"
#define PRICE_XPATH @"//p[@class='price']"
#define TITLE_XPATH @"//h3[@class='product-name']"
#define SELLER_XPATH @"//div[@class='vendor-info pull-left']/strong"
#define AVAILABILITY_XPATH @"//p[@class='stoc']"
#define DESCRIPTION_XPATH @"//div[@id='short-description']"
#define SPECIFICATIONS_XPATH @"//div[@class='specs specs-full info-box']/div[@class='group']"
#define SHIPPING_TYPE_XPATH @"//strong[@class='free']"

@implementation ProductDetailsParser

- (ProductDetails *) parseProductPage:(NSData *)data
{
    TFHpple *htmlParser = [TFHpple hppleWithHTMLData:data];
    
    ProductDetails *productDetails = [[ProductDetails alloc] init];
    productDetails.imageLink = [self getImageLink:htmlParser];
    productDetails.price = [self getPrice:htmlParser];
    productDetails.title = [self getTitle:htmlParser];
    productDetails.seller = [self getSeller:htmlParser];
    productDetails.availability = [self getAvailability:htmlParser];
    productDetails.description = [self getDescription:htmlParser];
    productDetails.specifications = [self getSpecifications:htmlParser];
    productDetails.shippingType = [self getShippingType:htmlParser];
    
    return productDetails;
}

- (NSString *) getImageLink:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:IMAGE_LINK_XPATH];
    
    if ([nodes count] == 0)
		return nil;
	
	TFHppleElement *element = [nodes objectAtIndex:0];
	NSString *imageLink = [[[element firstChild] content] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	
	return imageLink;
}

- (NSString *) getPrice:(TFHpple *) parser
{
    NSArray *priceNodes = [parser searchWithXPathQuery:PRICE_XPATH];

	if ([priceNodes count] == 0)
		return nil;

	TFHppleElement *element = [priceNodes objectAtIndex:0];
	
	NSString *intPrice = [self removeTags:[[element firstChildWithClassName:@"money-int"] raw]];
	NSString *decimalPrice = [self removeTags:[[element firstChildWithClassName:@"money-decimal"] raw]];
	NSString *currency = [self removeTags:[[element firstChildWithClassName:@"money-currency"] raw]];
	
	if ((intPrice == nil) && (decimalPrice == nil) && (currency == nil))
		return nil;
	
	//if there is no decimal part in price use this format
	if (decimalPrice == nil || [decimalPrice isEqualToString:@""])
	{
		NSString *price = [NSString stringWithFormat:@"%@ %@", intPrice, currency];
		//if price and currency are empty return empty string
		if ([price isEqualToString:@" "])
			return @"";
		
		return price;
	}
	
	NSString *price = [NSString stringWithFormat:@"%@,%@ %@", intPrice, decimalPrice, currency];
	return price;
}

- (NSString *) getTitle:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:TITLE_XPATH];
    
	if ([nodes count] == 0)
		return nil;
	
    TFHppleElement *element = [nodes objectAtIndex:0];
    NSString *rawTitle = [element raw];
    NSString *cleanTitle = [self removeTags:rawTitle];
    
    return cleanTitle;
}

- (NSString *) getSeller:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:SELLER_XPATH];
    
	if ([nodes count] == 0)
		return nil;
	
    TFHppleElement *element = [nodes objectAtIndex:0];
	return [[[element firstChild] content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *) getAvailability:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:AVAILABILITY_XPATH];
    
	if ([nodes count] == 0)
		return nil;
	
    TFHppleElement *element = [nodes objectAtIndex:0];
    return [[[element firstChild] content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSString *) getDescription:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:DESCRIPTION_XPATH];
    
	if ([nodes count] == 0)
		return nil;
	
	TFHppleElement *element = [nodes objectAtIndex:0];
	NSMutableString *description = [[NSMutableString alloc] init];
        
	//concatenate all its children
	for (TFHppleElement *child in [element children])
	{
		if ([child content] != nil)
		{
			NSString *trimmed = [[child content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			[description appendString:trimmed];
		}
		
		NSString *content = [self concatenateChildTagsContent:child];
		
		if (content != nil)
			[description appendString:content];
	}
    
	//get rid of extra white space
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[  \t]+\n" options:NSRegularExpressionCaseInsensitive error:nil];
	NSRange range = NSMakeRange(0, description.length);
    
	NSString *cleanDescription = [regex stringByReplacingMatchesInString:description options:0 range:range withTemplate:@""];
        
	//fix paragraphs indentation
	regex = [NSRegularExpression regularExpressionWithPattern:@"[\t]+" options:NSRegularExpressionCaseInsensitive error:nil];
	range = NSMakeRange(0, cleanDescription.length);
        
	cleanDescription = [regex stringByReplacingMatchesInString:cleanDescription options:0 range:range withTemplate:@"\t"];
        
	return cleanDescription;
}

- (NSDictionary *) getSpecifications:(TFHpple *) parser
{
    NSMutableDictionary *specs = [[NSMutableDictionary alloc] init];
    NSArray *nodes = [parser searchWithXPathQuery:SPECIFICATIONS_XPATH];
    
    //for each tag found
    for (TFHppleElement *element in nodes)
    {
        //if it has a title
        if ([[element childrenWithTagName:@"h5"] count] > 0)
        {
            TFHppleElement *titleNode = [[element childrenWithTagName:@"h5"] objectAtIndex:0];
            NSString *title = [titleNode text];
        
            NSMutableArray *lines = [[NSMutableArray alloc] init];
            TFHppleElement *linesNode = [[element childrenWithTagName:@"ul"] objectAtIndex:0];
            //add li tags content
            for (TFHppleElement *child in [linesNode children])
            {
                if ([child raw] != nil)
                {
                    NSString *line = [self removeTags:[child raw]];
                    [lines addObject:line];
                }
            }
			
			//if it doesn't have a section put an empty one
			if (title == nil)
			{
				title = @"";
			}
			
            [specs setObject:lines forKey:title];
        }
    }
    
    return specs;
}

- (NSString *) getShippingType:(TFHpple *) parser
{
    NSArray *nodes = [parser searchWithXPathQuery:SHIPPING_TYPE_XPATH];
    
	if ([nodes count] == 0)
	{
		return nil;
	}
	
	TFHppleElement *element = [nodes objectAtIndex:0];
	return [[element firstChild] content];
}

- (NSString *) removeTags:(NSString *) text
{
	if (text == nil)
		return nil;
	
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<[0-9 a-z/\"=-]*>" options:NSRegularExpressionCaseInsensitive error:nil];
    NSRange range = NSMakeRange(0, text.length);
    
    NSString *cleanText = [regex stringByReplacingMatchesInString:text options:0 range:range withTemplate:@""];

    return cleanText;
}

- (NSString *) concatenateChildTagsContent:(TFHppleElement *) parent
{
	if (![parent hasChildren])
		return nil;
	
	NSMutableString *description = [[NSMutableString alloc] init];
	for (TFHppleElement *child in [parent children])
	{
		if ([child content] != nil)
		{
			NSString *trimmed = [[child content] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
			[description appendString:trimmed];
		}
	}
	
	return description;
}

@end
