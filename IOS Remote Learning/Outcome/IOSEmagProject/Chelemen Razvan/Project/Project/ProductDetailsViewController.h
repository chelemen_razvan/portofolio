//
//  ViewController.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-22.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDetailsHandler.h"
#import "ProductDetails.h"
#import "ProductDetailsSource.h"
#import "ImageSource.h"

@interface ProductDetailsViewController : UIViewController <ProductDetailsHandler>

@property (strong, nonatomic) IBOutlet UIImageView *productImageView;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UILabel *priceLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryDetailsLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadingSpinner;
@property (strong, nonatomic) IBOutlet UILabel *errorLabel;

@property (strong, nonatomic) NSString *detailsURL;

@end
