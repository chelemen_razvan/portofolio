//
//  ProductDetails.h
//  Project
//
//  Created by Razvan Chelemen on 2014-04-24.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ProductDetails : NSObject

@property (nonatomic, strong) NSString *imageLink;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *seller;
@property (nonatomic, strong) NSString *availability;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSDictionary *specifications;
@property (nonatomic, strong) NSString *shippingType;

@end
