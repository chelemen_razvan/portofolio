//
//  SearchService.m
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "SearchService.h"

#define SEARCH_PATH @"http://m.emag.ro/search/"

@implementation SearchService
{
	NSOperationQueue *_operationQueue;
}

-(id) init
{
	self = [super init];
	if (self)
	{
		_operationQueue = [[NSOperationQueue alloc] init];
	}
	
	return self;
}

- (void) getProductsForQuery:(NSString *) query WithCompletionBlock:(ServiceProviderCompletionBlock) completionBlock
{
	[_operationQueue addOperation:[NSBlockOperation blockOperationWithBlock:^{
		//create url
		NSString *stringUrl = [NSString stringWithFormat:@"%@%@", SEARCH_PATH, [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
		
        //load page
        HTMLLoader *loader = [[HTMLLoader alloc] init];
        [loader loadHTMLForURL:stringUrl WithCompletionBlock:^(NSData *data, NSError *error) {
            if (error != nil)
            {
				dispatch_async(dispatch_get_main_queue(),^{
                    completionBlock(nil, error);
                });
				
				return;
			}
			
			//parse it
			ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
			NSArray *productDetails = [parser parseResultsPage:data];
			
			//send product on the main thread
			dispatch_async(dispatch_get_main_queue(),^{
				completionBlock(productDetails, error);
			});
			
        }];
    }]];
}

@end
