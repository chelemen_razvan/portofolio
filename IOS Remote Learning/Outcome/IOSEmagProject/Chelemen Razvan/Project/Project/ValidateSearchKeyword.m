//
//  ValidateSearchKeyword.m
//  Project
//
//  Created by razvan on 5/6/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import "ValidateSearchKeyword.h"

@implementation ValidateSearchKeyword

+ (BOOL) validateKeyword:(NSString *) keyWord
{
	if (keyWord == nil || [keyWord length] == 0)
		return NO;
	
	NSCharacterSet *validCharacters = [NSCharacterSet alphanumericCharacterSet];
	
	NSString *trimmedString = [keyWord stringByReplacingOccurrencesOfString:@" " withString:@""];
	trimmedString = [trimmedString stringByTrimmingCharactersInSet:validCharacters];
	BOOL valid = [trimmedString length] == 0;
	
	return valid;
}

@end
