//
//  SearchService.h
//  Project
//
//  Created by razvan on 5/3/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HTMLLoader.h"
#import "ProductListHTMLParser.h"

typedef void(^ServiceProviderCompletionBlock)(NSArray *data, NSError *error);

@interface SearchService : NSObject

- (void) getProductsForQuery:(NSString *) query WithCompletionBlock:(ServiceProviderCompletionBlock) completionBlock;

@end
