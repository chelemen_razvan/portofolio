//
//  HTMLLoaderTestCase.m
//  Project
//
//  Created by razvan on 5/9/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HTMLLoader.h"

#define VALID_URL @"http://m.emag.ro"
#define INVALID_URL @"http://m.emaga.ro"

@interface HTMLLoaderTestCase : XCTestCase

@end

@implementation HTMLLoaderTestCase

- (void)testValidUrlRequest
{
    HTMLLoader *loader = [[HTMLLoader alloc] init];
	
	__block NSData *receivedData;
	__block NSError *receivedError;
	__block BOOL blockCalled = NO;
	
	[loader loadHTMLForURL:VALID_URL WithCompletionBlock:^(NSData *data, NSError *error) {
		@synchronized(self)
		{
			receivedData = data;
			receivedError = error;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertEqualWithAccuracy([receivedData length], 48619, 100, @"length should be 4616");
	XCTAssertNil(receivedError, @"there shouldn't be an error");
}

- (void)testInvalidUrlRequest
{
	HTMLLoader *loader = [[HTMLLoader alloc] init];
	
	__block NSData *receivedData;
	__block NSError *receivedError;
	__block BOOL blockCalled = NO;
	
	[loader loadHTMLForURL:INVALID_URL WithCompletionBlock:^(NSData *data, NSError *error) {
		@synchronized(self)
		{
			receivedData = data;
			receivedError = error;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertNil(receivedData, @"data should be nil");
	XCTAssertEqual([receivedError code], 18446744073709550613, @"error codes should be equal");
}

@end
