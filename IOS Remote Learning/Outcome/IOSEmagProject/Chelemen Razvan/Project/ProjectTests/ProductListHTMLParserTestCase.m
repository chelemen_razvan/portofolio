//
//  ProductListHTMLParserTestCase.m
//  Project
//
//  Created by razvan on 5/8/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProductListHTMLParser.h"
#import "Product.h"

#define VALID_HTML @"AllTagsValidHTMLProductList"
#define VALID_HTML_NO_VALUES @"EmptyTagsHTMLProductList"
#define INVALID_HTML @"InvalidHTML"
#define MISSING_TITLE_HTML @"MissingTitleHTMLProductList"
#define MISSING_ALL_TAGS_HTML @"AllTagsMissingHTML"
#define BROKEN_UNUSED_TAGS_HTML @"BrokenUnusedTagsHTMLProductList"
#define BROKEN_USED_TAGS_HTML @"BrokenUsedTagsHTMLProductList"

@interface ProductListHTMLParserTestCase : XCTestCase

@end

@implementation ProductListHTMLParserTestCase

- (void)testParseHTMLWithAllTagsValid
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:VALID_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 24, @"productArray size should be 24");
	
	Product *firstProduct = [productArray objectAtIndex:0];
	Product *middleProduct = [productArray objectAtIndex:10];
	Product *lastProduct = [productArray objectAtIndex:23];
	
	//check fields of the first product
	XCTAssertEqualObjects(firstProduct.imageLink, @"http://s1.emagst.net/products/489/488994/images/img233743_05062013155008_0_100x100c_j29j.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(firstProduct.title, @"Laptop Apple MacBook Pro 15\" cu procesor Intel® CoreTM i7 2.30GHz, Retina Display, 16GB, SSD 512GB, nVidia GT 750M 2GB, INT KB", @"title should be equal");
	XCTAssertEqualObjects(firstProduct.price, @"11.775,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(firstProduct.detailsPageLink, @"http://m.emag.ro/laptop-apple-macbook-pro-15-cu-procesor-intel-174-core-small-sup-tm-sup-small-i7-2-30ghz-retina-display-16gb-ssd-512gb-nvidia-gt-750m-2gb-int-kb-me294z-a/pd/DB5PJBBBM/", @"detailsLink should be equal");
	
	//check fields of a product from the middle
	XCTAssertEqualObjects(middleProduct.imageLink, @"http://s1.emagst.net/products/338/337830/images/img301167_18062013114352_0_100x100c_iti4.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(middleProduct.title, @"Laptop MacBook Air 11\" cu procesor Intel® CoreTM i5 1.30GHz, Haswell, 4GB, SSD 128GB, Intel® HD Graphics, INT KB");
	XCTAssertEqualObjects(middleProduct.price, @"4.699,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(middleProduct.detailsPageLink, @"http://m.emag.ro/laptop-macbook-air-11-cu-procesor-intel-174-core-small-sup-tm-sup-small-i5-1-30ghz-haswell-4gb-ssd-128gb-intel-174-hd-graphics-int-kb-md711z-a/pd/EYP30BBBM/", @"detailsLink should be equal");
	
	//check fields of the last product
	XCTAssertEqualObjects(lastProduct.imageLink, @"http://s1.emagst.net/products/486/485254/images/res_30b4cb7e1ebff5dd2076ed3927e2afeb_100x100c_7f62.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(lastProduct.title, @"Telefon mobil Apple iPhone 5S, 16GB, Silver", @"title should be equal");
	XCTAssertEqualObjects(lastProduct.price, @"2.799,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(lastProduct.detailsPageLink, @"http://m.emag.ro/telefon-mobil-apple-iphone-5s-16gb-silver-iphone-5s-16gb-silver/pd/DHGKJBBBM/", @"detailsLink should be equal");
	
}

- (void)testParseHTMLWithBrokenUnusedTags
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:BROKEN_UNUSED_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 24, @"productArray size should be 24");
	
	Product *firstProduct = [productArray objectAtIndex:0];
	Product *middleProduct = [productArray objectAtIndex:10];
	Product *lastProduct = [productArray objectAtIndex:23];
	
	XCTAssertEqualObjects(firstProduct.imageLink, @"http://s1.emagst.net/products/489/488994/images/img233743_05062013155008_0_100x100c_j29j.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(firstProduct.title, @"Laptop Apple MacBook Pro 15\" cu procesor Intel® CoreTM i7 2.30GHz, Retina Display, 16GB, SSD 512GB, nVidia GT 750M 2GB, INT KB", @"title should be equal");
	XCTAssertEqualObjects(firstProduct.price, @"11.775,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(firstProduct.detailsPageLink, @"http://m.emag.ro/laptop-apple-macbook-pro-15-cu-procesor-intel-174-core-small-sup-tm-sup-small-i7-2-30ghz-retina-display-16gb-ssd-512gb-nvidia-gt-750m-2gb-int-kb-me294z-a/pd/DB5PJBBBM/", @"detailsLink should be equal");
	
	XCTAssertEqualObjects(middleProduct.imageLink, @"http://s1.emagst.net/products/338/337830/images/img301167_18062013114352_0_100x100c_iti4.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(middleProduct.title, @"Laptop MacBook Air 11\" cu procesor Intel® CoreTM i5 1.30GHz, Haswell, 4GB, SSD 128GB, Intel® HD Graphics, INT KB");
	XCTAssertEqualObjects(middleProduct.price, @"4.699,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(middleProduct.detailsPageLink, @"http://m.emag.ro/laptop-macbook-air-11-cu-procesor-intel-174-core-small-sup-tm-sup-small-i5-1-30ghz-haswell-4gb-ssd-128gb-intel-174-hd-graphics-int-kb-md711z-a/pd/EYP30BBBM/", @"detailsLink should be equal");
	
	XCTAssertEqualObjects(lastProduct.imageLink, @"http://s1.emagst.net/products/486/485254/images/res_30b4cb7e1ebff5dd2076ed3927e2afeb_100x100c_7f62.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(lastProduct.title, @"Telefon mobil Apple iPhone 5S, 16GB, Silver", @"title should be equal");
	XCTAssertEqualObjects(lastProduct.price, @"2.799,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(lastProduct.detailsPageLink, @"http://m.emag.ro/telefon-mobil-apple-iphone-5s-16gb-silver-iphone-5s-16gb-silver/pd/DHGKJBBBM/", @"detailsLink should be equal");
}

- (void)testParseHTMLWithBrokenUsedTags
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:BROKEN_USED_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 23, @"productArray size should be 23");
	
	Product *middleProduct = [productArray objectAtIndex:9];
	Product *lastProduct = [productArray objectAtIndex:22];
	
	XCTAssertEqualObjects(middleProduct.imageLink, @"http://s1.emagst.net/products/338/337830/images/img301167_18062013114352_0_100x100c_iti4.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(middleProduct.title, @"Laptop MacBook Air 11\" cu procesor Intel® CoreTM i5 1.30GHz, Haswell, 4GB, SSD 128GB, Intel® HD Graphics, INT KB");
	XCTAssertEqualObjects(middleProduct.price, @"4.699,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(middleProduct.detailsPageLink, @"http://m.emag.ro/laptop-macbook-air-11-cu-procesor-intel-174-core-small-sup-tm-sup-small-i5-1-30ghz-haswell-4gb-ssd-128gb-intel-174-hd-graphics-int-kb-md711z-a/pd/EYP30BBBM/", @"detailsLink should be equal");
	
	XCTAssertEqualObjects(lastProduct.imageLink, @"http://s1.emagst.net/products/486/485254/images/res_30b4cb7e1ebff5dd2076ed3927e2afeb_100x100c_7f62.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(lastProduct.title, @"Telefon mobil Apple iPhone 5S, 16GB, Silver", @"title should be equal");
	XCTAssertEqualObjects(lastProduct.price, @"2.799,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(lastProduct.detailsPageLink, @"http://m.emag.ro/telefon-mobil-apple-iphone-5s-16gb-silver-iphone-5s-16gb-silver/pd/DHGKJBBBM/", @"detailsLink should be equal");
}

- (void)testParseHTMLWithMissingTitle
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:MISSING_TITLE_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertNil(productArray, @"productArray should be nil");
}

- (void)testParseHTMLWithEmptyTags
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:VALID_HTML_NO_VALUES ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 24, @"productArray size should be 24");
	
	Product *firstProduct = [productArray objectAtIndex:0];
	
	XCTAssertEqualObjects(firstProduct.imageLink, @"", "imageLink should be empty");
	XCTAssertEqualObjects(firstProduct.title, @"", @"title should be empty");
	XCTAssertEqualObjects(firstProduct.price, @"", @"price should be empty");
	XCTAssertEqualObjects(firstProduct.detailsPageLink, @"", @"detailsLink should be empty");
}

- (void) testParseHTMLWithAllTagsMissing
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:MISSING_ALL_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 0, @"productArray should be empty");
}

- (void) testParseInvalidHTML
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:INVALID_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductListHTMLParser *parser = [[ProductListHTMLParser alloc] init];
	
	NSArray *productArray = [parser parseResultsPage:data];
	
	XCTAssertEqual([productArray count], 0, @"productArray should be empty");
}

@end
