//
//  ImageSourceTestCase.m
//  Project
//
//  Created by razvan on 5/9/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ImageSource.h"

#define VALID_IMAGE_URL @"http://s1.emagst.net/products/489/488984/images/img255395_05062013152552_0_100x100c_93ic.jpg"
#define INVALID_IMAGE_URL @"http://s1.emagst.net/products/489/488984/images/img255395_05062013152552_0_100x100c.jpg"

@interface ImageSourceTestCase : XCTestCase

@end

@implementation ImageSourceTestCase

- (void)testValidImageUrl
{
    ImageSource *imageSource = [[ImageSource alloc] init];
	
	__block UIImage *receivedImage;
	__block BOOL blockCalled = NO;
	
	[imageSource getImageForURL:VALID_IMAGE_URL WithCompletionBlock:^(UIImage *image) {
		@synchronized(self)
		{
			receivedImage = image;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertEqual(receivedImage.size.width, 100, @"width should be 100");
	XCTAssertEqual(receivedImage.size.height, 100, @"height should be 100");
}

- (void)testInvalidImageUrl
{
    ImageSource *imageSource = [[ImageSource alloc] init];
	
	__block UIImage *receivedImage;
	__block BOOL blockCalled = NO;
	
	[imageSource getImageForURL:INVALID_IMAGE_URL WithCompletionBlock:^(UIImage *image) {
		@synchronized(self)
		{
			receivedImage = image;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertNil(receivedImage, @"image should be nil");
}

@end
