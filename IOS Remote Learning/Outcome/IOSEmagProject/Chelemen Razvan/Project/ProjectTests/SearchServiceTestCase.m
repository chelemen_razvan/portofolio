//
//  SearchServiceTestCase.m
//  Project
//
//  Created by razvan on 5/9/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SearchService.h"

#define QUERY_WITH_RESULTS @"apple"
#define QUERY_WITHOUT_RESULTS @"applehapple"

@interface SearchServiceTestCase : XCTestCase

@end

@implementation SearchServiceTestCase

- (void)testQueryWithResults
{
    SearchService *searchService = [[SearchService alloc] init];
	
	__block NSArray *productArray;
	__block NSError *receivedError;
	__block BOOL blockCalled = NO;
	
	[searchService getProductsForQuery:QUERY_WITH_RESULTS WithCompletionBlock:^(NSArray *data, NSError *error) {
		@synchronized(self)
		{
			productArray = data;
			receivedError = error;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertEqual([productArray count], 24, @"productArray size should be 24");
	XCTAssertNil(receivedError, @"error should be nil");
	
	Product *firstProduct = [productArray objectAtIndex:0];
	Product *lastProduct = [productArray objectAtIndex:23];
	
	//check fields of the first product
	XCTAssertEqualObjects(firstProduct.imageLink, @"http://s1.emagst.net/products/489/488989/images/img277959_05062013154626_0_100x100c_emj2.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(firstProduct.title, @"Laptop Apple MacBook Pro 15\" cu procesor Intel® CoreTM i7 2.0GHz, Retina Display, 8GB, SSD 256GB, Intel® Iris Graphics, INT KB", @"title should be equal");
	XCTAssertEqualObjects(firstProduct.price, @"8.999,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(firstProduct.detailsPageLink, @"http://m.emag.ro/laptop-apple-macbook-pro-15-cu-procesor-intel-174-core-small-sup-tm-sup-small-i7-2-0ghz-retina-display-8gb-ssd-256gb-intel-174-iris-graphics-int-kb-me293z-a/pd/D5RPJBBBM/", @"detailsLink should be equal");

	//check fields of the last product
	XCTAssertEqualObjects(lastProduct.imageLink, @"http://s1.emagst.net/products/486/485255/images/res_de0217c88e05c9fe0ad444f128b51684_100x100c_4c2d.jpg", "imageLink should be equal");
	XCTAssertEqualObjects(lastProduct.title, @"Telefon mobil Apple iPhone 5S, 16GB, Gold", @"title should be equal");
	XCTAssertEqualObjects(lastProduct.price, @"2.749,99 Lei", @"price should be equal");
	XCTAssertEqualObjects(lastProduct.detailsPageLink, @"http://m.emag.ro/telefon-mobil-apple-iphone-5s-16gb-gold-iphone-5s-16gb-gold/pd/DPGKJBBBM/", @"detailsLink should be equal");
}

- (void)testQueryWithoutResults
{
    SearchService *searchService = [[SearchService alloc] init];
	
	__block NSArray *productArray;
	__block NSError *receivedError;
	__block BOOL blockCalled = NO;
	
	[searchService getProductsForQuery:QUERY_WITHOUT_RESULTS WithCompletionBlock:^(NSArray *data, NSError *error) {
		@synchronized(self)
		{
			productArray = data;
			receivedError = error;
			blockCalled = YES;
		}
	}];
	
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	BOOL auxBlockCalled = NO;
	
	while (!auxBlockCalled)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		
		@synchronized(self)
		{
			auxBlockCalled = blockCalled;
		}
	}
	
	XCTAssertEqual([productArray count], 0, @"productArray size should be empty");
	XCTAssertNil(receivedError, @"error should be nil");
	
}

@end
