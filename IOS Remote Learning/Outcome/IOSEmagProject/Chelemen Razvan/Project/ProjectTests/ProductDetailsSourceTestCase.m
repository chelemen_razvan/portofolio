//
//  ProductDetailsSourceTestCase.m
//  Project
//
//  Created by razvan on 5/9/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProductDetailsSource.h"

#define VALID_URL @"http://m.emag.ro/telefon-mobil-samsung-i8190-galaxy-s3-mini-ceramic-white-sami8190wht/pd/EBTZKBBBM/"
#define INVALID_URL @"http://m.emaga.ro/telefon-mobil-samsung-i8190--s3-mini-ceramic-pink-sami80wht/pd/"

@interface ProductDetailsSourceTestCase : XCTestCase

@end

@interface TestHandler : NSObject <ProductDetailsHandler>

- (ProductDetails *) getProductDetails;
- (NSError *) getError;

@end

@implementation ProductDetailsSourceTestCase

- (void)testValidUrlRequest
{
    ProductDetailsSource *detailsSource = [[ProductDetailsSource alloc] init];
	TestHandler *testHandler = [[TestHandler alloc] init];

	[detailsSource getProductDetailsFor:VALID_URL WithCallback:testHandler];
	ProductDetails *result = [testHandler getProductDetails];
	
	XCTAssertEqualObjects(result.imageLink, @"http://s1.emagst.net/products/38/37608/images/img259687_29052013161657_0_720x720c_u5iq.jpg", @"imageLinks should be equal");
	XCTAssertEqualObjects(result.title, @"Telefon mobil Samsung I8190 Galaxy S3 Mini, Ceramic White ", @"titles should be equal");
	XCTAssertEqualObjects(result.price, @"669,99 Lei", @"prices should be equal");
	XCTAssertEqualObjects(result.seller, @"eMAG", @"sellers should be equal");
	XCTAssertEqualObjects(result.availability, @"În stoc", @"availability should be equal");
	XCTAssertEqualObjects(result.shippingType, @"Transport gratuit", @"shipping type should be equal");
	
	//string too long test length instead
	XCTAssertEqual([result.description length], 875, @"description should have this length");
	
	//test product specifications
	NSDictionary *specifications = result.specifications;
	
	XCTAssertNotNil([result.specifications  objectForKey:@"General"], @"specifications should have this key");
	XCTAssertEqual([[specifications objectForKey:@"General"] count], 13, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Memorie"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Memorie"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Afisaj"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Afisaj"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Conectivitate"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Conectivitate"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Foto video"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Foto video"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Alimentare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Alimentare"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Comunicare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Comunicare"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Multimedia"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Multimedia"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Altele"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Altele"] count], 2, @"specifications number wrong");
	
}

- (void)testInvalidUrlRequest
{
	ProductDetailsSource *detailsSource = [[ProductDetailsSource alloc] init];
	TestHandler *testHandler = [[TestHandler alloc] init];
	
	[detailsSource getProductDetailsFor:INVALID_URL WithCallback:testHandler];
	ProductDetails *result = [testHandler getProductDetails];
	NSError *error = [testHandler getError];
	
	XCTAssertNil(result, @"result should be nil");
	XCTAssertEqual([error code], 18446744073709550613, @"error codes should be equal");
}

@end

@implementation TestHandler
{
	ProductDetails *_productDetails;
	NSError *_error;
	BOOL _callReceived;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_callReceived = NO;
	}
	return self;
}

-(void) productDetailsReceived:(ProductDetails *) productDetails error:(NSError *) error
{
	@synchronized(self)
	{
		
		_productDetails = productDetails;
		_error = error;
		_callReceived = YES;
	}
}

- (ProductDetails *) getProductDetails
{
	BOOL gotCall;
	@synchronized(self)
	{
		gotCall = _callReceived;
	}
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	
	while (!gotCall)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		@synchronized(self)
		{
			gotCall = _callReceived;
		}
	}
	
	return _productDetails;
}

- (NSError *) getError
{
	BOOL gotCall;
	@synchronized(self)
	{
		gotCall = _callReceived;
	}
	NSDate *loopUntil = [NSDate dateWithTimeIntervalSinceNow:1];
	
	while (!gotCall)
	{
		[[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:loopUntil];
		@synchronized(self)
		{
			gotCall = _callReceived;
		}
	}
	
	return _error;
}

@end
