//
//  ProductDetailsParserTestCase.m
//  Project
//
//  Created by razvan on 5/7/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ProductDetailsParser.h"
#import "ProductDetails.h"

#define VALID_HTML @"AllTagsValidHTMLProductDetails"
#define VALID_HTML_NO_VALUES @"AllUsedTagsEmptyProductDetails"
#define BROKEN_UNUSED_TAGS_HTML @"BrokenUnusedTagsHTMLProductDetails"
#define BROKEN_USED_TAGS_HTML @"AllUsedTagsBrokenHTMLProductDetails"
#define MISSING_TAGS_HTML @"AllTagsMissingHTML"
#define INVALID_HTML @"Invalid HTML"

@interface ProductDetailsParserTestCase : XCTestCase

@end

@implementation ProductDetailsParserTestCase

- (void)testParseHTMLWithAllTagsValid
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:VALID_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	XCTAssertEqualObjects(result.imageLink, @"http://s1.emagst.net/products/38/37608/images/img259687_29052013161657_0_720x720c_u5iq.jpg", @"imageLinks should be equal");
	XCTAssertEqualObjects(result.title, @"Telefon mobil Samsung I8190 Galaxy S3 Mini, Ceramic White ", @"titles should be equal");
	XCTAssertEqualObjects(result.price, @"689,99 Lei", @"prices should be equal");
	XCTAssertEqualObjects(result.seller, @"eMAG", @"sellers should be equal");
	XCTAssertEqualObjects(result.availability, @"În stoc", @"availability should be equal");
	XCTAssertEqualObjects(result.shippingType, @"Transport gratuit", @"shipping type should be equal");
	
	//string too long test length instead
	XCTAssertEqual([result.description length], 875, @"description should have this length");
	
	//test product specifications
	NSDictionary *specifications = result.specifications;
	
	XCTAssertNotNil([result.specifications  objectForKey:@"General"], @"specifications should have this key");
	XCTAssertEqual([[specifications objectForKey:@"General"] count], 13, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Memorie"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Memorie"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Afisaj"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Afisaj"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Conectivitate"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Conectivitate"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Foto video"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Foto video"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Alimentare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Alimentare"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Comunicare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Comunicare"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Multimedia"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Multimedia"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Altele"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Altele"] count], 2, @"specifications number wrong");
}

//this should work exactly as a valid HTML
- (void)testParseHTMLWithBrokenUnusedTags
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:BROKEN_UNUSED_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	XCTAssertEqualObjects(result.imageLink, @"http://s1.emagst.net/products/38/37608/images/img259687_29052013161657_0_720x720c_u5iq.jpg", @"imageLinks should be equal");
	XCTAssertEqualObjects(result.title, @"Telefon mobil Samsung I8190 Galaxy S3 Mini, Ceramic White ", @"titles should be equal");
	XCTAssertEqualObjects(result.price, @"689,99 Lei", @"prices should be equal");
	XCTAssertEqualObjects(result.seller, @"eMAG", @"sellers should be equal");
	XCTAssertEqualObjects(result.availability, @"În stoc", @"availability should be equal");
	XCTAssertEqualObjects(result.shippingType, @"Transport gratuit", @"shipping type should be equal");
	
	//string too long test length instead
	XCTAssertEqual([result.description length], 875, @"description should have this length");
	
	//test product specifications
	NSDictionary *specifications = result.specifications;
	
	XCTAssertNotNil([result.specifications  objectForKey:@"General"], @"specifications should have this key");
	XCTAssertEqual([[specifications objectForKey:@"General"] count], 13, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Memorie"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Memorie"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Afisaj"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Afisaj"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Conectivitate"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Conectivitate"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Foto video"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Foto video"] count], 6, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Alimentare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Alimentare"] count], 4, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Comunicare"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Comunicare"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Multimedia"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Multimedia"] count], 3, @"specifications number wrong");
	
	XCTAssertNotNil([specifications objectForKey:@"Altele"], @"specs should have this key");
	XCTAssertEqual([[specifications objectForKey:@"Altele"] count], 2, @"specifications number wrong");
}

- (void)testParseHTMLWithBrokenUsedTags
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:BROKEN_USED_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	//tags missing dimond brackets fail
	XCTAssertNil(result.imageLink, @"imageLink should be nil");
	XCTAssertNil(result.title, @"title should be nil");
	XCTAssertNil(result.price, @"price should be nil");
	
	//tags missing closing pair should work
	XCTAssertEqualObjects(result.seller, @"eMAG", @"sellers should be equal");
	XCTAssertEqualObjects(result.availability, @"În stoc", @"availability should be equal");
	XCTAssertEqualObjects(result.shippingType, @"Transport gratuit", @"shipping type should be equal");
	
	//string too long test length instead
	XCTAssertEqual([result.description length], 875, @"description should have this length");
	
	//test product specifications
	NSDictionary *specifications = result.specifications;
	
	XCTAssertNotNil([result.specifications  objectForKey:@"General"], @"specifications should have this key");
	XCTAssertEqual([[specifications objectForKey:@"General"] count], 13, @"specifications number wrong");
	
	XCTAssertNil([specifications objectForKey:@"Memorie"], @"specs should not have this key");

}

- (void)testParseHTMLWithAllUsedTagsEmpty
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:VALID_HTML_NO_VALUES ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	XCTAssertEqualObjects(result.imageLink, @"", @"imageLink should be empty");
	XCTAssertEqualObjects(result.title, @"", @"title should be empty");
	XCTAssertEqualObjects(result.price, @"", @"price should be empty");
	XCTAssertNil(result.seller, @"seller should be nil");
	XCTAssertNil(result.availability, @"availability should be nil");
	XCTAssertNil(result.shippingType, @"shipping type should be empty");
	
	XCTAssertEqualObjects(result.description, @"", @"description should be empty");
	XCTAssertEqual([result.specifications count], 0, @"specifications should be empty");
	
}

- (void)testParseHTMLWithAllTagsMissing
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:MISSING_TAGS_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	XCTAssertNil(result.imageLink, @"imageLink should be nil");
	XCTAssertNil(result.title, @"title should be nil");
	XCTAssertNil(result.price, @"price should be nil");
	XCTAssertNil(result.seller, @"seller should be nil");
	XCTAssertNil(result.availability, @"availability should be nil");
	XCTAssertNil(result.shippingType, @"shipping type should be nil");
	
	XCTAssertNil(result.description, @"description should be nil");
	XCTAssertEqual([result.specifications count], 0, @"specifications should be empty");
}

- (void) testParseInvalidHTML
{
	NSBundle *bundle = [NSBundle bundleForClass:[self class]];
	NSString *path = [bundle pathForResource:INVALID_HTML ofType:@"html"];
	
	NSData *data = [NSData dataWithContentsOfFile:path];
	
	ProductDetailsParser *parser = [[ProductDetailsParser alloc] init];
	ProductDetails *result = [parser parseProductPage:data];
	
	XCTAssertNil(result.imageLink, @"imageLink should be nil");
	XCTAssertNil(result.title, @"title should be nil");
	XCTAssertNil(result.price, @"price should be nil");
	XCTAssertNil(result.seller, @"seller should be nil");
	XCTAssertNil(result.availability, @"availability should be nil");
	XCTAssertNil(result.shippingType, @"shipping type should be nil");
	
	XCTAssertNil(result.description, @"description should be nil");
	XCTAssertEqual([result.specifications count], 0, @"specifications should be empty");
}

@end
