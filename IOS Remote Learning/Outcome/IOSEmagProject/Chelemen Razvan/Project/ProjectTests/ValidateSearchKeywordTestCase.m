//
//  ValidateSearchKeywordTestCase.m
//  Project
//
//  Created by razvan on 5/9/14.
//  Copyright (c) 2014 Razvan Chelemen. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ValidateSearchKeyword.h"


@interface ValidateSearchKeywordTestCase : XCTestCase

@end

@implementation ValidateSearchKeywordTestCase

- (void)testWithValidKeyword
{
    XCTAssertTrue([ValidateSearchKeyword validateKeyword:@"apple and samsung"], @"this should be valid");
}

- (void)testWithExtraSpaces
{
	XCTAssertTrue([ValidateSearchKeyword validateKeyword:@"   lost   in space  "], @"this should be valid");
}

- (void)testWithInvalidCharacters
{
	XCTAssertFalse([ValidateSearchKeyword validateKeyword:@"no ? mark or other @#$"], @"this should be invalid");
}

- (void)testWithEmptyString
{
	XCTAssertFalse([ValidateSearchKeyword validateKeyword:@""], @"this should be invalid");
}

@end
