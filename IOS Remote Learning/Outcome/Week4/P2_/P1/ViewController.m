//
//  ViewController.m
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ViewController.h"
#import "PersonViewController.h"
#import "stdlib.h"


@implementation ViewController {
	NSMutableArray *_tableItems;
	NSMutableArray *_dictionaryArray;
	NSMutableArray *pathForImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_tableItems = [[NSMutableArray alloc] init ];
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Problem1Data2" ofType:@"plist"];
	_dictionaryArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
	pathForImage = [[NSMutableArray alloc] init];
	for (NSDictionary *dic in _dictionaryArray)
	{
		[_tableItems addObject:[[NSString alloc] initWithFormat:@"%@ %@" ,[dic objectForKey:@"Name"], [dic objectForKey:@" Surname"]]];
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	int no = 0;
	while (no == 0)
	{
		no = arc4random() % 6;
	}
	
	NSString *imageName = [[NSString alloc] initWithFormat:@"img%d",no];
	NSString *imgPath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
	[[_dictionaryArray objectAtIndex:indexPath.row] setObject:imgPath forKey:@"image"];
	
	UIImage *theImage = [UIImage imageWithContentsOfFile:imgPath];
    cell.textLabel.text = [_tableItems objectAtIndex:indexPath.row];
	cell.imageView.image = theImage;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"transition"])
	{
		PersonViewController *pvc = [segue destinationViewController];
		NSIndexPath *path = [self.tableView indexPathForSelectedRow];
		
		long row = [path row];
		pvc.personDetails = [_dictionaryArray objectAtIndex:row];

	}
}

@end
