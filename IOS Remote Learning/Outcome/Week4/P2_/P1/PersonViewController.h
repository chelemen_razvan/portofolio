//
//  PersonViewController.h
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PersonViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (strong, nonatomic) NSString *imagePath;
@property (weak, nonatomic) IBOutlet UILabel *personDateOfDeath;
@property (weak, nonatomic) IBOutlet UILabel *personDateOfBirth;
@property (weak, nonatomic) IBOutlet UILabel *personSurname;
@property (strong, nonatomic) NSDictionary *personDetails;
@property (weak, nonatomic) IBOutlet UILabel *personName;
@end
