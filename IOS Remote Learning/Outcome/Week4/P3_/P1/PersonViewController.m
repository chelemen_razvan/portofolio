//
//  PersonViewController.m
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "PersonViewController.h"


@implementation PersonViewController {
	NSMutableString *_location;
}

@synthesize personDateOfBirth = _personDateOfBirth;
@synthesize personDateOfDeath = _personDateOfDeath;
@synthesize personName = _personName;
@synthesize personSurname = _personSurname;
@synthesize personDetails = _personDetails;
@synthesize image = _image;
@synthesize imagePath = _imagePath;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_location = [[NSMutableString alloc] initWithFormat:@"http://maps.apple.com/?q=%@",[_personDetails	objectForKey:@"location"]];
	_personName.text = [_personDetails objectForKey:@"Name"];
	_personSurname.text = [_personDetails objectForKey:@" Surname"];
	_personDateOfBirth.text = [_personDetails objectForKey:@" Date of birth"];
	_personDateOfDeath.text = [_personDetails objectForKey:@" Date of death"];
	_image.image = [UIImage imageWithContentsOfFile:[_personDetails objectForKey:@"image"]];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)showMapsButton:(id)sender {

	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:_location]];
}
@end
