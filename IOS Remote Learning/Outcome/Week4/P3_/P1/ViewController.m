//
//  ViewController.m
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ViewController.h"
#import "PersonViewController.h"
#import "stdlib.h"


@implementation ViewController {
	NSMutableArray *objects;
	NSMutableArray *array;
	NSArray *locations;
	NSMutableArray *pathForImage;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	locations = @[@"New York",@"Paris",@"London",@"Berlin",@"Bucharest",@"Bahamas"];
	objects = [[NSMutableArray alloc] init ];
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Problem1Data2" ofType:@"plist"];
	array = [[NSMutableArray alloc] initWithContentsOfFile:path];
	pathForImage = [[NSMutableArray alloc] init];
	
	for (NSDictionary *dic in array)
	{
		[objects addObject:[[NSString alloc] initWithFormat:@"%@ %@" ,[dic objectForKey:@"Name"], [dic objectForKey:@" Surname"]]];
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [objects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
	
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
	int no = 0;
	
	while (no == 0)
	{
		no = arc4random() % 6;
	}
	
	NSString *imageName = [[NSString alloc] initWithFormat:@"img%d",no];
	NSString *imgPath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"jpg"];
	[[array objectAtIndex:indexPath.row] setObject:imgPath forKey:@"image"];
	[[array objectAtIndex:indexPath.row] setObject:[locations objectAtIndex:no] forKey:@"location"];
	
	UIImage *theImage = [UIImage imageWithContentsOfFile:imgPath];
    cell.textLabel.text = [objects objectAtIndex:indexPath.row];
	cell.imageView.image = theImage;
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"transition"])
	{
		PersonViewController *pvc = [segue destinationViewController];
		NSIndexPath *path = [self.tableView indexPathForSelectedRow];
		
		long row = [path row];
		pvc.personDetails = [array objectAtIndex:row];

	}
}

@end
