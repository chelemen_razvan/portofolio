//
//  ViewController.h
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
