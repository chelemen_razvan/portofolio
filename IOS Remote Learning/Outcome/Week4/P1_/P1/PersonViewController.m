//
//  PersonViewController.m
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "PersonViewController.h"


@implementation PersonViewController
@synthesize personDateOfBirth = _personDateOfBirth;
@synthesize personDateOfDeath = _personDateOfDeath;
@synthesize personName = _personName;
@synthesize personSurname = _personSurname;
@synthesize personDetails = _personDetails;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_personName.text = [_personDetails objectForKey:@"Name"];
	_personSurname.text = [_personDetails objectForKey:@" Surname"];
	_personDateOfBirth.text = [_personDetails objectForKey:@" Date of birth"];
	_personDateOfDeath.text = [_personDetails objectForKey:@" Date of death"];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
