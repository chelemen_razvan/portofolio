//
//  ViewController.m
//  P1
//
//  Created by Dobrean Dragos on 22/03/14.
//  Copyright (c) 2014 Dobrean Dragos. All rights reserved.
//

#import "ViewController.h"
#import "PersonViewController.h"


@implementation ViewController {
	NSMutableArray *_tableItems;
	NSMutableArray *_dictionaryArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	_tableItems = [[NSMutableArray alloc] init ];
	NSString *path = [[NSBundle mainBundle] pathForResource:@"Problem1Data2" ofType:@"plist"];
	_dictionaryArray = [[NSMutableArray alloc] initWithContentsOfFile:path];
	for (NSDictionary *dic in _dictionaryArray)
	{
		[_tableItems addObject:[[NSString alloc] initWithFormat:@"%@ %@" ,[dic objectForKey:@"Name"], [dic objectForKey:@" Surname"]]];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_tableItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
	
    cell.textLabel.text = [_tableItems objectAtIndex:indexPath.row];
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([[segue identifier] isEqualToString:@"transition"])
	{
		PersonViewController *pvc = [segue destinationViewController];
		NSIndexPath *path = [self.tableView indexPathForSelectedRow];
		
		pvc.personDetails = [_dictionaryArray objectAtIndex:path.row];
	}
}

@end
