//
//  BAValidatorTestCase.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BAFieldsValidator.h"

@interface BAFieldsValidatorTestCase : XCTestCase

@end

@implementation BAFieldsValidatorTestCase
{
	BAFieldsValidator *_validator;
}

- (void) setUp
{
	[super setUp];
	_validator = [[BAFieldsValidator alloc] init];
	
}

- (void)testValidEmail
{
	
    XCTAssertEqual(BAValidEmail, [_validator validateEmail:@"ion@gmail.com"], @"Email should be valid");
}

- (void)testEmailWithoutName
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"@gmail.com"], @"Email format should be invalid");
}

- (void)testEmailWithoutDomain
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"ion@.com"], @"Email format should be invalid");
}

- (void)testEmailWitoutTopDomain
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"ion@gmail."], @"Email format should be invalid");
}

- (void)testEmailWithIpDomain
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"ion@[192.168.0.1].com"], @"Email format should be invalid");
}

- (void)testEmailWithSubdomain
{
    XCTAssertEqual(BAValidEmail, [_validator validateEmail:@"ion@yo.gmail.com"], @"Email should be valid");
}

- (void)testEmailWithRandomText
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"dsfdsgdfgdf.v./d"], @"Email format should be invalid");
}

- (void)testEmailWithMultipleAtSigns
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@"io@n@yo.gmail.com"], @"Email format should be invalid");
}

- (void)testEmailWithEmptyString
{
    XCTAssertEqual(BAInvalidEmailFormat, [_validator validateEmail:@""], @"Email format should be invalid");
}

- (void)testEmailTooLong
{
    XCTAssertEqual(BAEmailTooLong, [_validator validateEmail:@"vrenvnenivieivreicerinveircerivneriviunerivneicmeurnvnviernvierinu@vfidjnvirunvineru.eiruvueirnvei.co"], @"Email should be too long");
}

- (void)testPasswordValid
{
    XCTAssertEqual(BAValidPassword, [_validator validatePassword:@"#SWAG#yolo"], @"Password should be valid");
}

- (void)testPasswordTooShort
{
    XCTAssertEqual(BAPasswordTooShort, [_validator validatePassword:@"123456"], @"Password should be too short");
}

- (void)testPasswordTooLong
{
    XCTAssertEqual(BAPasswordTooLong, [_validator validatePassword:@"12345612345612345612345"], @"Password should be too long");
}

- (void)testPasswordWithoutUppercase
{
    XCTAssertEqual(BAPasswordWithoutUppercase, [_validator validatePassword:@"#swag#selfie"], @"Password should be invalid");
}

- (void)testPasswordWithoutSpecialChars
{
    XCTAssertEqual(BAPasswordWithoutSpecialCharacter, [_validator validatePassword:@"nohashtagSWAG"], @"Password should be invalid");
}

- (void)testEmptyPassword
{
    XCTAssertEqual(BAPasswordTooShort, [_validator validatePassword:@""], @"Password should be invalid");
}

@end
