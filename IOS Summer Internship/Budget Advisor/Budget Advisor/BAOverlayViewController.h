//
//  BAOverlayViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

//inherit from this class if you need an overlay for your controller
@interface BAOverlayViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *overlayView;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) UILabel *errorMessageLabel;
@property (strong, nonatomic) UIButton *retryButton;

@property (copy, nonatomic) void(^overlayWillAppear)();
@property (copy, nonatomic) void(^overlayWillDisappear)();

//call this if you need an empty overlay view
- (void) hideOverlayView;
- (void) showOverlayView;

//call this if you need to display a loading screen
- (void) hideLoadingScreen;
- (void) showLoadingScreen;

//shows the error overlay
- (void)showErrorScreenWithMessage:(NSString *) message andRetryBlock:(void(^)()) block;
//hides the error overlay
- (void)hideErrorScreen;

@end
