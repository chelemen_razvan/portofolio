//
//  BATransactionsViewController.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 28/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BATransactionService.h"
#import "BATransaction.h"
#import "BAOverlayViewController.h"
#import "BATransactionsTableViewCell.h"

@interface BATransactionsViewController : BAOverlayViewController <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *transactionsTable;
@property (weak, nonatomic) IBOutlet UIButton *addTransactionButton;

- (IBAction)addTransactionButton:(id)sender;

@end
