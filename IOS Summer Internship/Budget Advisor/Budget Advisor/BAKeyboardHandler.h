//
//  BAKeyboardHandler.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/23/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BAKeyboardHandler : NSObject

@property (nonatomic, weak) UIView *rootView;
@property (nonatomic, weak) UIView *coveredView;

//moves rootView up such that coveredView becomes visible when keyboard appears
//method should be called in viewWillAppear
-(void) registerKeyboardHandlerFor:(UIView *) rootView viewCoveredByKeyboard:(UIView *) coveredView withTolerance:(CGFloat) tolerance;

//unregister the keyboard handler
//method should be called in viewWillDisappear
-(void) unregisterKeyboardHandler;

//set to yes if you want to move up the view or to no otherwise
-(void) shouldMoveView:(BOOL) move;
@end
