//
//  BAUser.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/16/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "JSONModel.h"

@interface BACredential : JSONModel

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;

@end
