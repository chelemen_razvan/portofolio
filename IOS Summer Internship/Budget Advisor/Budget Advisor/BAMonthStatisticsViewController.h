//
//  BAMonthStatisticsViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAStatisticsService.h"
#import "BAStatisticsColors.h"
#import "BAOverlayViewController.h"
#import "BAStatisticsTableViewCell.h"
#import "BADateMapper.h"


@interface BAMonthStatisticsViewController : BAOverlayViewController

@property (weak, nonatomic) IBOutlet UITableView *expensesTableView;
@property (weak, nonatomic) IBOutlet UILabel *leftMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightMonthLabel;

//array of uicolors for each arc of the piechart
@property(nonatomic, strong) NSArray *sliceColors;

- (IBAction)swipeBottomViewLeft:(id)sender;
- (IBAction)swipeBottomViewRight:(id)sender;

@end
