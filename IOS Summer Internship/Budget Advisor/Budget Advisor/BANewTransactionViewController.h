//
//  BANewTransactionViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/25/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BACategoriesService.h"
#import "BATransactionService.h"
#import "BAOverlayViewController.h"
#import "BAKeyboardHandler.h"
#import "BABottomBorderView.h"
#import "BACategory.h"

@interface BANewTransactionViewController : BAOverlayViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *incomeExpenseToggleButton;
@property (weak, nonatomic) IBOutlet UITextField *amountTextField;
@property (weak, nonatomic) IBOutlet UITextField *categoryTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UITextField *repetitionTextField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet BABottomBorderView *descriptionViewContainer;
@property (weak, nonatomic) IBOutlet BABottomBorderView *dateViewContainer;
@property (strong, nonatomic) BATransaction *selectedTransaction;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;

- (IBAction)categoryTextFieldEditingDidBegin:(id)sender;
- (IBAction)repetitionTextFieldEditingDidBegin:(id)sender;
- (IBAction)amountTextFieldEditingDidBegin:(id)sender;
- (IBAction)dateTextFieldEditingDidBegin:(id)sender;
- (IBAction)incomeExpenseToggleButtonValueChanged:(id)sender;


- (IBAction)backPressed:(id)sender;
- (IBAction)savePressed:(id)sender;

@end
