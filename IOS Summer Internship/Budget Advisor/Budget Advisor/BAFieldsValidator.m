//
//  BAFieldsValidator.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAFieldsValidator.h"

@implementation BAFieldsValidator

- (BAEmailValidationResult) validateEmail:(NSString *) email
{
	if (email.length > 100)
		return BAEmailTooLong;
	
	NSString *validEmailRegex = @"^[a-z0-9_.+-]+@[a-z0-9-]+\\.[a-z0-9-.]+$";
	NSPredicate *matchingPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", validEmailRegex];
	
	if ([matchingPredicate evaluateWithObject:email])
		return BAValidEmail;
	else
		return BAInvalidEmailFormat;
}

- (BAPasswordValidationResult) validatePassword:(NSString *) password
{
	if (password.length < 7)
		return BAPasswordTooShort;
	
	if (password.length > 20)
		return BAPasswordTooLong;
	
	NSRegularExpression *specialCharsRegex = [NSRegularExpression regularExpressionWithPattern:@"[‘!@#$%^&*?’]" options:0 error:nil];
	if ([specialCharsRegex numberOfMatchesInString:password options:0 range:NSMakeRange(0, password.length)] == 0)
		return BAPasswordWithoutSpecialCharacter;
	
	NSRegularExpression *uppercaseCharsRegex = [NSRegularExpression regularExpressionWithPattern:@"[A-Z]" options:0 error:nil];
	if ([uppercaseCharsRegex numberOfMatchesInString:password options:0 range:NSMakeRange(0, password.length)] == 0)
		return BAPasswordWithoutUppercase;
	
	return BAValidPassword;
}

- (BOOL) validateName:(NSString *)name
{
    if ((name.length > 50) || (name.length == 0))
    {
        return false;
    }
    else
    {
        return true;
    }
}

@end
