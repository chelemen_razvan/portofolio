//
//  BAIncome.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 17/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "JSONModel.h"
#import "BACategory.h"

typedef enum {
    BARepetitionEveryDay = 1,
    BARepetitionEveryWeek = 2,
    BARepetitionEveryMonth = 3,
    BAREpetitionEveryYear = 4
} BATransactionRepetition;

@interface BATransaction : JSONModel

@property (nonatomic, strong) BACategory *category;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, assign) NSInteger amount;
@property (nonatomic, assign) NSInteger transactionId;

//date needs to be of the following form: yyyy/mm/dd
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, assign) BATransactionRepetition repetition;
@property (nonatomic, assign) NSInteger userId;
//type can be "income" or "expense"
@property (nonatomic, assign) NSString *type;

@end
