//
//  BAStatisticsViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/29/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAStatisticsViewController.h"

#define kDefaultCurrency @"RON"
#define kAnimationScrollLength 30
#define kSegueIdentifierContainerController @"embedContainer"

typedef enum {
	BAMonth = 1,
	BAYear = 2
} BAUpdateType;

@implementation BAStatisticsViewController
{
	BAStatisticsService *_statisticsService;
	NSArray *_statisticsArray;
	NSInteger _currentlyShownMonth;
	NSInteger _currentlyShownYear;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	_statisticsService = [[BAStatisticsService alloc] init];
	
	_pieChartView.dataSource = self;
	BAStatisticsColors *colors = [[BAStatisticsColors alloc] init];
	_sliceColors = [colors getStatisticsColors];
	
	//get current date
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
	_currentlyShownMonth = dateComponents.month;
	_currentlyShownYear = dateComponents.year;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	//load statistics for current month by default
    [self loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
}

- (void)loadStatisticsForMonth:(NSInteger) month year:(NSInteger) year
{
	_currentlyShownMonth = month;
	_currentlyShownYear = year;
	
	[_statisticsService getStatisticOn:month from:year WithResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive data" andRetryBlock:^{
				[self loadStatisticsForMonth:month year:year];
			}];
			return;
		}
		
		//populate and reload views that use data
		_statisticsArray = array;
		[self computeAndDisplayTotalExpensesAndIncomes];
		[self updateTitleLabelFor:BAMonth];
		[_pieChartView reloadData];
	}];
}

- (void)loadStatisticsForYear:(NSInteger) year
{
	_currentlyShownYear = year;
	
	[_statisticsService getStatisticOn:year withResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive data" andRetryBlock:^{
				[self loadStatisticsForYear:year];
			}];
			return;
		}
		
		//populate and reload views that use data
		_statisticsArray = array;
		[self computeAndDisplayTotalExpensesAndIncomes];
		[self updateTitleLabelFor:BAYear];
		[_pieChartView reloadData];
	}];
}

- (void) computeAndDisplayTotalExpensesAndIncomes
{
	NSInteger expenses = 0;
	NSInteger incomes = 0;
	
	for (BAStatisticElement *element in _statisticsArray)
	{
		expenses += element.expenses;
		incomes += element.incomes;
	}
	
	_expensesTextField.text = [NSString stringWithFormat:@"%i %@", expenses, kDefaultCurrency];
	_incomesTextField.text = [NSString stringWithFormat:@"%i %@", incomes, kDefaultCurrency];
}

- (void) updateTitleLabelFor:(BAUpdateType) type
{
	if (type == BAMonth)
	{
		//set current month name
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:(_currentlyShownMonth - 1)];
		_titleLabel.text = [NSString stringWithFormat:@"Statistics for %@", monthName];
	}
	else
	{
		_titleLabel.text = [NSString stringWithFormat:@"Statistics for %i", _currentlyShownYear];
	}
}

- (IBAction)yearMonthChanged:(id)sender
{
	BOOL canChange = [self.containerViewController swapViewControllers];
	if (canChange)
	{
		if (_yearMonthSegmentedControl.selectedSegmentIndex == 0)
		{
			_yearMonthSegmentedControl.selectedSegmentIndex = 1;
		}
		else
		{
			_yearMonthSegmentedControl.selectedSegmentIndex = 0;
		}
	}
	
	if (_yearMonthSegmentedControl.selectedSegmentIndex == 0)
	{
		[self updateTitleLabelFor:BAMonth];
	}
	else
	{
		[self updateTitleLabelFor:BAYear];
	}
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kSegueIdentifierContainerController])
	{
        self.containerViewController = segue.destinationViewController;
		BAStatisticsContainerViewController *destinationViewController = [segue destinationViewController];
		destinationViewController.parentController = self;
    }
}

#pragma mark - Piechart datasource methods

- (NSUInteger)numberOfSlicesInPieChart:(XYPieChart *)pieChart
{
	return _statisticsArray.count;
}

- (CGFloat)pieChart:(XYPieChart *)pieChart valueForSliceAtIndex:(NSUInteger)index
{
	return ((BAStatisticElement *) _statisticsArray[index]).expenses;
}

- (UIColor *)pieChart:(XYPieChart *)pieChart colorForSliceAtIndex:(NSUInteger)index
{
    return _sliceColors[(index % self.sliceColors.count)];
}

@end
