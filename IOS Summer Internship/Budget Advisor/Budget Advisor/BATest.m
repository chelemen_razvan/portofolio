//
//  BATest.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BATest.h"

@implementation BATest

- (void) test
{
	//testing stuff
	/*
	 //GET call
	 NSDictionary *params = @{@"id": @"284910350"};
	 [BANetworking getResource:@"token/token" withParameters:nil success:^(NSDictionary *data) {
	 NSLog(@"GET CALL Success: %@", data);
	 } failure:^(NSDictionary *data, NSError *error) {
	 NSLog(@"GET Error: %@", error);
	 }];
	 */
	/*
	 //POST call
	 NSString *string = @"{username:\"johnny\", password:\"aslihasl\"}";
	 NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];
	 
	 [BANetworking postResource:@"authenticate" withBodyData:data success:^(NSDictionary *data) {
	 NSLog(@"POST CALL Success: %@", data);
	 } failure:^(NSDictionary *data, NSError *error) {
	 NSLog(@"POST Error: %@", error);
	 }];
	 */
	/*
	 //DELETE call
	 NSDictionary *params3 = @{@"id": @"284910350"};
	 
	 [BANetworking deleteResource:@"lookup" withParameters:params3 success:^(NSDictionary *data) {
	 NSLog(@"DELETE CALL Success: %@", data);
	 } failure:^(NSDictionary *data, NSError *error) {
	 NSLog(@"DELETE Error: %@", error);
	 }];
	 
	 //PUT call
	 NSData *data2 = [string dataUsingEncoding:NSUTF8StringEncoding];
	 
	 [BANetworking putResource:@"lookup" withBodyData:data2 success:^(NSDictionary *data) {
	 NSLog(@"PUT CALL Success: %@", data);
	 } failure:^(NSDictionary *data, NSError *error) {
	 NSLog(@"PUT Error: %@", error);
	 }];*/
	
	BAAuthenticationService *service = [[BAAuthenticationService alloc] init];
	
	//TEST FOR AUTHENTICATION SERVICE LOGIN
	/*
	 BAUser *user = [[BAUser alloc] init];
	 //user.username = @"johnny";
	 user.userName = @"user";
	 //user.password = @"aslihasl";
	 user.password = @"passs";
	 
	 [service loginUser:user withResponseBlock:^(NSString *message, NSError *error) {
	 NSLog(@"Message: %@ \n Error: %@ \n", message, [error localizedDescription]);
	 BASessionData *session = [BASessionData sessionData];
	 NSLog(@"Token %@", session.authToken);
	 }];
	 */
	
	//TEST FOR AUTHENTICATION SERVICE SIGNUP
	/*
	 BANewUser *newUser = [[BANewUser alloc] init];
	 newUser.userName = @"johndoe";
	 newUser.password = @"Secretpassword&";
	 newUser.firstName = @"John";
	 newUser.lastName = @"Doe";
	 newUser.email = @"john.doe@gmail.com";
	 newUser.country = @"France";
	 newUser.age = @"44";
	 
	 [service signupUser:newUser withResponseBlock:^(NSDictionary *messages, NSError *error) {
	 NSLog(@"Message: %@ \n Error: %@ \n", messages, error);
	 BASessionData *session = [BASessionData sessionData];
	 NSLog(@"Userid %@", session.userId);
	 }];
	 */
	
	//TEST FOR AUTHENTICATION SERVICE LOGOUT
	/*
	 BASessionData *sessionData = [BASessionData sessionData];
	 sessionData.authToken = @"12456789abcdefg123456789abcdefg";
	 [service logoutUserwithResponseBlock:^(NSString *message, NSError *error) {
	 NSLog(@"Message: %@ \n Error: %@ \n", message, error);
	 }];
	 */
	
	//TEST FOR AUTHENTICATION SERIVCE RESET PASSWORD
	/*
	 NSString *email = @"adrian@adrian.com";
	 [service resetPassword:email withResponseBlock:^(NSString *message, NSError *error) {
	 NSLog(@"Message: %@ \n Error: %@ \n", message, error);
	 }];
	 */
	
	//TEST FOR AUTHENTICATION SERVICE CHANGE PASSWORD
	
	[service changePassword:@"" passwordToken:@"" withResponseBlock:^(NSString *message, NSError *error) {
		
	}];
}

@end
