//
//  BACategory.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/18/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "JSONModel.h"


@interface BACategory : JSONModel

@property (nonatomic, assign) NSInteger categoryId;
@property (nonatomic, strong) NSString *name;

@end
