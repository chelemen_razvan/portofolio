//
//  BARegisterViewController.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 22/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAFieldsValidator.h"
#import "BANewUser.h"
#import "BAAuthenticationService.h"
#import "BAKeyboardHandler.h"

@interface BARegisterViewController : UIViewController <UITextFieldDelegate,UIAlertViewDelegate>

- (IBAction)goBack:(UIButton *)sender;
- (IBAction)signUpButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (retain, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (retain, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (retain, nonatomic) IBOutlet UITextField *emailTextField;
@property (retain, nonatomic) IBOutlet UITextField *retypeEmailTextField;
@property (retain, nonatomic) IBOutlet UITextField *passwordTextField;
@property (retain, nonatomic) IBOutlet UITextField *retypePasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *signUpButton;

@property (weak, nonatomic) IBOutlet UILabel *registerFormErrorLabel;

@end
