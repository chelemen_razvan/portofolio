//
//  BACategoriesService.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/18/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BASessionData.h"
#import "BANetworking.h"
#import "BACategory.h"

typedef void(^CategoriesServiceSingleCategoryResponseBlock)(BACategory *category, NSError *error);
typedef void(^CategoriesServiceMultipleCategoriesResponseBlock)(NSArray *categories, NSError *error);
typedef void(^CategoriesServiceWithMessageResponseBlock)(NSString *message, NSError *error);

typedef  enum {
	BAIncomeCategory = 1,
	BAExpenseCategory = 2
} BaCategoryType;

@interface BACategoriesService : NSObject

- (void) getAllCategoriesForType:(BaCategoryType) type withResponseBlock:(CategoriesServiceMultipleCategoriesResponseBlock) responseBlock;

- (void) getCategoryForId:(NSInteger) categoryId withType:(BaCategoryType) type andResponseBlock:(CategoriesServiceSingleCategoryResponseBlock) responseBlock;

//add a new category with specified name and type
- (void) addCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock;

//change the category name for a category id (name and id provided in category parameter)
- (void) editCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock;

//delete the category with the specified id
- (void) deleteCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock;

@end
