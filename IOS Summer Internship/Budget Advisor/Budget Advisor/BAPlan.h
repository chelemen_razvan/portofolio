//
//  BAPlan.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 21/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "JSONModel.h"

@interface BAPlan : JSONModel

@property (nonatomic, assign) NSInteger amount;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, assign) NSInteger planId;

//date needs to be of the following form: yyyy/mm/dd
@property (nonatomic, strong) NSDate *dueBy;

@property (nonatomic, assign) BOOL active;
@property (nonatomic, assign) BOOL isSuitable;

@end
