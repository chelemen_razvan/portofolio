//
//  BACategoriesService.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/18/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BACategoriesService.h"

#define kCategoryResource @"categories"
#define kCategoryExpenseResource @"expenses"
#define kCategoryIncomeResource @"incomes"

#define kTokenURLParamKey @"token"

#define kJsonIncomeCategoryKey @"incomeCategory"
#define kJsonCategoryIdKey @"id"

@implementation BACategoriesService
{
	BANetworking *_networkService;
}

-(id) init
{
	self = [super init];
	if (self)
	{
		_networkService = [[BANetworking alloc] init];
	}
	return self;
}

- (void) getAllCategoriesForType:(BaCategoryType) type withResponseBlock:(CategoriesServiceMultipleCategoriesResponseBlock) responseBlock
{
	//get authentication token for request
	BASessionData *sessionData = [BASessionData sessionData];
	
	//choose resource according to category type
	NSString *requestString = nil;
	if (type == BAIncomeCategory)
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryIncomeResource];
	}
	else
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryExpenseResource];
	}
		
	[_networkService getResource:requestString withParameters:@{kTokenURLParamKey:sessionData.authToken} success:^(id data) {
		NSArray *categories = (NSArray *) data;
		NSMutableArray *resultArray = [[NSMutableArray alloc] init];
		for (NSDictionary *element in categories)
		{
			NSError *error = nil;
			BACategory *category = [[BACategory alloc] initWithDictionary:element error:&error];
			if (error == nil)
			{
				[resultArray addObject:category];
			}
		}
		responseBlock(resultArray, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(nil, error);
	}];
}

- (void) getCategoryForId:(NSInteger) categoryId withType:(BaCategoryType) type andResponseBlock:(CategoriesServiceSingleCategoryResponseBlock) responseBlock
{
	//get authentication token for request
	BASessionData *sessionData = [BASessionData sessionData];
	
	//choose resource according to category type
	NSString *requestString = nil;
	if (type == BAIncomeCategory)
	{
		requestString = [NSString stringWithFormat:@"%@/%@/%li", kCategoryResource, kCategoryIncomeResource, (long)categoryId];
	}
	else
	{
		requestString = [NSString stringWithFormat:@"%@/%@/%li", kCategoryResource, kCategoryExpenseResource, (long)categoryId];
	}
		
	[_networkService getResource:requestString withParameters:@{kTokenURLParamKey:sessionData.authToken} success:^(id data)
	{
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSError *error = nil;
		BACategory *category = [[BACategory alloc] initWithDictionary:dictionaryData error:&error];
		
		if (error != nil)
		{
			responseBlock(nil, error);
		}
		responseBlock(category, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(nil, error);
	}];
}

- (void) addCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock
{
	//choose resource according to category type
	NSString *requestString = nil;
	if (type == BAIncomeCategory)
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryIncomeResource];
	}
	else
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryExpenseResource];
	}
	
	//get userid and authentication token
	BASessionData *sessionData = [BASessionData sessionData];
	
	//format body data
	NSDictionary *jsonDictionary = @{kJsonIncomeCategoryKey:category.name, kJsonIncomeCategoryKey:sessionData.userId};
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
	
	
	[_networkService postResource:requestString withBodyData:jsonData andParameters:@{kTokenURLParamKey:sessionData.authToken} success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, error);
	}];
}

- (void) editCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock
{
	//choose resource according to category type
	NSString *requestString = nil;
	if (type == BAIncomeCategory)
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryIncomeResource];
	}
	else
	{
		requestString = [NSString stringWithFormat:@"%@/%@", kCategoryResource, kCategoryExpenseResource];
	}
	//get userid and authentication token
	BASessionData *sessionData = [BASessionData sessionData];
	
	//format body data
	NSInteger categoryId = category.categoryId;
	NSDictionary *jsonDictionary = @{kJsonIncomeCategoryKey:category.name, kJsonCategoryIdKey:[NSNumber numberWithInteger:categoryId], kJsonIncomeCategoryKey:sessionData.userId};
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];
	
	[_networkService putResource:requestString withBodyData:jsonData andParameters:@{kTokenURLParamKey:sessionData.authToken} success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, error);
	}];
}

- (void) deleteCategory:(BACategory *) category forType:(BaCategoryType) type withResponseBlock:(CategoriesServiceWithMessageResponseBlock) responseBlock
{
	//choose resource according to category type
	NSString *requestString = nil;
	if (type == BAIncomeCategory)
	{
		requestString = [NSString stringWithFormat:@"%@/%@/%li", kCategoryResource, kCategoryIncomeResource, (long)category.categoryId];
	}
	else
	{
		requestString = [NSString stringWithFormat:@"%@/%@/%li", kCategoryResource, kCategoryExpenseResource, (long)category.categoryId];
	}
	
	BASessionData *sessionData = [BASessionData sessionData];
	
	[_networkService deleteResource:requestString withParameters:@{kTokenURLParamKey:sessionData.authToken} success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:@"content"];
		responseBlock(message, error);
	}];
}

@end
