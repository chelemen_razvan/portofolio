//
//  BAEmailPasswordRecoveryViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/23/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAEmailPasswordRecoveryViewController.h"

#define kBottomViewToKeyboardSpace 10
#define kRedValueInvalidTextField 1.0
#define kGreenValueInvalidTextField 0.0
#define kBlueValueInvalidTextField 0.0
#define kAlphaValueInvalidTextField 0.2

@implementation BAEmailPasswordRecoveryViewController
{
	BAKeyboardHandler *_keyboardHandler;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_keyboardHandler = [[BAKeyboardHandler alloc] init];
	//make the rounded corners
	_resetButton.layer.cornerRadius = 5;
	_resetButton.clipsToBounds = YES;
	_cancelButton.layer.cornerRadius = 5;
	_cancelButton.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [_keyboardHandler registerKeyboardHandlerFor:self.view viewCoveredByKeyboard:_resetButton withTolerance:kBottomViewToKeyboardSpace];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_keyboardHandler unregisterKeyboardHandler];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	//hide keyboard
	[self.view endEditing:YES];
}

- (IBAction)resetButtonPressed:(id)sender
{
	//validate email
	BAFieldsValidator *fieldsValidator = [[BAFieldsValidator alloc] init];
	switch ([fieldsValidator validateEmail:_emailTextField.text])
	{
		case BAInvalidEmailFormat:
			_errorLabel.text = @"Email format is invalid";
			[_emailTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
			return;
		case BAEmailTooLong:
			_errorLabel.text = @"Email address is too long";
			[_emailTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
			return;
		default:
			break;
	}
	
	[self showLoadingScreen];
	//send password request
	BAAuthenticationService *authenticationService = [[BAAuthenticationService alloc] init];
	[authenticationService resetPassword:_emailTextField.text withResponseBlock:^(NSString *message, NSError *error) {
		[self hideLoadingScreen];
		if (error != nil)
		{
			_errorLabel.text = message;
			return;
		}
		
		[self performSegueWithIdentifier:@"passwordRecoverySegue" sender:sender];
	}];
}

- (IBAction)cancelButtonPressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)emailTextFieldDidEndedOnExit:(id)sender
{
	[sender resignFirstResponder];
}
@end
