//
//  BALoginViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAAuthenticationService.h"
#import "BAFieldsValidator.h"
#import "BAKeyboardHandler.h"
#import "BAOverlayViewController.h"

@interface BALoginViewController : BAOverlayViewController
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *passwordResetButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)loginPressed:(id)sender;
- (IBAction)emailOrPasswordTextFieldTextChanged:(id)sender;
- (IBAction)textFieldsEditingDidEndedOnExit:(id)sender;

@end
