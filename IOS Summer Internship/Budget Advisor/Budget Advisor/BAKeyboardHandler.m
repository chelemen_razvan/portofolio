//
//  BAKeyboardHandler.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/23/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAKeyboardHandler.h"

@implementation BAKeyboardHandler
{
	BOOL _shouldMoveView;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_shouldMoveView = YES;
	}
	return self;
}

-(void) registerKeyboardHandlerFor:(UIView *) rootView viewCoveredByKeyboard:(UIView *) coveredView withTolerance:(CGFloat) tolerance
{
	_rootView = rootView;
	_coveredView = coveredView;
	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void) unregisterKeyboardHandler
{
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void) shouldMoveView:(BOOL) move
{
	_shouldMoveView = move;
}

#pragma mark - keyboard movements

- (void)keyboardWillShow:(NSNotification *)notification
{
	if (!_shouldMoveView)
		return;
	
	CGFloat keyboardHeight = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
	CGFloat screenHeight = [[UIScreen mainScreen] bounds].size.height;
	
	CGFloat loginButtonPosition = _coveredView.frame.origin.y;
	CGFloat loginButtonHeight = _coveredView.frame.size.height;
	
	CGFloat tolerance = 10;
	CGFloat hiddenPortionHeight = keyboardHeight - (screenHeight - (loginButtonPosition + loginButtonHeight + tolerance));
	//if chosen view is not visible move the base view
	NSLog(@"%f", hiddenPortionHeight);
	if (hiddenPortionHeight > 0) {
		[UIView animateWithDuration:0.3 animations:^{
			CGRect rootViewFrame = _rootView.frame;
			rootViewFrame.origin.y = 0 - hiddenPortionHeight;
			_rootView.frame = rootViewFrame;
		}];
	}
    
}

-(void)keyboardWillHide:(NSNotification *)notification
{
	if (!_shouldMoveView)
		return;
	
	//put view back to its initial position
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rootViewFrame = _rootView.frame;
        rootViewFrame.origin.y = 0.0f;
        _rootView.frame = rootViewFrame;
    }];
}

@end
