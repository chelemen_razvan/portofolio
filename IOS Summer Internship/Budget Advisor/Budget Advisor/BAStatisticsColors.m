//
//  BAStatisticsColors.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAStatisticsColors.h"

@implementation BAStatisticsColors

- (NSArray *) getStatisticsColors
{
	return @[[UIColor colorWithRed:0.898 green:0.11 blue:0.137 alpha:1] /*#e51c23*/,
			 [UIColor colorWithRed:0.914 green:0.118 blue:0.388 alpha:1] /*#e91e63*/,
			 [UIColor colorWithRed:0.612 green:0.153 blue:0.69 alpha:1] /*#9c27b0*/,
			 [UIColor colorWithRed:0.404 green:0.227 blue:0.718 alpha:1] /*#673ab7*/,
			 [UIColor colorWithRed:0.247 green:0.318 blue:0.71 alpha:1] /*#3f51b5*/,
			 [UIColor colorWithRed:0.337 green:0.467 blue:0.988 alpha:1] /*#5677fc*/];
}

@end
