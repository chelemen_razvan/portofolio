//
//  BAStatisticsContainerViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAStatisticsContainerViewController.h"

#define SegueIdentifierFirst @"embedMonth"
#define SegueIdentifierSecond @"embedYear"

@interface BAStatisticsContainerViewController ()
@property (strong, nonatomic) NSString *currentSegueIdentifier;
@property (assign, nonatomic) BOOL transitionInProgress;
@end

@implementation BAStatisticsContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

	self.transitionInProgress = NO;
    _currentSegueIdentifier = SegueIdentifierFirst;
    [self performSegueWithIdentifier:_currentSegueIdentifier sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Instead of creating new VCs on each seque we want to hang on to existing
    // instances if we have it. Remove the second condition of the following
    // two if statements to get new VC instances instead.
    if ([segue.identifier isEqualToString:SegueIdentifierFirst])
        self.firstViewController = segue.destinationViewController;
	
    if ([segue.identifier isEqualToString:SegueIdentifierSecond])
        self.secondViewController = segue.destinationViewController;
	
    // If we're going to the first view controller.
    if ([segue.identifier isEqualToString:SegueIdentifierFirst])
	{
        // If this is not the first time we're loading this.
        if (self.childViewControllers.count > 0)
		{
            [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.firstViewController];
        }
        else
		{
            // If this is the very first time we're loading this we need to do
            // an initial load and not a swap.
            [self addChildViewController:segue.destinationViewController];
            UIView* destView = ((UIViewController *)segue.destinationViewController).view;
            destView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
            destView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
            [self.view addSubview:destView];
            [segue.destinationViewController didMoveToParentViewController:self];
        }
    }
    // By definition the second view controller will always be swapped with the
    // first one.
    else if ([segue.identifier isEqualToString:SegueIdentifierSecond])
	{
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:self.secondViewController];
    }
}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
	
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
	
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        self.transitionInProgress = NO;
    }];
}

- (BOOL)swapViewControllers
{
    if (self.transitionInProgress)
        return YES;
	
    self.transitionInProgress = YES;
    self.currentSegueIdentifier = ([self.currentSegueIdentifier isEqualToString:SegueIdentifierFirst]) ? SegueIdentifierSecond : SegueIdentifierFirst;
    
    if (([self.currentSegueIdentifier isEqualToString:SegueIdentifierFirst]) && self.firstViewController)
	{
        [self swapFromViewController:self.secondViewController toViewController:self.firstViewController];
        return NO;
    }
    
    if (([self.currentSegueIdentifier isEqualToString:SegueIdentifierSecond]) && self.secondViewController)
	{
        [self swapFromViewController:self.firstViewController toViewController:self.secondViewController];
        return NO;
    }
	
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
	
	return NO;
}

@end
