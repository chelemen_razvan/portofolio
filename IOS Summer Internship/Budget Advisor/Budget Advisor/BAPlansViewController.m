//
//  BAPlansController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 8/1/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAPlansViewController.h"

@implementation BAPlansViewController
- (IBAction)backPressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

@end
