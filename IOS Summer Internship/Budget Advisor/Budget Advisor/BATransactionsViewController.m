//
//  BATransactionsViewController.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 28/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BATransactionsViewController.h"
#import "ELActionCell.h"
#import "BANewTransactionViewController.h"
#define kNavigationBackgroundColor [UIColor colorWithRed:205.0/255 green:225.0/255 blue:233.0/255 alpha:1.0]


@interface BATransactionsViewController () <ELActionCellDelegate, UIActionSheetDelegate>

@end

@implementation BATransactionsViewController
{
    BATransactionService *transactionService;
    NSMutableArray *allTransactionsArray;
    NSIndexPath *selectedIndexPath;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    transactionService = [[BATransactionService alloc] init];
    self.transactionsTable.dataSource = self;
    selectedIndexPath = [[NSIndexPath alloc] init];
    
    // this will appear as the title in the navigation bar
//    UILabel *label = [[UILabel alloc] initWithFrame:CGRectZero];
//    label.backgroundColor = kNavigationBackgroundColor;
//    label.font = [UIFont boldSystemFontOfSize:20.0];
//    label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
//    label.textAlignment = NSTextAlignmentCenter;
//    // ^-Use UITextAlignmentCenter for older SDKs.
//    label.textColor = [UIColor yellowColor]; // change this color
//    self.navigationItem.titleView = label;
//    [[UINavigationBar appearance] setBackgroundColor:kNavigationBackgroundColor];
 //   [[UINavigationBar appearance] setBarTintColor:[UIColor greenColor]];
    //label.text = NSLocalizedString(@"PageThreeTitle", @"");
    //[label sizeToFit];
    
    
}

- (void) viewWillAppear:(BOOL)animated
{
    [self loadTransactions];
    self.addTransactionButton.layer.cornerRadius = self.addTransactionButton.bounds.size.width / 2.0;
    [[UINavigationBar appearance] setBarTintColor:kNavigationBackgroundColor];
}
     
- (void) loadTransactions
{
    allTransactionsArray = [[NSMutableArray alloc] init];
    
#warning test data to be deleted when the server is functional
    BATransaction *tmpTransaction = [[BATransaction alloc] init];
    tmpTransaction.date = [NSDate date];
    
    tmpTransaction.category.name = @"FOOD";
    tmpTransaction.description = @"Dinner";
    tmpTransaction.type = @"income";
    tmpTransaction.amount = 22;
    [allTransactionsArray addObject:tmpTransaction];

    BATransaction *tmpTransaction2 = [[BATransaction alloc] init];
    tmpTransaction2.date = [NSDate date];
    tmpTransaction2.category.name = @"HOLIDAY";
    tmpTransaction2.description = @"Hotel";
    tmpTransaction2.type = @"expense";
    tmpTransaction2.amount = 1800;
    [allTransactionsArray addObject:tmpTransaction2];
    
    BATransaction *tmpTransaction3 = [[BATransaction alloc] init];
    tmpTransaction3.date = [NSDate date];
    tmpTransaction3.category.name = @"HOLIDAY";
    tmpTransaction3.description = @"Gas";
    tmpTransaction3.type = @"expense";
    tmpTransaction3.amount = 420;
    [allTransactionsArray addObject:tmpTransaction3];
    
    BATransaction *tmpTransaction4 = [[BATransaction alloc] init];
    tmpTransaction4.date = [NSDate date];
    tmpTransaction4.category.name = @"HOLIDAY";
    tmpTransaction4.description = @"Wine";
    tmpTransaction4.type = @"expense";
    tmpTransaction4.amount = 800;
    [allTransactionsArray addObject:tmpTransaction4];
    
    BATransaction *tmpTransaction5 = [[BATransaction alloc] init];
    tmpTransaction5.date = [NSDate date];
    tmpTransaction5.category.name = @"MONEY";
    tmpTransaction5.description = @"Salary";
    tmpTransaction5.type = @"income";
    tmpTransaction5.amount = 5000;
    [allTransactionsArray addObject:tmpTransaction5];

    BATransaction *tmpTransaction6 = [[BATransaction alloc] init];
    tmpTransaction6.date = [NSDate date];
    tmpTransaction6.category.name = @"MONEY";
    tmpTransaction6.description = @"Lotery";
    tmpTransaction6.type = @"income";
    tmpTransaction6.amount = 7000;
    [allTransactionsArray addObject:tmpTransaction6];
    
    BATransaction *tmpTransaction7 = [[BATransaction alloc] init];
    tmpTransaction7.date = [NSDate date];
    tmpTransaction7.category.name = @"SERVICES";
    tmpTransaction7.description = @"Rent";
    tmpTransaction7.type = @"expense";
    tmpTransaction7.amount = 1300;
    [allTransactionsArray addObject:tmpTransaction7];

    BATransaction *tmpTransaction8 = [[BATransaction alloc] init];
    tmpTransaction8.date = [NSDate date];
    tmpTransaction8.category.name = @"SERVICES";
    tmpTransaction8.description = @"Electrica";
    tmpTransaction8.amount = 130;
    tmpTransaction8.type = @"expense";
    [allTransactionsArray addObject:tmpTransaction8];
    
    BATransaction *tmpTransaction9 = [[BATransaction alloc] init];
    tmpTransaction9.date = [NSDate date];
    tmpTransaction9.category.name = @"SERVICES";
    tmpTransaction9.description = @"E-On";
    tmpTransaction9.type = @"expense";
    tmpTransaction9.amount = 70;
    [allTransactionsArray addObject:tmpTransaction9];
    
    BATransaction *tmpTransaction10 = [[BATransaction alloc] init];
    tmpTransaction10.date = [NSDate date];
    tmpTransaction10.category.name = @"LUCK";
    tmpTransaction10.description = @"Lottery";
    tmpTransaction10.amount = 2500;
    tmpTransaction10.type = @"income";
    [allTransactionsArray addObject:tmpTransaction10];

    BATransaction *tmpTransaction11 = [[BATransaction alloc] init];
    tmpTransaction11.date = [NSDate date];
    tmpTransaction11.category.name = @"CAR";
    tmpTransaction11.description = @"Oil Change";
    tmpTransaction11.amount = 900;
    tmpTransaction11.type = @"expense";
    [allTransactionsArray addObject:tmpTransaction11];




#warning to uncomment once the server is available
//    [transactionService retrieveAllIncomesWithResponseBlock:^(NSArray *transactionsArray, NSError *error) {
//        if (error != nil)
//        {
//            [self showErrorScreenWithMessage:@"Could not retrieve the data" andRetryBlock:^{
//                //hide the error overlay
//                [self hideErrorScreen];
//                //retry
//                [self loadTransactions];
//            }];
//            return;
//        }
//        
//        //populate allTransactionsArray with the Incomes
//        allTransactionsArray = [[NSMutableArray alloc] initWithArray:transactionsArray];
//      }];
//    
//    [transactionService retrieveAllExpensesWithResponseBlock:^(NSArray *transactionsArray, NSError *error) {
//        if (error != nil)
//        {
//            [self showErrorScreenWithMessage:@"Could not retrieve the data" andRetryBlock:^{
//                //hide the error overlay
//                [self hideErrorScreen];
//                //retry
//                [self loadTransactions];
//            }];
//            return;
//        }
//        
//        //add the Expenses to the allTransactionsArray
//        for (id object in transactionsArray)
//        {
//            [allTransactionsArray addObject:object];
//        }
//    }];
}

#pragma mark - Transactions tableview datasource methods

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [allTransactionsArray count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BATransactionsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionViewCell" forIndexPath:indexPath];
    cell.delegate = self;
    BATransaction *transaction = [allTransactionsArray objectAtIndex:indexPath.row];
    
    NSDateFormatter *DateFormatter=[[NSDateFormatter alloc] init];
    [DateFormatter setDateFormat:@"MM/dd"];
    [DateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSString *dateString = [DateFormatter stringFromDate:transaction.date];
    
    cell.dateLabel.text = [dateString substringToIndex:5];
    cell.categoryLabel.text = transaction.category.name;
    cell.descriptionLabel.text = transaction.description;
    if ([transaction.type isEqualToString:@"expense"])
    {
        cell.amountLabel.text = [NSString stringWithFormat:@"-%li",(long)transaction.amount];
    }
    else
    {
        cell.amountLabel.text = [NSString stringWithFormat:@"%li",(long)transaction.amount];
    }
    //cells not to remain highlighted when tapped
    [self.transactionsTable setEditing:YES animated:YES];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didTapButtonAtIndex:(NSUInteger)buttonIndex forCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    if (buttonIndex == 0)
    //Edit button tapped
    {
        [cell setEditing:NO animated:YES];
        selectedIndexPath = indexPath;
        [self performSegueWithIdentifier:@"editBANewTransactionViewController" sender:self];
    }
    else
    //Delete button tapped
    {
#warning to uncomment the below code once the server is functional
        
//        BATransaction *transactionToDelete = [allTransactionsArray objectAtIndex:indexPath.row];
//        if ([transactionToDelete.type isEqualToString:@"income"])
//        //transaction type is "income"
//        {
//            [transactionService deleteAnIncome:transactionToDelete withResponseBlock:^(NSString *message, NSError *error) {
//                    if (error != nil)
//                    {
//                        [self showErrorScreenWithMessage:@"Could not delete the income, please try again later" andRetryBlock:^{
//                            //hide the error overlay
//                            [self hideErrorScreen];
//                            }];
//                        return;
//                    }
//                    else
//                    {
//                        //transaction deleted from the server with success, deleting it from the tableview as well
//                        [allTransactionsArray removeObjectAtIndex:indexPath.row];
//                        [self.transactionsTable reloadData];
//                        [cell setEditing:NO animated:YES];
//                    }
//                }];
//            }
//        else
//        //transaction type is "expense"
//        {
//            [transactionService deleteAnExpense:transactionToDelete withResponseBlock:^(NSString *message, NSError *error) {
//                if (error != nil)
//                {
//                    [self showErrorScreenWithMessage:@"Could not delete the expense, please try again later" andRetryBlock:^{
//                        //hide the error overlay
//                        [self hideErrorScreen];
//                    }];
//                    return;
//                }
//                else
//                {
//                    //transaction deleted from the server with success, deleting it from the tableview as well
//                    [allTransactionsArray removeObjectAtIndex:indexPath.row];
//                    [self.transactionsTable reloadData];
//                    [cell setEditing:NO animated:YES];
//                }
//            }];
//        }
        
#warning to delete the below test code
    [allTransactionsArray removeObjectAtIndex:indexPath.row];
    [self.transactionsTable reloadData];
    [cell setEditing:NO animated:YES];
        
    }
}

- (BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"editBANewTransactionViewController"])
    {
        [[segue destinationViewController] setSelectedTransaction:[allTransactionsArray objectAtIndex:selectedIndexPath.row]];
    }
}

- (IBAction)addTransactionButton:(id)sender
{
    [self performSegueWithIdentifier:@"showBANewTransactionViewController" sender:self];
}
@end
