//
//  BANetworking.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/16/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

//data is a foundation container (NSDictionary or NSArray) depending on the json returned
typedef void(^NetworkingSuccessBlock)(id data);
typedef void(^NetworkingFailureBlock)(id data, NSError *error);

@interface BANetworking : NSObject

- (void) getResource:(NSString *) resource withParameters:(NSDictionary *)parameters success:(NetworkingSuccessBlock) success failure:(NetworkingFailureBlock)failure;

- (void) postResource:(NSString *) resource withBodyData:(NSData *) bodyData andParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure;

- (void) deleteResource:(NSString *) resource withParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure;

- (void) putResource:(NSString *) resource withBodyData:(NSData *) bodyData andParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure;

@end
