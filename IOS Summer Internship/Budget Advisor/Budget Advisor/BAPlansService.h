//
//  BAPlansService.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 21/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BANetworking.h"
#import "BASessionData.h"
#import "BAPlan.h"
#import "BAAuthenticationService.h"

typedef void(^PlansServiceDataResponseBlock)(NSDictionary *data, NSError *error);
typedef void(^PlansServiceMessageResponseBlock)(NSString *message, NSError *error);
typedef void(^PlansServiceArrayOfPlansResponseBlock)(NSArray *plansArray, NSError *error);


@interface BAPlansService : NSObject

- (void) addNewPlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock;
- (void) editPlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock;
- (void) deletePlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock;
- (void) retrievePlanWithId:(NSInteger)planId withResponseBlock:(PlansServiceDataResponseBlock)responseBlock;
- (void) retrieveAllPlansWithResponseBlock:(PlansServiceArrayOfPlansResponseBlock)responseBlock;

@end
