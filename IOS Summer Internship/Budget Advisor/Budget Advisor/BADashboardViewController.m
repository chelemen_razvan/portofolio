//
//  BADashboardViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/24/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BADashboardViewController.h"
#import "BALoginViewController.h"

#define kCurrency @"RON"

@implementation BADashboardViewController
{
	BAPlansService *_plansService;
	BAStatisticsService *_statisticsService;
	NSMutableArray *_activePlansArray;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _plansService = [[BAPlansService alloc] init];
	_statisticsService = [[BAStatisticsService alloc] init];
	
	[self makeButtonsCornersRound];
	
	//set tableView's datasource
	_activePlansTable.dataSource = self;
}

- (void) makeButtonsCornersRound
{
	_addTransactionButton.layer.cornerRadius = 22;
	_addTransactionButton.clipsToBounds = YES;
	
	_activePlansButton.layer.cornerRadius = 22;
	_activePlansButton.clipsToBounds = YES;
}

- (void) viewWillAppear:(BOOL)animated
{
	[self loadAccountBalanceLabels];
	[self loadActivePlans];
}

- (void) loadAccountBalanceLabels
{
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
	
	[_statisticsService getStatisticOn:dateComponents.month from:dateComponents.year WithResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive the data" andRetryBlock:^{
				//hide the error overlay
				[self hideErrorScreen];
				//call the method again
				[self loadAccountBalanceLabels];
			}];
			return;
		}
		
		NSInteger income = 0;
		NSInteger expense = 0;
		
		//compute income and expenses on current month
		for (BAStatisticElement *element in array)
		{
			income += element.incomes;
			expense += element.expenses;
		}
		
		NSInteger remainingValue = income - expense;
		
		//populate labels with correponding values
		if (remainingValue > 0)
		{
			NSString *remainingValueFormatted = [NSString stringWithFormat:@"+%i %@", remainingValue, kCurrency];
			_currentlyAvailableLabel.text = remainingValueFormatted;
			_remainingLabel.text = remainingValueFormatted;
		}
		else
		{
			NSString *remainingValueFormatted = [NSString stringWithFormat:@"%i %@", remainingValue, kCurrency];
			_currentlyAvailableLabel.text = remainingValueFormatted;
			_remainingLabel.text = remainingValueFormatted;
		}
		
		_incomeLabel.text = [NSString stringWithFormat:@"+%i %@", income, kCurrency];
		_expenseLabel.text = [NSString stringWithFormat:@"-%i %@", expense, kCurrency];
	}];
}

- (void) loadActivePlans
{
	[_plansService retrieveAllPlansWithResponseBlock:^(NSArray *plans, NSError *error) {
		if (error != nil)
		{
            	[self showErrorScreenWithMessage:@"Could not retrive the data" andRetryBlock:^{
				//hide the error overlay
				[self hideErrorScreen];
				//call the method again
				[self loadActivePlans];
			}];
			return;
		}
		
		_activePlansArray = [[NSMutableArray alloc] init];
		for (BAPlan *plan in plans)
		{
			if (plan.active)
				[_activePlansArray addObject:plan];
		}
		
		//tell table to reload data
		[_activePlansTable reloadData];
	}];
}

#pragma mark -Active plans tableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_activePlansArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BAActivePlansTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActivePlanCell" forIndexPath:indexPath];
    BAPlan *plan = [_activePlansArray objectAtIndex:indexPath.row];
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:plan.dueBy];
	BADateMapper *dateMapper = [[BADateMapper alloc] init];
	NSString *monthName = [dateMapper monthNumberToAbbreviation:([dateComponents month]-1)];
	
    cell.detailTextLabel.text = plan.description;
	cell.dueByLabel.text = [NSString stringWithFormat:@"%@ %i", monthName, [dateComponents day]];
	cell.amountLabel.text = [NSString stringWithFormat:@"%i %@", plan.amount, kCurrency];
    
    return cell;
}


- (IBAction)signOutTapped:(id)sender
{
    BALoginViewController *LoginViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    LoginViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:LoginViewController animated:YES completion:nil];

    self.tabBarController.tabBar.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
}
    
@end
