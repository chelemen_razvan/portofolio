//
//  BAPlansService.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 21/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAPlansService.h"
#define kPlansResource @"plans"
#define kJsonContentKey @"Content"
#define kJsonTokenKey @"token"
#define kAmountKey @"amount"
#define kDescriptionKey @"description"
#define kDueByKey @"dueBy"
#define kActiveKey @"active"
#define kIsSuitableKey @"isSuitable"

@implementation BAPlansService

- (void) addNewPlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock
{
    NSData *jsonAddNewPlan = [[plan toJSONStringWithKeys:@[kAmountKey, kDescriptionKey, kDueByKey, kActiveKey, kIsSuitableKey]] dataUsingEncoding:NSUTF8StringEncoding];
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *addNewPlanParameter = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking postResource:kPlansResource withBodyData:jsonAddNewPlan andParameters:addNewPlanParameter success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) editPlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock
{
    NSString *editPlanResource = [NSString stringWithFormat:@"%@%li", kPlansResource, (long)plan.planId];
    NSData *jsonEditPlan = [[plan toJSONStringWithKeys:@[kAmountKey, kDescriptionKey, kDueByKey, kActiveKey, kIsSuitableKey]] dataUsingEncoding:NSUTF8StringEncoding];
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *editPlanParameter = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking putResource:editPlanResource withBodyData:jsonEditPlan andParameters:editPlanParameter success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) deletePlan:(BAPlan *)plan withResponseBlock:(PlansServiceMessageResponseBlock)responseBlock
{
    NSString *deletePlanResource = [NSString stringWithFormat:@"%@%li", kPlansResource, (long)plan.planId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *deletePlanParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking deleteResource:deletePlanResource withParameters:deletePlanParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) retrievePlanWithId:(NSInteger)planId withResponseBlock:(PlansServiceDataResponseBlock)responseBlock
{
    NSString *retrievePlanResource = [NSString stringWithFormat:@"%@%li", kPlansResource, (long)planId];
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrievePlanParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:retrievePlanResource withParameters:retrievePlanParameters success:^(id data) {
        responseBlock(data, nil);
    } failure:^(id data, NSError *error) {
        responseBlock([data objectForKey:kJsonContentKey], error);
    }];
}

- (void) retrieveAllPlansWithResponseBlock:(PlansServiceArrayOfPlansResponseBlock)responseBlock
{
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrieveAllPlansParameter = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:kPlansResource withParameters:retrieveAllPlansParameter success:^(id data) {
        
        NSArray *plans = (NSArray *) data;
		NSMutableArray *resultArray = [[NSMutableArray alloc] init];
		for (NSDictionary *element in plans)
		{
			NSError *error = nil;
			BAPlan *plan = [[BAPlan alloc] initWithDictionary:element error:&error];
			if (error == nil)
			{
				[resultArray addObject:plan];
			}
		}
		responseBlock(resultArray, nil);
     
        
    } failure:^(id data, NSError *error) {
        responseBlock(nil, error);
    }];
}


@end
