//
//  BATabBarViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/25/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BATabBarViewController : UITabBarController

@end
