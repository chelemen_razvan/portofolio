//
//  BALoginViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BALoginViewController.h"

#define kBottomViewToKeyboardSpace 10
#define kRedValueInvalidTextField 1.0
#define kGreenValueInvalidTextField 0.0
#define kBlueValueInvalidTextField 0.0
#define kAlphaValueInvalidTextField 0.2

@implementation BALoginViewController
{
	BAAuthenticationService *_authenticationService;
	BAKeyboardHandler *_keyboardHandler;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_authenticationService = [[BAAuthenticationService alloc] init];
	_keyboardHandler = [[BAKeyboardHandler alloc] init];
	//make the rounded corners
	_loginButton.layer.cornerRadius = 5;
	_loginButton.clipsToBounds = YES;
	[_loginButton setEnabled:NO];
	_loginButton.backgroundColor = [UIColor grayColor];
#warning to delete - test data
    self.usernameTextField.text = @"john.doe@iquestgroup.com";
    self.passwordTextField.text = @"aaaaaaaaaaaaA@aad";
    self.loginButton.enabled = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [_keyboardHandler registerKeyboardHandlerFor:self.view viewCoveredByKeyboard:_loginButton withTolerance:kBottomViewToKeyboardSpace];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_keyboardHandler unregisterKeyboardHandler];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	
	//hide keyboard
	[self.view endEditing:YES];
}

- (IBAction)loginPressed:(id)sender
{
	if (![self validateTextFields])
		return;
	
	[self showLoadingScreen];
	BACredential *userCredentials = [[BACredential alloc] init];
	userCredentials.userName = _usernameTextField.text;
	userCredentials.password = _passwordTextField.text;
	[_authenticationService loginUser:userCredentials withResponseBlock:^(NSString *message, NSError *error) {
		[self hideLoadingScreen];
		if (error != nil) {
			NSLog(@"%@", error);
			_errorLabel.text = message;
			return;
		}
		
		[self performSegueWithIdentifier:@"loginSegue" sender:sender];
		
	}];
		
}

//check if fields are valid and show an error message accordingly
-(BOOL) validateTextFields
{
	BAFieldsValidator *fieldsValidator = [[BAFieldsValidator alloc] init];
	
	//check and color invalid text fields
	if (![fieldsValidator validateName:_usernameTextField.text])
		[_usernameTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
	
	if ([fieldsValidator validatePassword:_passwordTextField.text] != BAValidPassword)
		[_passwordTextField setBackgroundColor:[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.2]];
	
	//show messages according to the validator response
	if (![fieldsValidator validateName:_usernameTextField.text])
	{
		_errorLabel.text = @"Invalid username";
		return NO;
	}
	
	switch ([fieldsValidator validatePassword:_passwordTextField.text])
	{
		case BAPasswordTooShort:
			_errorLabel.text = @"Password must be atleast 7 characters long";
			return NO;
		case BAPasswordTooLong:
			_errorLabel.text = @"Password maximum size is 20 characters";
			return NO;
		case BAPasswordWithoutUppercase:
			_errorLabel.text = @"Password must contain atleast 1 uppercase character";
			return NO;
		case BAPasswordWithoutSpecialCharacter:
			_errorLabel.text = @"Password must contain atleast 1 special character";
			return NO;
		default:
			break;
	}
	
	return YES;
}

//enable or disable login button when there is/isn't text in email and password fields
- (IBAction)emailOrPasswordTextFieldTextChanged:(id)sender
{
	BOOL isEmailTextFieldEmpty = _usernameTextField.text && _usernameTextField.text.length == 0;
	BOOL isPasswordTextFieldEmpty = _passwordTextField.text && _passwordTextField.text.length == 0;
	if (isEmailTextFieldEmpty || isPasswordTextFieldEmpty)
	{
		[_loginButton setEnabled:NO];
		_loginButton.backgroundColor = [UIColor grayColor];
	}
	else
	{
		[_loginButton setEnabled:YES];
		_loginButton.backgroundColor = [UIColor colorWithRed:42/255.0f green:184/255.0f blue:148/255.0f alpha:1.0f];
	}
}

//hide keyboard when return is pressed and when textfields are touched
- (IBAction)textFieldsEditingDidEndedOnExit:(id)sender
{
	[sender resignFirstResponder];
}

@end
