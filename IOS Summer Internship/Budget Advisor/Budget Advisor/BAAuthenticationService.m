//
//  BAAuthenticationService.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/17/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAAuthenticationService.h"

#define kJsonMessageKey @"message"
#define kJsonNewPasswordKey @"newPassword"
#define kJsonUserIdKey @"userId"
#define kJsonResetTokenKey @"resetToken"
#define kJsonTokenKey @"token"

#define kEmailURLParameterKey @"email"

#define kLoginResource @"authenticate"
#define kSignupResource @"users"
#define kResetPasswordResource @"users/password-reset"
#define kChangePasswordResource @"users/password"
#define kTokenValidationResource @"authenticate"

#define kJsonKeyMissing 300
#define kErrorDomainName @"budget.advisor.error"

@implementation BAAuthenticationService
{
	BANetworking *_networkService;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_networkService = [[BANetworking alloc] init];
	}
	return self;
}

- (void) loginUser:(BACredential *) user withResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock
{
	NSData *jsonUserLoginData = [[user toJSONString] dataUsingEncoding:NSUTF8StringEncoding];
	[_networkService postResource:kLoginResource withBodyData:jsonUserLoginData andParameters:nil success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		if ([dictionaryData objectForKey:kJsonTokenKey] == nil) {
			NSError *error = [NSError errorWithDomain:kErrorDomainName code:kJsonKeyMissing userInfo:@{@"message":@"Invalid response"}];
			responseBlock(@"Login failed", error);
			return;
		}
			
		//network response is ok, we got the token
		NSString *authToken = [dictionaryData objectForKey:kJsonTokenKey];
		BASessionData *sessionData = [BASessionData sessionData];
		sessionData.authToken = authToken;
		responseBlock(@"Login successfully", nil);
	} failure:^(id data, NSError *error) {
		//some error happend
		responseBlock(@"Login failed", error);
	}];
}

- (void) signupUser:(BANewUser *) newUser withResponseBlock:(AuthenticationServiceSignupResponseBlock) responseBlock
{
	NSData *jsonUserData = [[newUser toJSONString] dataUsingEncoding:NSUTF8StringEncoding];
	[_networkService postResource:kSignupResource withBodyData:jsonUserData andParameters:nil success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		
		//save the user id in session storage
		BASessionData *sessionData = [BASessionData sessionData];
		sessionData.userId = [dictionaryData objectForKey:kJsonUserIdKey];
		
		responseBlock(data, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(data, error);
	}];
}

- (void) logoutUserwithResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock
{
	//get token
	BASessionData *sessionData = [BASessionData sessionData];
	NSString *authToken = sessionData.authToken;
	
	//send delete request for the token
	NSString *logoutUserResource = [NSString stringWithFormat:@"%@/%@", kLoginResource, authToken];
	[_networkService deleteResource:logoutUserResource withParameters:nil success:^(id data) {
		//everything went well send success message
		responseBlock(@"Logout successfully", nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		responseBlock([dictionaryData objectForKey:kJsonMessageKey], error);
	}];
}

- (void) resetPassword:(NSString *) email withResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock
{
	NSDictionary *parameters = @{kEmailURLParameterKey: email};
	
	[_networkService postResource:kResetPasswordResource withBodyData:nil andParameters:parameters success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		if ([dictionaryData objectForKey:kJsonMessageKey] == nil) {
			NSError *error = [NSError errorWithDomain:kErrorDomainName code:kJsonKeyMissing userInfo:@{@"message":@"Invalid response"}];
			responseBlock(@"Password reset failed", error);
			return;
		}
		
		NSString *message = [dictionaryData objectForKey:kJsonMessageKey];
		responseBlock(message, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:kJsonMessageKey];
		responseBlock(message, error);
	}];
}

- (void) changePassword:(NSString *) newPassword passwordToken:(NSString *) token withResponseBlock: (AuthenticationServiceLoginResponseBlock) responseBlock
{
	//convert newPassword to JSON
	NSError *error;
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:@{kJsonResetTokenKey: token, kJsonNewPasswordKey:newPassword} options:0 error:&error];
	
	[_networkService postResource:kChangePasswordResource withBodyData: jsonData andParameters:nil success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:kJsonMessageKey];
		responseBlock(message, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		NSString *message = [dictionaryData objectForKey:kJsonMessageKey];
		responseBlock(message, error);
	}];
}

- (void) validateTokenWithResponseBlock:(AuthenticationServiceSignupResponseBlock) responseBlock
{
	BASessionData *sessionData = [BASessionData sessionData];
	NSString *validateTokenResource = [NSString stringWithFormat:@"%@/%@",kTokenValidationResource ,sessionData.authToken];
	
	[_networkService getResource:validateTokenResource withParameters:nil success:^(id data) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		
		//everything went good so store the userId
		sessionData.userId = [dictionaryData objectForKey:kJsonUserIdKey];
		
		responseBlock(data, nil);
	} failure:^(id data, NSError *error) {
		NSDictionary *dictionaryData = (NSDictionary *) data;
		responseBlock(dictionaryData, error);
	}];
}

@end
