//
//  BADashboardViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/24/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAPlansService.h"
#import "BAStatisticsService.h"
#import "BAStatisticElement.h"
#import "BAPlan.h"
#import "BAActivePlansTableViewCell.h"
#import "BADateMapper.h"
#import "BAOverlayViewController.h"

@interface BADashboardViewController : BAOverlayViewController <UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *addTransactionButton;
@property (weak, nonatomic) IBOutlet UIButton *activePlansButton;
@property (weak, nonatomic) IBOutlet UILabel *currentlyAvailableLabel;
@property (weak, nonatomic) IBOutlet UILabel *incomeLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseLabel;
@property (weak, nonatomic) IBOutlet UILabel *remainingLabel;
@property (weak, nonatomic) IBOutlet UITableView *activePlansTable;
- (IBAction)signOutTapped:(id)sender;

@end
