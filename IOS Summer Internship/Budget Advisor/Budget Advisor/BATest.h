//
//  BATest.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BAAuthenticationService.h"

@interface BATest : NSObject

- (void) test;

@end
