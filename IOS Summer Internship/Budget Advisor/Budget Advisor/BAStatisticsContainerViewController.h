//
//  BAStatisticsContainerViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAMonthStatisticsViewController.h"
#import "BAYearStatisticsViewController.h"

@interface BAStatisticsContainerViewController : UIViewController

@property (strong, nonatomic) BAMonthStatisticsViewController *firstViewController;
@property (strong, nonatomic) BAYearStatisticsViewController *secondViewController;
@property (nonatomic, weak) UIViewController *parentController;

- (BOOL)swapViewControllers;

@end
