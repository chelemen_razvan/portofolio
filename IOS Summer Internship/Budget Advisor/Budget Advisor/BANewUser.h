//
//  BANewUser.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/17/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BACredential.h"

@interface BANewUser : BACredential

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, assign) NSString *age;

@end
