//
//  BAIncome.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 17/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BATransaction.h"

@implementation BATransaction

-(id)init
{
    if (self = [super init])
    {
        self.category = [[BACategory alloc] init];
    }
    return self;
}
@end
