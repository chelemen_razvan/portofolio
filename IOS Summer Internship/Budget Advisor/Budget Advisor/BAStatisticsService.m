//
//  BAStatisticsService.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAStatisticsService.h"

#define kStatisticsResource @"statistics"

@implementation BAStatisticsService
{
	BANetworking *_networkService;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_networkService = [[BANetworking alloc] init];
	}
	return self;
}

- (void) getStatisticOnAllYearsWithResponseBlock:(StatisticsServiceResponseBlock) responseBlock
{
	[_networkService getResource:kStatisticsResource withParameters:nil success:^(id data) {
		NSArray *categories = (NSArray *) data;
		NSMutableArray *resultArray = [[NSMutableArray alloc] init];
		for (NSDictionary *element in categories)
		{
			NSError *error = nil;
			BAStatisticElement *category = [[BAStatisticElement alloc] initWithDictionary:element error:&error];
			if (error == nil)
			{
				[resultArray addObject:category];
			}
		}
		responseBlock(resultArray, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(nil, error);
	}];
}

- (void) getStatisticOn:(NSInteger) year withResponseBlock:(StatisticsServiceResponseBlock) responseBlock;
{
	NSString *requestString = [NSString stringWithFormat:@"%@/%lu", kStatisticsResource, (long)year];
	
	[_networkService getResource:requestString withParameters:nil success:^(id data) {
		NSArray *categories = (NSArray *) data;
		NSMutableArray *resultArray = [[NSMutableArray alloc] init];
		for (NSDictionary *element in categories)
		{
			NSError *error = nil;
			BAStatisticElement *category = [[BAStatisticElement alloc] initWithDictionary:element error:&error];
			if (error == nil)
			{
				[resultArray addObject:category];
			}
		}
		responseBlock(resultArray, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(nil, error);
	}];
}

- (void) getStatisticOn:(NSInteger) month from:(NSInteger) year WithResponseBlock:(StatisticsServiceResponseBlock) responseBlock
{
	NSString *requestString = [NSString stringWithFormat:@"%@/%lu/%lu", kStatisticsResource, (long)year, (long)month];
	
	[_networkService getResource:requestString withParameters:nil success:^(id data) {
		NSArray *categories = (NSArray *) data;
		NSMutableArray *resultArray = [[NSMutableArray alloc] init];
		for (NSDictionary *element in categories)
		{
			NSError *error = nil;
			BAStatisticElement *category = [[BAStatisticElement alloc] initWithDictionary:element error:&error];
			if (error == nil)
			{
				[resultArray addObject:category];
			}
		}
		responseBlock(resultArray, nil);
	} failure:^(id data, NSError *error) {
		responseBlock(nil, error);
	}];
}

@end
