//
//  BAStatisticElement.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "JSONModel.h"

@interface BAStatisticElement : JSONModel

//this is only available for statistics on all years or on a year
//@property (nonatomic, strong) NSString *year;
//@property (nonatomic, strong) NSString *month;

@property (nonatomic, assign) NSInteger incomes;
@property (nonatomic, assign) NSInteger expenses;

//this is only available for monthly statistics
@property (nonatomic, strong) NSString *category;

@end
