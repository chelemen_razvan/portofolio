//
//  BAPasswordRecoveryViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAPasswordRecoveryViewController.h"

#define kBottomViewToKeyboardSpace 10
#define kRedValueInvalidTextField 1.0
#define kGreenValueInvalidTextField 0.0
#define kBlueValueInvalidTextField 0.0
#define kAlphaValueInvalidTextField 0.2

@implementation BAPasswordRecoveryViewController
{
	BAKeyboardHandler *_keyboardHandler;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_keyboardHandler = [[BAKeyboardHandler alloc] init];
    //make the rounded corners
	_saveButton.layer.cornerRadius = 5;
	_saveButton.clipsToBounds = YES;
	
	_backButton.layer.cornerRadius = 5;
	_backButton.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [_keyboardHandler registerKeyboardHandlerFor:self.view viewCoveredByKeyboard:_saveButton withTolerance:kBottomViewToKeyboardSpace];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_keyboardHandler unregisterKeyboardHandler];
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	
	//hide keyboard
	[self.view endEditing:YES];
}

- (IBAction)saveButtonPressed:(id)sender
{
	if (![self validateTextFields])
		return;
	
	
	[self showLoadingScreen];
	BAAuthenticationService *authenticationService = [[BAAuthenticationService alloc] init];
	[authenticationService changePassword:_passwordTextField.text passwordToken:_tokenTextField.text withResponseBlock:^(NSString *message, NSError *error) {
		[self hideLoadingScreen];
		if (error != nil) {
			_errorLabel.text = [error localizedDescription];
			return;
		}
		
		//show an success message
		_errorLabel.textColor = [UIColor greenColor];
		_errorLabel.text = @"Your password was changed successfully";
	}];
}

-(BOOL) validateTextFields
{
	BAFieldsValidator *fieldsValidator = [[BAFieldsValidator alloc] init];
	
	//check and color password field
	if ([fieldsValidator validatePassword:_passwordTextField.text] != BAValidEmail)
		[_passwordTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
	
	//show messages according to the validator response
	switch ([fieldsValidator validatePassword:_passwordTextField.text])
	{
		case BAPasswordTooLong:
			_errorLabel.text = @"Password must be atleast 7 characters long";
			return NO;
		case BAPasswordTooShort:
			_errorLabel.text = @"Password maximum size is 20 characters";
			return NO;
		case BAPasswordWithoutSpecialCharacter:
			_errorLabel.text = @"Password must contain atleast 1 special character";
			return NO;
		case BAPasswordWithoutUppercase:
			_errorLabel.text = @"Password must contain atleast 1 uppercase letter";
			return NO;
		default:
			break;
	}
	
	//check to see if passwords match, color and show error message accordingly
	if (![_retypePasswordTextField.text isEqualToString:_passwordTextField.text])
	{
		[_passwordTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
		[_retypePasswordTextField setBackgroundColor:[UIColor colorWithRed:kRedValueInvalidTextField green:kGreenValueInvalidTextField blue:kBlueValueInvalidTextField alpha:kAlphaValueInvalidTextField]];
		_errorLabel.text = @"Passwords do not match";
		return NO;
	}
	
	return YES;
}

- (IBAction)backPressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)tokenAndPasswordTextFieldsDidEndOnExit:(id)sender
{
	[sender resignFirstResponder];
}
@end
