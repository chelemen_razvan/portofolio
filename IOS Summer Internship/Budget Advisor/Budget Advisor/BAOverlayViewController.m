//
//  BAOverlayViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAOverlayViewController.h"

#define kErrorMessageWhiteColor 0.5f
#define kErrorMessageAlpha 0.5f

@implementation BAOverlayViewController
{
	//block called when retry is pressed
	void(^retryBlock)();
}

- (void)loadView
{
	[super loadView];
	_overlayView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
	
	_activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
	
	_errorMessageLabel = [[UILabel alloc] init];
	_retryButton = [[UIButton alloc] init];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[self.view addSubview:_overlayView];
	[self.view sendSubviewToBack:_overlayView];
	[_overlayView setBackgroundColor:[UIColor whiteColor]];
	
	[self.view addSubview:_activityIndicator];
	_activityIndicator.alpha = 1;
	_activityIndicator.center = self.view.center;
	_activityIndicator.hidesWhenStopped = YES;
	[self.view sendSubviewToBack:_activityIndicator];
	
	//add error label to view hierarchy
	[self.view addSubview:_errorMessageLabel];
	[self.view sendSubviewToBack:_errorMessageLabel];
	[self setErrorLabelProperties];
	
	//add retry button to view hierarchy
	[self.view addSubview:_retryButton];
	[self.view sendSubviewToBack:_retryButton];
	[self setRetryButtonProperties];
	
}

- (void) showOverlayView
{
	if (_overlayWillAppear)
		_overlayWillAppear();
	
	[self.view bringSubviewToFront:_overlayView];
	[_overlayView setHidden:NO];
}

- (void) hideOverlayView
{
	if (_overlayWillDisappear)
		_overlayWillDisappear();
	
	[_overlayView setHidden:YES];
}

- (void) showLoadingScreen
{
	[self showOverlayView];
	//show spinner
	[self.view bringSubviewToFront:_activityIndicator];
	[_activityIndicator startAnimating];
}

- (void) hideLoadingScreen
{
	[self hideOverlayView];
	//hide spinner
	[_activityIndicator stopAnimating];
}

- (void)setErrorLabelProperties
{
	CGRect errorMessageFrame = CGRectMake(0, 0, 300, 200);
	[_errorMessageLabel setFrame:errorMessageFrame];
	_errorMessageLabel.center = self.view.center;
	_errorMessageLabel.textColor = [UIColor colorWithWhite:kErrorMessageWhiteColor alpha:kErrorMessageAlpha];
	_errorMessageLabel.numberOfLines = 0;
	[_errorMessageLabel setTextAlignment:NSTextAlignmentCenter];
}

- (void)setRetryButtonProperties
{
	CGRect retryButtonFrame = CGRectMake(0, 0, 100, 30);
	[_retryButton setFrame:retryButtonFrame];
	CGPoint retryButtonCenter = CGPointMake(self.view.center.x, self.view.center.y + 70);
	_retryButton.center = retryButtonCenter;
	[_retryButton setTitle:@"Retry" forState:UIControlStateNormal];
	[_retryButton setBackgroundColor:[UIColor colorWithWhite:kErrorMessageWhiteColor alpha:kErrorMessageAlpha]];
	_retryButton.layer.cornerRadius = 5;
	_retryButton.clipsToBounds = YES;
	[_retryButton setHidden:YES];
}

- (void)showErrorScreenWithMessage:(NSString *) message andRetryBlock:(void(^)()) block
{
	[self showOverlayView];
	
	retryBlock = block;
	
	_errorMessageLabel.text = message;
	[_errorMessageLabel setHidden:NO];
	[self.view bringSubviewToFront:_errorMessageLabel];
	[_retryButton setHidden:NO];
	[self.view bringSubviewToFront:_retryButton];
	[_retryButton addTarget:self action:@selector(retryPressed)
		   forControlEvents:UIControlEventTouchUpInside];
}

- (void)hideErrorScreen
{
	[self hideOverlayView];
	
	[_errorMessageLabel setHidden:YES];
	[_retryButton setHidden:YES];
}

- (void)retryPressed
{
	retryBlock();
}


@end
