//
//  BAStatisticsViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/29/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XYPieChart.h"
#import "BAStatisticsService.h"
#import "BAOverlayViewController.h"
#import "BAStatisticsTableViewCell.h"
#import "BADateMapper.h"
#import "BAStatisticsContainerViewController.h"
#import "BAStatisticsColors.h"

@interface BAStatisticsViewController : BAOverlayViewController <XYPieChartDataSource>
@property (weak, nonatomic) IBOutlet XYPieChart *pieChartView;
@property (weak, nonatomic) IBOutlet UILabel *incomesTextField;
@property (weak, nonatomic) IBOutlet UILabel *expensesTextField;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *yearMonthSegmentedControl;

//array of uicolors for each arc of the piechart
@property(nonatomic, strong) NSArray *sliceColors;
//bottom container view's controller
@property (nonatomic, strong) BAStatisticsContainerViewController *containerViewController;

- (IBAction)yearMonthChanged:(id)sender;

- (void)loadStatisticsForMonth:(NSInteger) month year:(NSInteger) year;
- (void)loadStatisticsForYear:(NSInteger) year;

@end
