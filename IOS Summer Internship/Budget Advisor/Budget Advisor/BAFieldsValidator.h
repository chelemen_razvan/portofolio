//
//  BAFieldsValidator.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

//return types of email validator
typedef enum
{
	BAValidEmail = 0,
	BAInvalidEmailFormat = 1,
	BAEmailTooLong = 2
} BAEmailValidationResult;

//return types of password validation
typedef enum
{
	BAValidPassword = 0,
	BAPasswordTooShort = 1,
	BAPasswordTooLong = 2,
	BAPasswordWithoutUppercase = 3,
	BAPasswordWithoutSpecialCharacter = 4
} BAPasswordValidationResult;

@interface BAFieldsValidator : NSObject

- (BAEmailValidationResult) validateEmail:(NSString *) email;
- (BAPasswordValidationResult) validatePassword:(NSString *) password;
- (BOOL) validateName:(NSString *) name;

@end
