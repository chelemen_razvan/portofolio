//
//  BAYearStatisticsViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAOverlayViewController.h"
#import "BAStatisticsService.h"
#import "BAStatisticsTableViewCell.h"
#import "BAStatisticsColors.h"

@interface BAYearStatisticsViewController : BAOverlayViewController

@property (weak, nonatomic) IBOutlet UITableView *expensesTableView;
//array of uicolors for each arc of the piechart
@property(nonatomic, strong) NSArray *sliceColors;

@end
