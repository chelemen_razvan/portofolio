//
//  BAAuthenticationService.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/17/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BACredential.h"
#import "BANetworking.h"
#import "BASessionData.h"
#import "BANewUser.h"
#import "Foundation/NSJSONSerialization.h"

typedef void(^AuthenticationServiceLoginResponseBlock)(NSString *message, NSError *error);
typedef void(^AuthenticationServiceSignupResponseBlock)(NSDictionary *messages, NSError *error);


@interface BAAuthenticationService : NSObject

//log the user in and save the response access token in BASessionData
- (void) loginUser:(BACredential *) user withResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock;

//sign the new user user up
- (void) signupUser:(BANewUser *) newUser withResponseBlock:(AuthenticationServiceSignupResponseBlock) responseBlock;

//log the user out and set the token to nil since it won't be valid anymore
- (void) logoutUserwithResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock;

//send username for password recovery
//this method uses only usename propery of the BAUser object
- (void) resetPassword:(NSString *) email withResponseBlock:(AuthenticationServiceLoginResponseBlock) responseBlock;

//change user's password based on a passwordToken (provided by email probably)
- (void) changePassword:(NSString *) newPassword passwordToken:(NSString *) token withResponseBlock: (AuthenticationServiceLoginResponseBlock) responseBlock;

//this method is used to validate the token and get the userid for it, user id is automatically saved in session data
- (void) validateTokenWithResponseBlock:(AuthenticationServiceSignupResponseBlock) responseBlock;

@end
