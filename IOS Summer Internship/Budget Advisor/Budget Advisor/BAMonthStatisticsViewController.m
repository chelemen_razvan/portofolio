//
//  BAMonthStatisticsViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAMonthStatisticsViewController.h"
#import "BAStatisticsViewController.h"
#import "BAStatisticsContainerViewController.h"

#define kAnimationScrollLength 30
#define kDefaultCurrency @"RON"

typedef enum {
	BADirectionLeft = 1,
	BADirectionRight = 2
} BAAnimationDirection;

@implementation BAMonthStatisticsViewController
{
	BAStatisticsService *_statisticsService;
	BAStatisticsViewController *_parentController;
	NSArray *_statisticsArray;
	NSArray *_previousMonthStatisticsArray;
	NSInteger _currentlyShownMonth;
	NSInteger _currentlyShownYear;
	
	//initial position of months label used for swipe animation
	CGFloat _leftMonthX;
	CGFloat _rightMonthX;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	_statisticsService = [[BAStatisticsService alloc] init];
	
	//values used for animation
	_leftMonthX = _leftMonthLabel.frame.origin.x;
	_rightMonthX = _rightMonthLabel.frame.origin.x;
	
	BAStatisticsColors *colors = [[BAStatisticsColors alloc] init];
	_sliceColors = [colors getStatisticsColors];
	
	//get current date
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
	_currentlyShownMonth = dateComponents.month;
	_currentlyShownYear = dateComponents.year;
	
	//get a reference to the parent controller
	BAStatisticsContainerViewController *containerController = (BAStatisticsContainerViewController *) [self parentViewController];
	_parentController = (BAStatisticsViewController *) [containerController parentController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
	//load statistics for current month by default
    [self loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
}

- (void)loadStatisticsForMonth:(NSInteger) month year:(NSInteger) year
{
	[_statisticsService getStatisticOn:month from:year WithResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive data" andRetryBlock:^{
				[self loadStatisticsForMonth:month year:year];
			}];
			return;
		}
		
		//populate and reload views that use data
		_statisticsArray = array;
		[_expensesTableView reloadData];
	}];
	
	//load data on the previous month
	[_statisticsService getStatisticOn:month - 1 from:year WithResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive data" andRetryBlock:^{
				[self loadStatisticsForMonth:month year:year];
			}];
		}
		
		_previousMonthStatisticsArray = array;
		[_expensesTableView reloadData];
	}];
}

//handles the display of the next and prev months labels
- (void) displayMonthsAnimated:(BAAnimationDirection) direction
{
	[_leftMonthLabel setAlpha:0];
	[_rightMonthLabel setAlpha:0];
	
	BADateMapper *dateMapper = [[BADateMapper alloc] init];
	_leftMonthLabel.text = [dateMapper monthNumberToAbbreviation:_currentlyShownMonth - 1];
	_rightMonthLabel.text = [dateMapper monthNumberToAbbreviation:_currentlyShownMonth + 1];
	
	//set initial position
	CGRect leftMonthLabelFrame = _leftMonthLabel.frame;
	
	if (direction == BADirectionLeft)
		leftMonthLabelFrame.origin.x = _leftMonthX + kAnimationScrollLength;
	if (direction == BADirectionRight)
		leftMonthLabelFrame.origin.x = _leftMonthX - kAnimationScrollLength;
	
	_leftMonthLabel.frame = leftMonthLabelFrame;
	
	CGRect rightMonthLabelFrame = _rightMonthLabel.frame;
	
	if (direction == BADirectionLeft)
		rightMonthLabelFrame.origin.x = _rightMonthX + kAnimationScrollLength;
	if (direction == BADirectionRight)
		rightMonthLabelFrame.origin.x = _rightMonthX - kAnimationScrollLength;
	
	_rightMonthLabel.frame = rightMonthLabelFrame;
	
	[UIView animateWithDuration:1.0f animations:^{
		if (_currentlyShownMonth > 1)
		{
			//move left label
			CGRect leftMonthLabelFrame = _leftMonthLabel.frame;
			if (direction == BADirectionLeft)
				leftMonthLabelFrame.origin.x = leftMonthLabelFrame.origin.x - kAnimationScrollLength;
			if (direction == BADirectionRight)
				leftMonthLabelFrame.origin.x = leftMonthLabelFrame.origin.x + kAnimationScrollLength;
			[_leftMonthLabel setAlpha:1];
			_leftMonthLabel.frame = leftMonthLabelFrame;
		}
		
		if (_currentlyShownMonth == 12)
			return;
		
		//move right label
		CGRect rightMonthLabelFrame = _rightMonthLabel.frame;
		if (direction == BADirectionLeft)
			rightMonthLabelFrame.origin.x = rightMonthLabelFrame.origin.x - kAnimationScrollLength;
		if (direction == BADirectionRight)
			rightMonthLabelFrame.origin.x = rightMonthLabelFrame.origin.x + kAnimationScrollLength;
		[_rightMonthLabel setAlpha:1];
		_rightMonthLabel.frame = rightMonthLabelFrame;
	}];
}

//called when you swipe on the lower side of the screen
- (IBAction)swipeBottomViewLeft:(id)sender
{
	//keep month value in bounds
	if (_currentlyShownMonth < 12)
	{
		_currentlyShownMonth++;
	}
	
	//hide right label if last months stats are shown
	if (_currentlyShownMonth == 12)
	{
		[_rightMonthLabel setAlpha:0];
	}
	[self displayMonthsAnimated: BADirectionLeft];
	[self loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
	[_parentController loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
}

- (IBAction)swipeBottomViewRight:(id)sender
{
	//keep month value in bounds
	if (_currentlyShownMonth > 1)
	{
		_currentlyShownMonth--;
	}
	
	//hide left label if last months stats are shown
	if (_currentlyShownMonth == 1)
	{
		[_leftMonthLabel setAlpha:0];
	}
	[self displayMonthsAnimated: BADirectionRight];
	[self loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
	[_parentController loadStatisticsForMonth:_currentlyShownMonth year:_currentlyShownYear];
}

#pragma mark - Tableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _statisticsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BAStatisticsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatisticCell" forIndexPath:indexPath];
    BAStatisticElement *statistic = [_statisticsArray objectAtIndex:indexPath.row];
	
    cell.categoryNameLabel.text = statistic.category;
	cell.categoryNameLabel.backgroundColor = _sliceColors[indexPath.row];
	cell.expenseValueLabel.text = [NSString stringWithFormat:@"%i %@", statistic.expenses, kDefaultCurrency];
    if ([self compareStatisticWithPreviousMonth:statistic] > 0)
		cell.statusImageView.image = [UIImage imageNamed:@"up"];
	else if ([self compareStatisticWithPreviousMonth:statistic ] == 0)
		cell.statusImageView.image = [UIImage imageNamed:@"nada"];
	else
		cell.statusImageView.image = [UIImage imageNamed:@"down"];
	
    return cell;
}

- (NSInteger) compareStatisticWithPreviousMonth:(BAStatisticElement *) statistic
{
	for (BAStatisticElement *stat in _previousMonthStatisticsArray)
	{
		if ([stat.category isEqual:statistic.category ])
			return stat.expenses - statistic.expenses;
	}
	return 0;
}

@end
