//
//  BAStatisticsColors.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BAStatisticsColors : NSObject

//TO DO: Put all colors
- (NSArray *) getStatisticsColors;

@end
