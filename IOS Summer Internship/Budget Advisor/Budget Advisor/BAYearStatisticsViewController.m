//
//  BAYearStatisticsViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAYearStatisticsViewController.h"
#import "BAStatisticsContainerViewController.h"
#import "BAStatisticsViewController.h"

#define kDefaultCurrency @"RON"

@interface BAYearStatisticsViewController ()

@end

@implementation BAYearStatisticsViewController
{
	BAStatisticsService *_statisticsService;
	BAStatisticsViewController *_parentController;
	NSArray *_statisticsArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	_statisticsService = [[BAStatisticsService alloc] init];
	BAStatisticsColors *colors = [[BAStatisticsColors alloc] init];
	_sliceColors = [colors getStatisticsColors];
	
	//get a reference to the parent controller
	BAStatisticsContainerViewController *containerController = (BAStatisticsContainerViewController *) [self presentingViewController];
	NSLog(@"%@", containerController);
	_parentController = (BAStatisticsViewController *) [containerController parentController];
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	//get current date
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *dateComponents = [gregorian components:(NSYearCalendarUnit  | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:[NSDate date]];
	[self loadStatisticsForYear:dateComponents.year];
}

- (void)loadStatisticsForYear:(NSInteger) year
{
	[_statisticsService getStatisticOn:year withResponseBlock:^(NSArray *array, NSError *error) {
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive data" andRetryBlock:^{
				[self loadStatisticsForYear:year];
			}];
			return;
		}
		
		//populate and reload views that use data
		_statisticsArray = array;
		[_expensesTableView reloadData];
		NSLog(@"%@", _parentController);
		[_parentController loadStatisticsForYear:year];
	}];
}

#pragma mark - Tableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _statisticsArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BAStatisticsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"StatisticCell" forIndexPath:indexPath];
    BAStatisticElement *statistic = [_statisticsArray objectAtIndex:indexPath.row];
	
    cell.categoryNameLabel.text = statistic.category;
	cell.categoryNameLabel.backgroundColor = _sliceColors[indexPath.row];
	cell.expenseValueLabel.text = [NSString stringWithFormat:@"%i %@", statistic.expenses, kDefaultCurrency];
	
    return cell;
}


@end
