//
//  BAStatisticsTableViewCell.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/29/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BAStatisticsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *expenseValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;

@end
