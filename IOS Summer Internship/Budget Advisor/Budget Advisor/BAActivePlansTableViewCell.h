//
//  BAActivePlansTableViewCell.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/24/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BAActivePlansTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *dueByLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
