//
//  BATabBarViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/25/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BATabBarViewController.h"

@interface BATabBarViewController ()

@end

@implementation BATabBarViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
	[self.tabBar setTintColor:nil];
	UIColor *titleColor = [UIColor colorWithRed:0 green:0.588 blue:0.533 alpha:0.8]; /*#009688*/
	[[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : titleColor } forState:UIControlStateSelected];
	
	UITabBarItem *dashboardItem = [self.tabBar.items objectAtIndex:0];
	[dashboardItem setImage:[[UIImage imageNamed:@"visa.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	UITabBarItem *transactionsItem = [self.tabBar.items objectAtIndex:1];
	[transactionsItem setImage:[[UIImage imageNamed:@"cash_light.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	UITabBarItem *statisticsItem = [self.tabBar.items objectAtIndex:2];
	[statisticsItem setImage:[[UIImage imageNamed:@"statistics.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
	UITabBarItem *plansItem = [self.tabBar.items objectAtIndex:3];
	[plansItem setImage:[[UIImage imageNamed:@"plan.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

@end
