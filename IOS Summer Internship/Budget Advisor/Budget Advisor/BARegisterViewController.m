//
//  BARegisterViewController.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 22/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BARegisterViewController.h"
#define kKeyboardPadding 20
#define kFieldWithErrorBackgroundColor [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.2]

@interface BARegisterViewController ()

- (BOOL)fieldsAreCorrect;

@end

@implementation BARegisterViewController
{
    BAKeyboardHandler *keyboardHandler;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [keyboardHandler registerKeyboardHandlerFor:self.view viewCoveredByKeyboard:self.registerFormErrorLabel withTolerance:10];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [keyboardHandler unregisterKeyboardHandler];
    [super viewWillDisappear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.firstNameTextField.delegate = self;
    self.lastNameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.retypeEmailTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.retypePasswordTextField.delegate = self;
    
    //needed to dismiss the keyboard when another part of layout is touched
    UITapGestureRecognizer *yourTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollTap:)];
    [self.scrollView addGestureRecognizer:yourTap];
    [self.view addSubview:self.scrollView];
    [self makeButtonsCornersRound];
    keyboardHandler = [[BAKeyboardHandler alloc] init];
}

- (void) makeButtonsCornersRound
{
	self.cancelButton.layer.cornerRadius = 5;
	self.cancelButton.clipsToBounds = YES;
    self.signUpButton.layer.cornerRadius = 5;
	self.signUpButton.clipsToBounds = YES;
}

- (void)scrollTap:(UIGestureRecognizer*)gestureRecognizer
{
    //make keyboard disappear
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    self.registerFormErrorLabel.text = nil;
}

- (BOOL) textFieldShouldReturn:(UITextField *)theTextField
{
    //hides keyboard when Return is pressed
    [theTextField resignFirstResponder];
    return YES;
}

- (IBAction)goBack:(UIButton *)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)signUpButtonPressed:(id)sender
{
    if (self.fieldsAreCorrect)
    {
        BANewUser *userDetails = [[BANewUser alloc] init];
        userDetails.firstName = self.firstNameTextField.text;
        userDetails.lastName = self.lastNameTextField.text;
        userDetails.email = self.emailTextField.text;
        BAAuthenticationService *authenticationService = [[BAAuthenticationService alloc] init];
        [authenticationService signupUser:userDetails withResponseBlock:^(NSDictionary *messages, NSError *error) {
            if (error != nil)
            {
                if ([messages objectForKey:@"userName"] != nil)
                {
                    self.registerFormErrorLabel.text = [messages objectForKey:@"userName"];
                }
                else self.registerFormErrorLabel.text = @"Something went wrong. Please try again later.";
            }
            else 
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Succes" message:@"Registration successful. Please sign in to use the app!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
    else return;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)fieldsAreCorrect
{
    BOOL correctlyFilled = true;
    NSInteger numberOfFieldErrors = 0;
    BAFieldsValidator *fieldsValidator = [[BAFieldsValidator alloc] init];
        self.registerFormErrorLabel.text = nil;
    
    //validate the email fields - must match
    if ([self.emailTextField.text isEqualToString:self.retypeEmailTextField.text])
    {
        self.emailTextField.backgroundColor = [UIColor whiteColor];
        self.retypeEmailTextField.backgroundColor = [UIColor whiteColor];
       
        //validate the email fields - must be valid
        if ([fieldsValidator validateEmail:self.emailTextField.text] != 0)
        {
            correctlyFilled = false;
            numberOfFieldErrors ++;
            self.registerFormErrorLabel.text = @"The provided email address is not valid. Please update the highlighted fields";
            self.emailTextField.backgroundColor = kFieldWithErrorBackgroundColor;
            self.retypeEmailTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        }
    }
    else
    {
        correctlyFilled = false;
        numberOfFieldErrors ++;
        
        self.emailTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        self.retypeEmailTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        if (numberOfFieldErrors > 1)
        {
            self.registerFormErrorLabel.text = @"Please update the highlighted fields with valid information";
        }
        else
        {
            self.registerFormErrorLabel.text = @"The provided emails don't match. Please update the highlighted fields";
        }
    
    }
    
    //validate the password fields - must match
    if ([self.passwordTextField.text isEqualToString:self.retypePasswordTextField.text])
    {
        self.passwordTextField.backgroundColor = [UIColor whiteColor];
        self.retypePasswordTextField.backgroundColor = [UIColor whiteColor];
        
        //validate the password fields - must be valid
        if ([fieldsValidator validatePassword:self.passwordTextField.text] != 0)
        {
            correctlyFilled = false;
            numberOfFieldErrors ++;
            
            self.passwordTextField.backgroundColor = kFieldWithErrorBackgroundColor;
            self.retypePasswordTextField.backgroundColor = kFieldWithErrorBackgroundColor;
            if (numberOfFieldErrors > 1)
            {
                self.registerFormErrorLabel.text = @"Please update the highlighted fields with valid information";
            }
            else
            {
                self.registerFormErrorLabel.text = @"Password must be between 7-20 characters, must contain at least 1 uppercase letter and 1 special character.";
            }
        }
    }
    else
    {
        correctlyFilled = false;
        numberOfFieldErrors ++;

        self.passwordTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        self.retypePasswordTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        if (numberOfFieldErrors > 1)
        {
            self.registerFormErrorLabel.text = @"Please update the highlighted fields with valid information";
        }
        else
        {
            self.registerFormErrorLabel.text = @"The provided passwords don't match. Please update the highlighted fields";
        }
    }
    
    //validate the first name field
    if ([fieldsValidator validateName:self.firstNameTextField.text])
    {
        self.firstNameTextField.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        correctlyFilled = false;
        numberOfFieldErrors ++;
        
        self.firstNameTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        if (numberOfFieldErrors > 1)
        {
            self.registerFormErrorLabel.text = @"Please update the highlighted fields with valid information";
        }
        else
        {
            if (self.firstNameTextField.text.length == 0)
            {
                self.registerFormErrorLabel.text = @"First name field must be completed";
            }
            else
            {
                self.registerFormErrorLabel.text = @"First name can be max 50 characters long";
            }
        }
    }
    
    //validate the last name field
    if ([fieldsValidator validateName:self.lastNameTextField.text])
    {
        self.lastNameTextField.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        correctlyFilled = false;
        numberOfFieldErrors ++;
        
        self.lastNameTextField.backgroundColor = kFieldWithErrorBackgroundColor;
        if (numberOfFieldErrors > 1)
        {
            self.registerFormErrorLabel.text = @"Please update the highlighted fields with valid information";
        }
        else
        {
            if (self.lastNameTextField.text.length == 0)
            {
                self.registerFormErrorLabel.text = @"Last name field must be completed";
            }
            else
            {
                self.registerFormErrorLabel.text = @"Last name can be max 50 characters long";
            }
        }
    }
    return correctlyFilled;
}

@end
