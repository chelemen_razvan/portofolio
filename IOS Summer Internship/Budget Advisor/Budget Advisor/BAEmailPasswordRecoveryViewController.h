//
//  BAEmailPasswordRecoveryViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/23/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAFieldsValidator.h"
#import "BAAuthenticationService.h"
#import "BAKeyboardHandler.h"
#import "BAOverlayViewController.h"

@interface BAEmailPasswordRecoveryViewController : BAOverlayViewController
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

- (IBAction)resetButtonPressed:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)emailTextFieldDidEndedOnExit:(id)sender;
@end
