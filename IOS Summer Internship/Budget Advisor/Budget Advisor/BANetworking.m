//
//  BANetworking.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/16/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BANetworking.h"
#import "JSONModel.h"

//#define SERVER_ADDRESS @"http://siminescu.apiary-mock.com"
//#define SERVER_ADDRESS @"http://ci-univ.university.iquest.ro:8181/budget-adviser-authentication-webapp-1.0.0"

#define SERVER_ADDRESS @"http://169.254.149.6:8080/service"

@implementation BANetworking
{
	AFHTTPRequestOperationManager *_requestManager;
}

- (id) init
{
	self = [super init];
	if (self)
	{
		_requestManager = [AFHTTPRequestOperationManager manager];
		_requestManager.requestSerializer = [AFJSONRequestSerializer serializer];
		_requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
	}
	
	return self;
}

- (void) getResource:(NSString *) resource withParameters:(NSDictionary *)parameters success:(NetworkingSuccessBlock) success failure:(NetworkingFailureBlock)failure
{
    NSString *urlString = [self getURLFormattedForResource:resource andParameters:parameters];
	
	[_requestManager GET:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
		success(responseObject);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		failure(operation.responseObject, error);
	}];
}

- (void) postResource:(NSString *) resource withBodyData:(NSData *) bodyData andParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure
{
	NSString *urlString = [self getURLFormattedForResource:resource andParameters:parameters];
	
	[_requestManager POST:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
		success(responseObject);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		failure(operation.responseObject, error);
	}];
}

- (void) deleteResource:(NSString *) resource withParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure
{
	NSString *urlString = [self getURLFormattedForResource:resource andParameters:parameters];
	[_requestManager DELETE:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
		success(responseObject);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		failure(operation.responseObject, error);
	}];
	
}

- (void) putResource:(NSString *) resource withBodyData:(NSData *) bodyData andParameters:(NSDictionary *) parameters success:(NetworkingSuccessBlock)success failure:(NetworkingFailureBlock)failure
{
	NSString *urlString = [self getURLFormattedForResource:resource andParameters:parameters];
	
	[_requestManager PUT:urlString parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
		success(responseObject);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		failure(operation.responseObject, error);
	}];
}

- (NSString *) getURLFormattedForResource:(NSString *) resource andParameters:(NSDictionary *) parameters
{
	if (resource == nil)
		return nil;
	
	NSMutableString *urlString = [NSMutableString stringWithFormat:@"%@/%@", SERVER_ADDRESS, resource];
	if (parameters == nil)
	{
		return urlString;
	}
	
	[urlString appendString:@"?"];
	//append each parameter to string
	for (NSString *key in parameters)
	{
		NSString *value = [parameters objectForKey:key];
		[urlString appendString:[NSString stringWithFormat:@"%@=%@&", key, value]];
	}
	
	//remove last ampersand
	[urlString deleteCharactersInRange:NSMakeRange([urlString length]-1, 1)];
	
	return urlString;
}

@end
