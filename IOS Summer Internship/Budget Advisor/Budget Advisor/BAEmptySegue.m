//
//  BAEmptySegue.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/30/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BAEmptySegue.h"

@implementation BAEmptySegue

- (void)perform
{
    // Nothing. The ContainerViewController class handles all of the view
    // controller action.
}

@end
