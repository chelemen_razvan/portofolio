//
//  BATransactionService.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 17/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BANetworking.h"
#import "BASessionData.h"
#import "BATransaction.h"
#import "BAAuthenticationService.h"

typedef void(^TransactionServiceRetrieveAnIncomeResponseBlock)(NSDictionary *data, NSError *error);
typedef void(^TransactionServiceAddAnIncomeResponseBlock)(NSString *message, NSError *error);
typedef void(^TransactionServiceArrayOfTransactionsResponseBlock)(NSArray *transactionsArray, NSError *error);

@interface BATransactionService : NSObject

#pragma mark Incomes
//retrieve an income with id
- (void) retrieveAnIncomeWithId:(NSInteger)incomeId andResponseBlock:(TransactionServiceRetrieveAnIncomeResponseBlock)responseBlock;

//add an income
- (void) addAnIncome:(BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//edit an income
- (void) editAnIncome: (BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//delete an income
- (void) deleteAnIncome:(BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//retrieve all incomes
-(void) retrieveAllIncomesWithResponseBlock:(TransactionServiceArrayOfTransactionsResponseBlock)responseBlock;

#pragma mark Expenses
//retrieve an expense with id
- (void) retrieveAnExpenseWithId:(NSInteger)currentId andResponseBlock:(TransactionServiceRetrieveAnIncomeResponseBlock)responseBlock;

//add an expense
- (void) addAnExpense:(BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//edit an expense
- (void) editAnExpense: (BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//delete an expense
- (void) deleteAnExpense:(BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock;

//retrieve all expenses
-(void) retrieveAllExpensesWithResponseBlock:(TransactionServiceArrayOfTransactionsResponseBlock)responseBlock;

@end
