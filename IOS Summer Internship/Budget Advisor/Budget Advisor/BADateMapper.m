//
//  BADateMapper.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/25/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BADateMapper.h"

@implementation BADateMapper

- (NSString *) monthNumberToAbbreviation:(NSInteger) monthNumber
{
	switch (monthNumber)
	{
		case 1:
			return @"Jan.";
		case 2:
			return @"Feb.";
		case 3:
			return @"Mar.";
		case 4:
			return @"Apr.";
		case 5:
			return @"May";
		case 6:
			return @"Jun.";
		case 7:
			return @"Jul.";
		case 8:
			return @"Aug.";
		case 9:
			return @"Sept.";
		case 10:
			return @"Oct.";
		case 11:
			return @"Nov.";
		case 12:
			return @"Dec.";
		default:
			return @"Invalid month number";
	}
}

@end
