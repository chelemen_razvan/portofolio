//
//  BATransactionsTableViewCell.h
//  Budget Advisor
//
//  Created by Istvan Szekely on 28/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ELActionCell.h"

@interface BATransactionsTableViewCell : ELActionCell
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;

@end
