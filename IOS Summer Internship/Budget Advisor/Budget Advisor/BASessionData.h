//
//  BASessionData.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/17/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BASessionData : NSObject

@property (nonatomic, strong) NSString *authToken;
@property (nonatomic, strong) NSString *userId;

//this is a singleton class used to store data for current session
//use this method to get a reference to the singleton object
+ (BASessionData *) sessionData;

@end
