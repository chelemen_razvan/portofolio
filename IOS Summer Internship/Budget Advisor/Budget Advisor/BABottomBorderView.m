//
//  BABottomBorderView.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/24/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BABottomBorderView.h"

#define kBorderGrayscaleValue 0.8f
#define kBorderAlphaValue 0.4f
#define kBorderThickness 1.0f

@implementation BABottomBorderView
{
	CALayer *_bottomBorder;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _bottomBorder = [CALayer layer];
    }
    return self;
}


- (void)layoutSubviews
{
	[super layoutSubviews];
	
	//add the border layer
	_bottomBorder.frame = CGRectMake(0.0f, self.frame.size.height, self.frame.size.width, kBorderThickness);
	_bottomBorder.backgroundColor = [UIColor colorWithWhite:kBorderGrayscaleValue alpha:kBorderAlphaValue].CGColor;
	[self.layer addSublayer:_bottomBorder];
}


@end
