//
//  BAPasswordRecoveryViewController.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/22/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BAKeyboardHandler.h"
#import "BAFieldsValidator.h"
#import "BAAuthenticationService.h"
#import "BAOverlayViewController.h"

@interface BAPasswordRecoveryViewController : BAOverlayViewController
@property (weak, nonatomic) IBOutlet UITextField *tokenTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *retypePasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

- (IBAction)saveButtonPressed:(id)sender;
- (IBAction)backPressed:(id)sender;
- (IBAction)tokenAndPasswordTextFieldsDidEndOnExit:(id)sender;

@end
