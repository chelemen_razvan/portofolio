//
//  BATransactionService.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 17/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BATransactionService.h"
#define kAnIncomeResource @"incomes"
#define kExpensesResource @"expenses"
#define kJsonTokenKey @"token"
#define kJsonContentKey @"Content"
#define kIdCategory @"idCategory"
#define kAmount @"amount"
#define kDate @"date"
#define kCurrency @"currency"
#define kRepetition @"repetition"
#define kUserId @"userId"

@implementation BATransactionService

#pragma mark Incomes

- (void) retrieveAnIncomeWithId:(NSInteger)incomeId andResponseBlock:(TransactionServiceRetrieveAnIncomeResponseBlock)responseBlock
{
    NSString *retrieveAnIncomeResource = [NSString stringWithFormat:@"%@%li", kAnIncomeResource, (long)incomeId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrieveAnIncomeParameters = @{kJsonTokenKey: sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:retrieveAnIncomeResource withParameters:retrieveAnIncomeParameters success:^(id data) {
        //network response is ok, income is retreieved
        responseBlock(data, nil);
    } failure:^(id data, NSError *error) {
        //error occured
        responseBlock([data objectForKey:kJsonContentKey], error);
    }];
    
}

- (void) addAnIncome:(BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *addAnIncomeParameters = @{kJsonTokenKey: sessionData.authToken};
    NSData *jsonAddAnIncome = [[income toJSONStringWithKeys:@[kIdCategory, kAmount, kDate, kCurrency, kRepetition, kUserId]] dataUsingEncoding:NSUTF8StringEncoding];
    BANetworking *networking = [[BANetworking alloc] init];
    [networking postResource:kAnIncomeResource withBodyData:jsonAddAnIncome andParameters:addAnIncomeParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) editAnIncome:(BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    NSString *editAnIncomeResource = [NSString stringWithFormat:@"%@%li", kAnIncomeResource, (long)income.transactionId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *editAnIncomeParameters = @{kJsonTokenKey:sessionData.authToken};
    NSData *jsonEditAnIncome = [[income toJSONStringWithKeys:@[kIdCategory, kAmount, kDate, kCurrency, kRepetition, kUserId]] dataUsingEncoding:NSUTF8StringEncoding];
    BANetworking *networking = [[BANetworking alloc] init];
    [networking putResource:editAnIncomeResource withBodyData:jsonEditAnIncome andParameters:editAnIncomeParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) deleteAnIncome:(BATransaction *)income withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    NSString *deleteAnIncomeResource = [NSString stringWithFormat:@"%@%li", kAnIncomeResource, (long)income.transactionId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *deleteAnIncomeParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking deleteResource:deleteAnIncomeResource withParameters:deleteAnIncomeParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void)retrieveAllIncomesWithResponseBlock:(TransactionServiceArrayOfTransactionsResponseBlock)responseBlock
{
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrieveAnIncomeParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:kAnIncomeResource withParameters:retrieveAnIncomeParameters success:^(id data) {
        
        NSArray *incomes = (NSArray *) data;
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (NSDictionary *element in incomes)
        {
            NSError *error = nil;
            BATransaction *income = [[BATransaction alloc] initWithDictionary:element error:&error];
            if (error == nil)
            {
                [resultArray addObject:income];
            }
        }
        responseBlock(resultArray, nil);
        
    } failure:^(id data, NSError *error) {
        responseBlock([data objectForKey:kJsonContentKey], error);
    }];
}

#pragma mark Expenses

- (void) retrieveAnExpenseWithId:(NSInteger)currentId andResponseBlock:(TransactionServiceRetrieveAnIncomeResponseBlock)responseBlock
{
    NSString *retrieveAnExpenseResource = [NSString stringWithFormat:@"%@%li", kExpensesResource, (long)currentId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrieveAnExpenseParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:retrieveAnExpenseResource withParameters:retrieveAnExpenseParameters success:^(id data) {
        //network response is ok, income is retreieved
        responseBlock(data, nil);
    } failure:^(id data, NSError *error) {
        //error occured
        responseBlock([data objectForKey:kJsonContentKey], error);
    }];
}

- (void) addAnExpense:(BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *addAnExpenseParameters = @{kJsonTokenKey:sessionData.authToken};
    NSData *jsonAddAnExpense = [[expense toJSONStringWithKeys:@[kIdCategory, kAmount, kDate, kCurrency, kRepetition, kUserId]] dataUsingEncoding:NSUTF8StringEncoding];
    BANetworking *networking = [[BANetworking alloc] init];
    [networking postResource:kExpensesResource withBodyData:jsonAddAnExpense andParameters:addAnExpenseParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) editAnExpense:(BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    NSString *editAnExpenseResource = [NSString stringWithFormat:@"%@/%li", kExpensesResource, (long)expense.transactionId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *editAnExpenseParameters = @{kJsonTokenKey:sessionData.authToken};
    NSData *jsonEditAnExpense = [[expense toJSONStringWithKeys:@[kIdCategory, kAmount, kDate, kCurrency, kRepetition, kUserId]] dataUsingEncoding:NSUTF8StringEncoding];
    BANetworking *networking = [[BANetworking alloc] init];
    [networking putResource:editAnExpenseResource withBodyData:jsonEditAnExpense andParameters:editAnExpenseParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void) deleteAnExpense:(BATransaction *)expense withResponseBlock:(TransactionServiceAddAnIncomeResponseBlock)responseBlock
{
    NSString *deleteAnExpenseResource = [NSString stringWithFormat:@"%@/%li", kExpensesResource, (long)expense.transactionId];
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *deleteAnExpenseParameters = @{kJsonTokenKey:sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking deleteResource:deleteAnExpenseResource withParameters:deleteAnExpenseParameters success:^(id data) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, nil);
    } failure:^(id data, NSError *error) {
        NSString *message = [data objectForKey:kJsonContentKey];
        responseBlock(message, error);
    }];
}

- (void)retrieveAllExpensesWithResponseBlock:(TransactionServiceArrayOfTransactionsResponseBlock)responseBlock
{
    //get token
    BASessionData *sessionData = [BASessionData sessionData];
    NSDictionary *retrieveAllExpensesParameters = @{kJsonTokenKey: sessionData.authToken};
    BANetworking *networking = [[BANetworking alloc] init];
    [networking getResource:kExpensesResource withParameters:retrieveAllExpensesParameters success:^(id data) {
        
        NSArray *expenses = (NSArray *) data;
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        for (NSDictionary *element in expenses)
        {
            NSError *error = nil;
            BATransaction *expense = [[BATransaction alloc] initWithDictionary:element error:&error];
            if (error == nil)
            {
                [resultArray addObject:expense];
            }
        }
        responseBlock(resultArray, nil);
        
    } failure:^(id data, NSError *error) {
        responseBlock([data objectForKey:kJsonContentKey], error);
    }];
}

@end
