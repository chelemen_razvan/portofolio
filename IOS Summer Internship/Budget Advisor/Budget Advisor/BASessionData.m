//
//  BASessionData.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/17/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BASessionData.h"

@implementation BASessionData

#warning this is only for testing
- (id) init
{
	self = [super init];
	if (self)
	{
		_authToken = @"test_token";
	}
	return self;
}

+ (BASessionData *) sessionData
{
	static BASessionData *sessionData = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sessionData = [[self alloc] init];
	});
	return sessionData;
}

@end
