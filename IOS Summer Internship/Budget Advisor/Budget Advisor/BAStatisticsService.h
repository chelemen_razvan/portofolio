//
//  BAStatisticsService.h
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/21/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BANetworking.h"
#import "BAStatisticElement.h"

typedef void(^StatisticsServiceResponseBlock)(NSArray *array, NSError *error);

@interface BAStatisticsService : NSObject

- (void) getStatisticOnAllYearsWithResponseBlock:(StatisticsServiceResponseBlock) responseBlock;
- (void) getStatisticOn:(NSInteger) year withResponseBlock:(StatisticsServiceResponseBlock) responseBlock;
- (void) getStatisticOn:(NSInteger) month from:(NSInteger) year WithResponseBlock:(StatisticsServiceResponseBlock) responseBlock;

@end
