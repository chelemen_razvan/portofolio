//
//  BANewTransactionViewController.m
//  Budget Advisor
//
//  Created by Razvan Chelemen on 7/25/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import "BANewTransactionViewController.h"

#define kBottomViewToKeyboardSpace -10.0
#define kDefaultCurrency @"RON"

@implementation BANewTransactionViewController
{
	NSArray *_pickerData;
	NSArray *_incomesCategories;
	NSArray *_expensesCategories;
	NSArray *_repetition;
	
	UIPickerView *_picker;
	UIDatePicker *_datePicker;
	
	BACategoriesService *_categoriesService;
	BATransactionService *_transactionService;
	BAKeyboardHandler *_keyboardHandler;
	
	BACategory *_selectedCategory;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	_categoriesService = [[BACategoriesService alloc] init];
	_transactionService = [[BATransactionService alloc] init];
	_keyboardHandler = [[BAKeyboardHandler alloc] init];
	
	_picker = [[UIPickerView alloc] init];
    _picker.dataSource = self;
    _picker.delegate = self;
	
	_datePicker = [[UIDatePicker alloc] init];
	_datePicker.datePickerMode = UIDatePickerModeDate;
	[_datePicker addTarget:self action:@selector(pickerDateChanged) forControlEvents:UIControlEventValueChanged];
	
	//set imputs for each textfield
	[_amountTextField setKeyboardType:UIKeyboardTypeNumberPad];
    _categoryTextField.inputView = _picker;
	_repetitionTextField.inputView = _picker;
	_dateTextField.inputView = _datePicker;
	
	_repetition = @[@"Daily", @"Weekly", @"Monthly", @"Yearly"];
	
	//set overlay callbacks
	__weak typeof(_saveButton) weakSaveButton = _saveButton;
	self.overlayWillAppear = ^(){
		weakSaveButton.enabled = NO;
	};
	self.overlayWillDisappear = ^(){
		weakSaveButton.enabled = YES;
	};
}

- (void) viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[_keyboardHandler registerKeyboardHandlerFor:self.view viewCoveredByKeyboard:_descriptionViewContainer withTolerance:kBottomViewToKeyboardSpace];

	if (_selectedTransaction) {
		[self populateFields];
	}
	[self loadCategories];
}

- (void) populateFields
{
	NSLog(@"%@", _selectedTransaction.type);
	if ([_selectedTransaction.type isEqualToString:@"expense"])
		_incomeExpenseToggleButton.on = YES;
	else
		_incomeExpenseToggleButton.on = NO;
	_amountTextField.text = [NSString stringWithFormat:@"%i",_selectedTransaction.amount];
	_categoryTextField.text = _selectedTransaction.category.name;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd-MMMM-yyyy"];
	_dateTextField.text = [formatter stringFromDate:_selectedTransaction.date];
	_repetitionTextField.text = _repetition[_selectedTransaction.repetition];
	_descriptionTextView.text = _selectedTransaction.description;
	
	_selectedTransaction = nil;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	
	//hide keyboard
	[self.view endEditing:YES];
}

- (void) viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[_keyboardHandler unregisterKeyboardHandler];
}

- (void) pickerDateChanged
{
	NSDate *pickerDate = _datePicker.date;
	NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
	[formatter setDateFormat:@"dd-MMMM-yyyy"];
	_dateTextField.text = [formatter stringFromDate:pickerDate];
}

- (void) loadCategories
{
	[self showLoadingScreen];
	//load expenses categories
	[_categoriesService getAllCategoriesForType:BAExpenseCategory withResponseBlock:^(NSArray *categories, NSError *error) {
		[self hideLoadingScreen];
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive the data" andRetryBlock:^{
				//hide the error overlay
				[self hideErrorScreen];
				//call the method again
				[self loadCategories];
			}];
			return;
		}
		_expensesCategories = categories;
	}];
	
	[self showLoadingScreen];
	//load incomes categories
	[_categoriesService getAllCategoriesForType:BAIncomeCategory withResponseBlock:^(NSArray *categories, NSError *error) {
		[self hideLoadingScreen];
		if (error != nil)
		{
			[self showErrorScreenWithMessage:@"Could not retrive the data" andRetryBlock:^{
				//hide the error overlay
				[self hideErrorScreen];
				//call the method again
				[self loadCategories];
			}];
			return;
		}
		_incomesCategories = categories;
	}];
}

//tell the keyboard handler when to move up the view and when not
- (IBAction)categoryTextFieldEditingDidBegin:(id)sender
{
	[_keyboardHandler shouldMoveView:NO];
	if (_incomeExpenseToggleButton.isOn)
		_pickerData = _expensesCategories;
	else
		_pickerData = _incomesCategories;
	[_picker reloadAllComponents];
}

- (IBAction)repetitionTextFieldEditingDidBegin:(id)sender
{
	[_keyboardHandler shouldMoveView:YES];
	_keyboardHandler.coveredView = _dateViewContainer;
	_pickerData = _repetition;
	[_picker reloadAllComponents];
}

- (IBAction)amountTextFieldEditingDidBegin:(id)sender
{
	[_keyboardHandler shouldMoveView:NO];
}

- (IBAction)dateTextFieldEditingDidBegin:(id)sender
{
	[_keyboardHandler shouldMoveView:YES];
	_keyboardHandler.coveredView = _dateViewContainer;
}

- (IBAction)incomeExpenseToggleButtonValueChanged:(id)sender
{
	//reset categories field since they are different for incomes/expenses
	_selectedCategory = nil;
	_categoryTextField.text = nil;
	if ([_categoryTextField isFirstResponder])
		[_categoryTextField resignFirstResponder];
}

#pragma mark - Navigation bar items actions

- (IBAction)backPressed:(id)sender
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)savePressed:(id)sender
{
	BATransaction *newTransaction = [[BATransaction alloc] init];
	newTransaction.category = _selectedCategory;
	newTransaction.description = _descriptionTextView.text;
	newTransaction.amount = [_amountTextField.text integerValue];
	newTransaction.date = _datePicker.date;
	newTransaction.currency = kDefaultCurrency;
	
	if ([_repetitionTextField.text isEqualToString:@"Daily"])
		newTransaction.repetition = BARepetitionEveryDay;
	if ([_repetitionTextField.text isEqualToString:@"Weekly"])
		newTransaction.repetition = BARepetitionEveryWeek;
	if ([_repetitionTextField.text isEqualToString:@"Monthly"])
		newTransaction.repetition = BARepetitionEveryMonth;
	if ([_repetitionTextField.text isEqualToString:@"Yearly"])
		newTransaction.repetition = BAREpetitionEveryYear;
	
	[self showLoadingScreen];
	
	__weak typeof(self) weakSelf = self;
	if (_incomeExpenseToggleButton.isOn)
	{
		[_transactionService addAnExpense:newTransaction withResponseBlock:^(NSString *message, NSError *error) {
			[weakSelf hideLoadingScreen];
			if (error != nil)
			{
				[weakSelf showErrorScreenWithMessage:@"Transaction could not be saved" andRetryBlock:^{
					[weakSelf savePressed:sender];
				}];
				return;
			}
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Transaction successfully saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
			[alert show];
		}];
	}
	else
	{
		[_transactionService addAnIncome:newTransaction withResponseBlock:^(NSString *message, NSError *error) {
			[weakSelf hideLoadingScreen];
			if (error != nil)
			{
				[weakSelf showErrorScreenWithMessage:@"Transaction could not be saved" andRetryBlock:^{
					[weakSelf savePressed:sender];
				}];
				return;
			}
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Save" message:@"Transaction successfully saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
			[alert show];
		}];
	}
}

#pragma mark - Picker datasource methods

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return  1;
}

#pragma mark - Picker delegate methods

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	if (_categoryTextField.isEditing)
		return ((BACategory *) _pickerData[row]).name;
	else
		return _pickerData[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	
	//check who is first responder to know which field should be edited
	if (_categoryTextField.isFirstResponder)
	{
		_selectedCategory = _pickerData[row];
		_categoryTextField.text = ((BACategory *) _pickerData[row]).name;
	}
    
	if (_repetitionTextField.isFirstResponder)
	{
		_repetitionTextField.text = _pickerData[row];
	}
	
	if (_dateTextField.isFirstResponder)
	{
		_dateTextField.text = _pickerData[row];
	}
}

#pragma mark - TextView delegate methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
	_keyboardHandler.coveredView = _descriptionViewContainer;
	[_keyboardHandler shouldMoveView:YES];
	return YES;
}

@end
