//
//  main.m
//  Budget Advisor
//
//  Created by Istvan Szekely on 14/07/14.
//  Copyright (c) 2014 iQuest Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BAAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BAAppDelegate class]));
    }
}
